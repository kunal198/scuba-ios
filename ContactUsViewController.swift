//
//  ContactUsViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 15/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet var User_NAME_textFeild: UITextField!
    
    @IBOutlet var User_Email_textfeild: UITextField!
    
        @IBOutlet var User_Phoneno_textfeild: UITextField!
    
    @IBOutlet var Message_textfeild: UITextField!
    
    
    @IBOutlet var User: UIView!
    
    
    
    
    @IBOutlet var scroolview: UIScrollView!
    var viewHasMovedToRight = false
    var controller:SidemenuViewController? = nil    //hold the reference here.
    
    
    
    func swipeview(notification:Notification) -> Void {
        
        self  .swipefunction()
        
    }
    
    func swipefunction()
    {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                        self.viewHasMovedToRight = false
                        
                        //self.controller?.didMove(toParentViewController: self)
        }, completion: { (finished) -> Void in
            
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let nc = NotificationCenter.default // Note that default is now a property, not a method call
        nc.addObserver(forName:Notification.Name(rawValue:"Contactus"),
                       object:nil, queue:nil,
                       using:swipeview)
        
        
        controller = storyboard?.instantiateViewController(withIdentifier: "slidemenu") as! SidemenuViewController?
        addChildViewController(controller!)
        controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
        
        view.addSubview((controller?.view)!)
        controller?.didMove(toParentViewController: self)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        controller?.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        controller?.view.addGestureRecognizer(swipeLeft)
        
        let tap = UITapGestureRecognizer  (target: self, action: #selector(self.tapgesture))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false

    }
    @IBAction func Menu_Action(_ sender: Any) {
    
        if viewHasMovedToRight == false
            
        {
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                            self.viewHasMovedToRight = true
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        else
        {
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                            self.viewHasMovedToRight = false
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        
        
        
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = true
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.down:
                
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = false
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.up:
                
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    //MArk : Textfeild Delegate :: 
    
    //Mark :- TextFeild Delegate
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        print("touched began")
        User_NAME_textFeild.resignFirstResponder()
        User_Email_textfeild.resignFirstResponder()
        User_Phoneno_textfeild.resignFirstResponder()
        Message_textfeild.resignFirstResponder()
        
    }
    
    
    func tapgesture(gesture: UIGestureRecognizer) {
        self.view.endEditing(true)
        print("touched began")
        User_NAME_textFeild.resignFirstResponder()
        User_Email_textfeild.resignFirstResponder()
        User_Phoneno_textfeild.resignFirstResponder()
        Message_textfeild.resignFirstResponder()
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (self.view.frame.size.width == 320)
        {
            
            
            if (textField == User_NAME_textFeild)
            {
                scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(150)), animated: true)
                
            }
            else if (textField == User_Email_textfeild)
            {
                scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(190)), animated: true)
            }
            else if (textField == User_Phoneno_textfeild)
            {
                scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(220)), animated: true)
            }
            else
            {
                scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(300)), animated: true)
            }
            
            
        }
          //  if (self.view.frame.size.width == 414)
        else
        {
             if (textField == User_Email_textfeild)
            {
                scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(190)), animated: true)
            }
            else if (textField == User_Phoneno_textfeild)
            {
                scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(220)), animated: true)
            }
            else
            {
                scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(300)), animated: true)
            }
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    public func textFieldDidEndEditing(_ textField: UITextField)
        
    {
        textField.resignFirstResponder()
        
        scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
        
    }
    
    
}
  

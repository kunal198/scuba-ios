//
//  HistoricalViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 08/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import MapKit

class HistoricalViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate {
    var controller:SidemenuViewController? = nil    //hold the reference here.
    var viewHasMovedToRight = false
    
    var array = [NSMutableArray]()
    var dict = [Any]()
    var imageName: String!
    var coordinates: [[Double]]!
    var names:[String]!
    var addresses:[String]!
    var phones:[String]!
    var weather:[String]!
    var water :[String]!
    var conditions :[String]!
    var visibility :[String]!
    var itle: [String]!
    var Page  = 1;
    var array_activity = NSMutableArray()
    var array_type = NSMutableArray()
    
    var array_ID = NSMutableArray()
    var array_userId = NSMutableArray()
    var User_id = NSString()
    
    @IBOutlet var NextLabel: UILabel!
    var Pagination = 0;
    var numberofpage :Int = 0
     var cellcount :Int = 0
    @IBOutlet var table_view: UITableView!
    
    var Trips = NSArray()
    @IBOutlet var Gallery_Leading: NSLayoutConstraint!
    @IBOutlet var Phae1_btn: UIButton!
    @IBOutlet var Page2_btn: UIButton!
    @IBOutlet var Page3_btn: UIButton!
    @IBOutlet var Pae4_btn: UIButton!
    @IBOutlet var Page5_btn: UIButton!
    
    struct Location {
        let title: String
        let latitude: Double
        let longitude: Double
    }
    
    @IBOutlet var nextbtn: UIButton!
    @IBOutlet var Pagination_view: UIView!
    @IBOutlet var Gallery_heading: UILabel!
    @IBOutlet var Mapview: MKMapView!
    @IBOutlet var selectedList: UILabel!
    @IBOutlet var Mapview_topconst: NSLayoutConstraint!
    @IBOutlet var MapviewMain_view: UIView!
    @IBOutlet var ListviewMain_view: UIView!
    @IBOutlet var MainListview: UIView!
    @IBOutlet var Listview: UIView!
    @IBOutlet var topconstraint: NSLayoutConstraint!
    @IBOutlet var viewtop: NSLayoutConstraint!
    @IBOutlet var xtopsr: NSLayoutConstraint!
    @IBOutlet var selectview: UIView!
    @IBOutlet var dropdownbtn: UIButton!
    
    @IBAction func DropdownAction(_ sender: Any) {
        

        
        if dropdownbtn.isSelected
        {
            dropdownbtn .isSelected = false
            topconstraint.constant = 20;
            Mapview_topconst.constant = 20;
            Listview.isHidden = true
        }
        else
        {
            
            topconstraint.constant = topconstraint.constant+60;
            Mapview_topconst.constant = topconstraint.constant+60;
            Listview.isHidden = false
            dropdownbtn .isSelected = true
            
        }
        
        
    }
    
    
    @IBAction func Listview_select(_ sender: Any)
    {
        dropdownbtn .isSelected = false
        topconstraint.constant = 20;
        Mapview_topconst.constant = 20;
        selectedList.text = "List View"
        Listview.isHidden = true
        ListviewMain_view.isHidden=false;
        MapviewMain_view.isHidden = true;
        
    }
    
    @IBAction func Mapview_select(_ sender: Any)
    {
        self.Mapview.delegate = self;
        dropdownbtn .isSelected = false
        topconstraint.constant = 20;
        Mapview_topconst.constant = 20;
        selectedList.text = "Map View"
        Listview.isHidden = true
        ListviewMain_view.isHidden=true;
        MapviewMain_view.isHidden = false;
        
        
        for i in 0..<Trips.count
        {

            let history : HistoryLogs = Trips .object(at: i) as! HistoryLogs
            let point = StarbucksAnnotation(coordinate: CLLocationCoordinate2D(latitude: history.latitude , longitude: history.longitude ))
            point.title = history.activity
            point.address = history.location
            point.conditions=history.water_conditions
            point.weather = history.meteo
            point.water = history.water_temprature
            point.visibility = history.visibility
            point.latitude = String (history.latitude)
            point.longitude = String (history.longitude)
            self.Mapview.addAnnotation(point)
            self.Mapview.showAnnotations( self.Mapview.annotations, animated: true)

        }
        
    }
    @IBAction func log_edit(_ sender: UIButton)
    {
        UserDefaults.standard.set(false, forKey: "checkSearchedItem")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"addLogs") as! AdddLOgsViewController
        viewController.checkEdit = true
        let data = self.array_ID[0] as!NSArray
        let data1 = data[sender.tag]
        UserDefaults.standard.set(data1, forKey: "log_id")
        UserDefaults.standard.set(self.User_id, forKey: "user_id")
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func Button_Action(_ sender: Any) {
    
        if viewHasMovedToRight == false
        {
            
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                            self.viewHasMovedToRight = true
                            self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        else
        {
            

            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                            self.viewHasMovedToRight = false
                            
                            self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        
        
        
    }

    
    
    func swipeview(notification:Notification) -> Void {
        
        self  .swipefunction()
        
    }
    
    func swipefunction()
    {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                        self.viewHasMovedToRight = false
                        
                        //self.controller?.didMove(toParentViewController: self)
        }, completion: { (finished) -> Void in
            
        })
    }
    

//   @IBAction func Page_Action(_ sender: Any)
//    {
//        Page = 1;
//        self.getHistorylogs(with:Page)
//        Phae1_btn.layer.borderWidth = 0.0;
//        Page2_btn.layer.borderWidth = 1.0;
//        Page3_btn.layer.borderWidth = 1.0;
//        Pae4_btn.layer.borderWidth = 1.0;
//        Page5_btn.layer.borderWidth = 1.0;
//
//
//
//
//    }
//    
//    @IBAction func Page_Action_1(_ sender: Any) {
//        
//        Page = 2;
//        self.getHistorylogs(with:Page)
//        Phae1_btn.layer.borderWidth = 1.0;
//        Page2_btn.layer.borderWidth = 0.0;
//        Page3_btn.layer.borderWidth = 1.0;
//        Pae4_btn.layer.borderWidth = 1.0;
//        Page5_btn.layer.borderWidth = 1.0;
//    }
//    
//    @IBAction func Page_Action_2(_ sender: Any)
//    {
//        Page = 3;
//        self.getHistorylogs(with:Page)
//        Phae1_btn.layer.borderWidth = 1.0;
//        Page2_btn.layer.borderWidth = 1.0;
//        Page3_btn.layer.borderWidth = 0.0;
//        Pae4_btn.layer.borderWidth = 1.0;
//        Page5_btn.layer.borderWidth = 1.0;
//
//    }
//    @IBAction func Page_Action_3(_ sender: Any)
//    {
//        Page = 4;
//        self.getHistorylogs(with:Page)
//        Phae1_btn.layer.borderWidth = 1.0;
//        Page2_btn.layer.borderWidth = 1.0;
//        Page3_btn.layer.borderWidth = 1.0;
//        Pae4_btn.layer.borderWidth = 0.0;
//        Page5_btn.layer.borderWidth = 1.0;
//
//    }
//    @IBAction func Page_Action_4(_ sender: Any)
//    {
//        Page = 5;
//        self.getHistorylogs(with:Page)
//        Phae1_btn.layer.borderWidth = 1.0;
//        Page2_btn.layer.borderWidth = 1.0;
//        Page3_btn.layer.borderWidth = 1.0;
//        Pae4_btn.layer.borderWidth =  1.0;
//        Page5_btn.layer.borderWidth = 0.0;
//
//    }
//    
//    
//    @IBAction func Next_btn(_ sender: Any)
//    {
//        
//        Page = Page+1
//        let newage  =   Page+1
//        
//        if (newage > numberofpage )
//        {
//            NextLabel .isHidden = true
//            nextbtn.isHidden = true
//        }
//        
//        self.getHistorylogs(with: Page)
//    }
//    
//    @IBAction func Previous_btn(_ sender: Any)
//    {
//        
//        if Page > 1
//        {
//            nextbtn.isHidden = false
//            Page = Page-1
//            self.getHistorylogs(with: Page)
//        }
//      
//    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.Pagination_view.isHidden = true
        
         self.tabBarController?.selectedIndex = 1
         Page = 1
     //   Phae1_btn.layer.borderWidth = 0.0
        
        getHistorylogs()
       
        Listview.isHidden = true
        if (self.view.frame.size.width == 375)
        {
            xtopsr.constant = xtopsr.constant + 20
            Gallery_Leading.constant = 25

        }
        else if (self.view.frame.size.width == 414)
        {
            xtopsr.constant = xtopsr.constant + 40
            Gallery_Leading.constant = 27

        }
        else if (self.view.frame.size.width == 320)
        {
            
//            Gallery_heading.font = UIFont(name: "Helvetica Neue-Regular", size: CGFloat(2))
            
        }

        selectview.layer.borderColor = UIColor.lightGray.cgColor
        selectview.layer.borderWidth = 1
        selectview.layer.masksToBounds = true
        
        Listview.layer.borderColor = UIColor.lightGray.cgColor
        Listview.layer.borderWidth = 1
        Listview.layer.masksToBounds = true
        
        
        MainListview.layer.borderColor = UIColor.lightGray.cgColor
        MainListview.layer.borderWidth = 1
        MainListview.layer.masksToBounds = true
        

        let nc = NotificationCenter.default // Note that default is now a property, not a method call
        nc.addObserver(forName:Notification.Name(rawValue:"Historicalview"),
                       object:nil, queue:nil,
                       using:swipeview)
        controller = storyboard?.instantiateViewController(withIdentifier: "slidemenu") as! SidemenuViewController?
        addChildViewController(controller!)
        controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
        view.addSubview((controller?.view)!)
        controller?.didMove(toParentViewController: self)
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        controller?.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        controller?.view.addGestureRecognizer(swipeLeft)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool)
    {
       // print(self.ListviewMain_view.frame.origin.y)
       // print(self.tabBarController?.tabBar.frame.size.height!)
//    self.table_view.frame.size.height = self.view.frame.size.height - (self.ListviewMain_view.frame.origin.y+40+(self.tabBarController?.tabBar.frame.size.height)!)
    }
 
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = true
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
//            case UISwipeGestureRecognizerDirection.down:
//                
//                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = false
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
//            case UISwipeGestureRecognizerDirection.up:
//                
//                print("Swiped up")
            default:
                break
            }
        }
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        
        
    {
//        cellcount = Page
//        if (Page == 1)
//        {
//            cellcount = 1;
//        }
//        if (Page == 2)
//        {
//            cellcount = 11;
//        }
//        if (Page == 3)
//        {
//            cellcount = 21;
//        }
//        if (Page == 4)
//        {
//            cellcount = 31;
//        }
//        if (Page == 5)
//        {
//            cellcount = 41;
//        }
        return self.Trips .count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
       
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SlidemenuTableViewCell
        cell.srxconst.constant = xtopsr.constant
       let history : HistoryLogs = Trips .object(at: indexPath.row) as! HistoryLogs

       cell.srno.text! = String(indexPath.row+1)
       cellcount = cellcount + 1 ;
       cell.POI.text = history.location
        
       cell.edit.tag = indexPath.row
        
        if (history.date .isEmpty)
        {
          
        }
        else
        {
            var token = history.date.components(separatedBy:"-")
            print (token[0])
            cell.year.text = token[0]
           // cell.year.text = Util .GetyearFromDate(with: history.date)

        }
       //cell.LOGs.text = "LOGS"
    //cell.Galeery.text = "MEDIA"
        return cell
    }
     public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    
     {
        return 40;
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation
        {
            return nil
        }
        
        var annotationView = self.Mapview.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        
        print (annotation.title!!)
        
        if annotation.title!! == "Snorking"
        {
            annotationView?.image = UIImage(named: "search POIimg3")
            
        }
        else if annotation.title!! == "Surfing"
        {
            annotationView?.image = UIImage(named: "search POIimg2")
            
        }
        else
        {
            annotationView?.image = UIImage(named: "search POIimg1")
            
        }
        
        
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let starbucksAnnotation = view.annotation as! StarbucksAnnotation
        let views = Bundle.main.loadNibNamed("CustomCalloutView", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCalloutView
        calloutView.starbucksName.text = starbucksAnnotation.name
        calloutView.starbucksAddress.text = starbucksAnnotation.address
        // calloutView.starbucksPhone.text = starbucksAnnotation.phone
        calloutView.starbuckswater.text = starbucksAnnotation.water
        calloutView.starbucksweather.text = starbucksAnnotation.weather
        calloutView.starbucksconditions.text = starbucksAnnotation.conditions
        calloutView.starbucksvisibility.text = starbucksAnnotation.visibility
        calloutView.starbuckscord.text = starbucksAnnotation.latitude  + "," + "  " + starbucksAnnotation.longitude
        
        // 3
        calloutView.center = CGPoint(x: view.bounds.size.width / 3+140, y: +calloutView.bounds.size.height*0.52+20)
        view.addSubview(calloutView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      //  self .swipefunction()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
//        Page = 1
       self.tabBarController?.tabBar.isHidden = false
        self.getHistorylogs()
    }
    
    
    
    func getHistorylogs()
    {
       
        
        DispatchQueue.main.async {
            self.view .isUserInteractionEnabled = false
            Util.showindicatior(with: (self.view)!)
        }
        
     
        Model .getlogs(withSuccess: { (Result) in
            print(Result);
            
      //  var success = Result.value(forKey: "c") as? String
        let success = Result.value(forKey: "status") as? String
   
        if (success == "success" )
        {
                self.array_ID.removeAllObjects()
                self.array_activity.removeAllObjects()
                self.array_type.removeAllObjects()
          DispatchQueue.main.async {
                    [weak self] in
                  //  self?.Pagination_view.isHidden = false
                    
                    print(Result)
                    let Message = Result.value(forKey: "message") as! String
                    if (Message == "Log History")
                    {
                        
                        //let arr = Result.value(forKey: "pagination") as! NSDictionary
                        //print(arr)
                        //self?.numberofpage = arr .value(forKey: "lastpage") as! Int
                        
//                       if (self?.numberofpage == 1)
//                        {
//                            self?.Pagination_view.isHidden = true
//                        }
//                        else if (self?.numberofpage == 2)
//                        {
//                               self?.Pagination_view.isHidden = false
//                            self?.NextLabel .isHidden = true
//                            self?.nextbtn.isHidden = true
//                            self?.Page2_btn.isHidden = false;
//                            self?.Page3_btn.isHidden = true;
//                            self?.Pae4_btn.isHidden = true;
//                            self?.Page5_btn.isHidden = true;
//
//
//                        }
//                        else if (self?.numberofpage == 3)
//                        {
//                             self?.Pagination_view.isHidden = false
//                            self?.NextLabel .isHidden = true
//                            self?.nextbtn.isHidden = true
//                            self?.Page2_btn.isHidden = false;
//                            self?.Page3_btn.isHidden = false;
//                            self?.Pae4_btn.isHidden = true;
//                            self?.Page5_btn.isHidden = true;
//                            
//                            
//                        }
//                        else if (self?.numberofpage == 4)
//                        {
//                            self?.Pagination_view.isHidden = false
//                            self?.NextLabel .isHidden = true
//                            self?.nextbtn.isHidden = true
//                            self?.Page2_btn.isHidden = false;
//                            self?.Page3_btn.isHidden = false;
//                            self?.Pae4_btn.isHidden = false;
//                            self?.Page5_btn.isHidden = true;
//                        }
//                       else if ((self?.numberofpage)! >= 5)
//                       {
//                        if (self?.numberofpage == 5)
//                        {
//                            self?.NextLabel .isHidden = true
//                            self?.nextbtn.isHidden = true
//                        }
//                        else
//                        {
//                            self?.NextLabel .isHidden = false
//                            self?.nextbtn.isHidden = false
//                        }
//                        self?.Pagination_view.isHidden = false
//                        self?.Page2_btn.isHidden = false;
//                        self?.Page3_btn.isHidden = false;
//                        self?.Pae4_btn.isHidden = false;
//                        self?.Page5_btn.isHidden = false;
//                        }

                        self?.Trips = NSMutableArray()
                        print(Result)
                        let array  = Result.value(forKey: "data") as! NSArray
                      //  let activity = array.value(forKey: "activity") as! NSArray
                        self?.array_activity.add(array.value(forKey: "activity"))
                        self?.array_type.add(array.value(forKey: "type"))
                        self?.array_ID.add(array.value(forKey: "id"))
                        self?.array_userId.add(array.value(forKey: "user_id"))
                        
                        let demoaerr1 = self?.array_userId[0] as! NSArray
                        let demoaerr2 = demoaerr1[0] as! NSString
                        
                        self?.User_id = demoaerr2
                        Util.stopIndicator(with: (self?.view)!)
                        self?.Trips = Util .Parsedict(with: array) ;
                        self?.table_view .reloadData()
                        self?.view .isUserInteractionEnabled = true
                    }
                    else
                    {
                        
                        OperationQueue.main.addOperation {
                            [weak self] in
                            //self?.activityIndicator.stopAnimating();
                            let alert = UIAlertController(title: "Alert", message: "There is no History Logs", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                            self?.present(alert, animated: true, completion: nil)
                            Util.showMessage("There is no History Logs", withTitle: "Alert!")
                            Util.stopIndicator(with: (self?.view)!)
                            print("no data Found");
                            self?.Trips = NSArray()
                            self?.view.isUserInteractionEnabled = true
                            self?.table_view .reloadData()
                            
                        }
                    
                }
                    
                }
            }
            else {
                OperationQueue.main.addOperation {
                    [weak self] in
                    //self?.activityIndicator.stopAnimating();
                    self?.view .isUserInteractionEnabled = true
                    
                    let alert = UIAlertController(title: "Alert", message: "There is no History Logs", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                    Util.stopIndicator(with: (self?.view)!)
                    
                }
                
            }
            
        }, failure: { (error) in
            
            
            OperationQueue.main.addOperation {
                [weak self] in
                //self?.activityIndicator.stopAnimating();
                self?.view .isUserInteractionEnabled = true

                let alert = UIAlertController(title: "Alert", message: error, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self?.present(alert, animated: true, completion: nil)
                Util.stopIndicator(with: (self?.view)!)
                print(error);
            }
            
        })
    }
    
    
        
        
    }


//
//  LoginViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 07/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class LoginViewController: UIViewController, UITextFieldDelegate{
   var presentWindow : UIWindow?
    var datamodel1 = NSData()
    var login  = FBSDKLoginManager()
    var fbAccessToken = String()
    @IBOutlet var User_Nametextfeild: UITextField!
    @IBOutlet var User_Emailtextfeild: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let returnValue = UserDefaults.standard.object(forKey: "Islogin") as? String
        
        if (returnValue == "yes")
        {
        
        Util.showindicatior(with: self.view)
        let kUserDefault = UserDefaults.standard
            
            
     // if ( kUserDefault.string(forKey: "username")! == "" )
        
        //{
            
                
       // }
        //else
        //{
         //   USerinfo.sharedInstance.user_name =  kUserDefault.string(forKey: "username")!
       // }
       
       // if ( kUserDefault.string(forKey: "user_email")! == "" )
        
        //{
            
       // }
       // else
       // {
       //    USerinfo.sharedInstance.user_email = kUserDefault.string(forKey: "email")!
       // }
       // exist
                if UserDefaults.standard.value(forKey: "id") != nil
                {
                    let str  = kUserDefault.string(forKey: "id")!
                    USerinfo.sharedInstance.user_id = str
                    OperationQueue.main.addOperation {
                        [weak self] in
                        
                        self? .performSegue(withIdentifier: "Homeview", sender: self)
                        Util.stopIndicator(with: (self?.view)!)
                        
                    }
                }
             }
        User_Emailtextfeild.isSecureTextEntry = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    public func textFieldDidEndEditing(_ textField: UITextField)
        
    {
        textField.resignFirstResponder()
        
        
    }
    
    @IBAction func Login_Action(_ sender: Any)
    {

     User_Nametextfeild.resignFirstResponder()
     User_Emailtextfeild.resignFirstResponder()
     LoginAction()
   
    }
    
    @IBAction func Back_btnAction(_ sender: Any)
   
    {
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func facebook_login(_ sender: Any)
   
    {
        
        login = FBSDKLoginManager()
        
       // login.logIn(withPublishPermissions: ["publish_actions"], from: self, handler: { (result, error) in
            //TODO: process error or result.
        //    print("error are \(String(describing: error))")
         //   print("REsult are \(String(describing: result))")
          //  if error == nil {
                self.login .logIn(withReadPermissions:["public_profile","user_friends","user_photos","user_location","user_education_history","user_birthday","user_posts"], from: self) { (result, error) in
                    if (error == nil)
                    {
                        if(result?.isCancelled)!
                        {
                        
                        } else {

                        OperationQueue.main.addOperation {
                            [weak self] in

                            Util.stopIndicator(with: (self?.view)!)
                          //  print( (result)!)
                            
                        //    let id = FBSDKAccessToken.current().userID
                         //   let user_id = FBSDKAccessToken.current().appID
                            self?.fbAccessToken = FBSDKAccessToken.current().tokenString
                            self?.getFaceBookDetails()
                        }
                      
                        OperationQueue.main.addOperation
                        {
                            [weak self] in
                           
                            Util.stopIndicator(with: (self?.view)!)
                        }
                        }
                    }
                    else
                    {
                        print(error!)
                   //     print(error?.localizedDescription)
                        OperationQueue.main.addOperation {
                            [weak self] in
                            Util.stopIndicator(with: (self?.view)!)
                           Util.showMessage("There is some Network Problem!", withTitle: "Alert!")
                        }
                    }
                    
                }
         //   }
      //  })
        
    }
    func getFaceBookDetails()
    {
        if ((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,birthday,gender,hometown,location,about"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    print(result as Any)
                    print(error as Any)
                    //let id = String()
                    var fname = String()
                    var lname = String()
                    var url = String()
            
                    let fbDetails = result as! NSDictionary
                    fname = fbDetails.value(forKey: "first_name") as! String
                    lname = fbDetails.value(forKey: "last_name") as! String
                    let dict1 = fbDetails.value(forKey: "picture") as! NSDictionary
                    let dict2 = dict1.value(forKey: "data") as! NSDictionary
                    url = dict2.value(forKey: "url") as! String
                    let id = FBSDKAccessToken.current().userID
                  //  print("\(id)\(fname)\(lname)\(url)")
                    self.completefblogin(id: id!, fname: fname, lname: lname, url: url)
                } else {
                print(result!)
                
                }
            })
        }
    }
    
    func completefblogin (id : String,fname:String,lname:String,url:String)
    {
         Util.showindicatior(with: self.view)
        Model .Loginfacebook(with: id, fname: fname, lName: lname, url: url,  withSuccess: { (Result) in
           // print(Result)
            OperationQueue.main.addOperation {
                [weak self] in
                print(Result)
                 let kUserDefault = UserDefaults.standard
                let dict = Result.value(forKey: "data")as! NSDictionary
                
                let dictt = dict.value(forKey: "user_data") as! NSDictionary
                
                print (dictt)
                var str = String()
            
                if dictt.value(forKey: "id") as? String == nil{
                    
                    str = String (dictt.value(forKey: "id") as! Int )
                    
                }else{
                    str = dictt.value(forKey: "id") as! String

                }
                
                USerinfo.sharedInstance.user_id = str
                
                kUserDefault.set(USerinfo.sharedInstance.user_id, forKey: "id")
                UserDefaults.standard.set("yes", forKey: "Islogin")
                UserDefaults.standard.synchronize()
                self? .performSegue(withIdentifier: "Homeview", sender: self)
                Util.stopIndicator(with: (self?.view)!)
            }
        }) { (error) in
            OperationQueue.main.addOperation {
                [weak self] in
                Util.stopIndicator(with: (self?.view)!)
            }
            print("There is some Error")
        }
    }
    
    @IBAction func ForgotPassword(_ sender: Any)
    {
        
        User_Nametextfeild.resignFirstResponder()
        User_Emailtextfeild.resignFirstResponder()
        
        if (User_Nametextfeild.text == nil || User_Nametextfeild.text == "" )
        {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Email", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            Util.showindicatior(with: self.view)
        
        var request = URLRequest(url: URL(string: "http://beta.brstdev.com/Scubap/api/web/index.php/v1/forgot")!)
        request.httpMethod = "POST"
        
        
        let post = NSString(format:"email=%@",User_Nametextfeild.text!)
        
        let postString :String  = post as String
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
               // print("error=\(error)")
                
                let alert = UIAlertController(title: "Alert", message: "There is some Network Error", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
              Util.stopIndicator(with: self.view)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
               // print("statusCode should be 200, but is \(httpStatus.statusCode)")
               // print("response = \(response)")
                
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
           // print("json = \(json)")
            
            let success = json?["success"] as! Bool
            
            if (success == true )
            {
                OperationQueue.main.addOperation {
                    [weak self] in
                    
                    Util.stopIndicator(with: (self?.view)!)
                    let alert = UIAlertController(title: "Alert", message:"Please Check Your Email for Further instructions" , preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    [weak self] in
                    Util.stopIndicator(with: (self?.view)!)
                    let alert = UIAlertController(title: "Alert", message:json?["error"] as? String , preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        }
        task.resume()
        
        }
  
    }

    func LoginAction()
    {
        if (User_Emailtextfeild.text == nil || User_Emailtextfeild.text == "" || User_Nametextfeild.text == nil || User_Nametextfeild.text == "")
        {
            
            Util.showMessage("Please check your Email and Password", withTitle: "Alert!")
            
        }
       else
        {
            self.view.isUserInteractionEnabled = false
            
            Util.showindicatior(with: self.view)
            
            
            var request = URLRequest(url: URL(string: "http://beta.brstdev.com/Scubap/api/web/index.php/v1/login")!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 40)
            
        request.httpMethod = "POST"
   
        let post = NSString(format:"email=%@&password=%@",User_Nametextfeild.text!,User_Emailtextfeild.text!)
        let postString :String  = post as String
        request.httpBody = postString.data(using: .utf8)
        
    
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                OperationQueue.main.addOperation {
                    [weak self] in
                    MBProgressHUD.hideAllHUDs(for: (self?.view)!, animated: true);
                    self?.view.isUserInteractionEnabled = true
                    Util.showMessage("There is some Network Error", withTitle: "Alert!")
                    
                }
                 return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")

            }

            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
             let success = json?["success"] as! Bool
            
            if (success == true )
            {
                let response = json?["response"] as! NSDictionary
              //  print(response);
                OperationQueue.main.addOperation {
                    [weak self] in
                   
                     let kUserDefault = UserDefaults.standard
                    USerinfo.sharedInstance.user_name = response.value(forKey: "username") as! String;
                    let m  = response.value(forKey: "id") as! Int;
                      USerinfo.sharedInstance.user_id = String (m)
                    USerinfo.sharedInstance.user_email = response.value(forKey: "email") as! String;
                    kUserDefault.set(USerinfo.sharedInstance.user_name, forKey: "username")
                    kUserDefault.set(USerinfo.sharedInstance.user_id, forKey: "id")
                    kUserDefault.set(USerinfo.sharedInstance.user_email, forKey: "email")
                    kUserDefault.synchronize()
                    let gender = response.value(forKey: "gender")
                    if (gender is NSNull )
                    {

                    }
                    else
                    {
                        USerinfo.sharedInstance.user_gender = response.value(forKey: "gender") as! String;

                    }
                    
                    UserDefaults.standard.set("yes", forKey: "Islogin")
                    UserDefaults.standard.synchronize()
                
                    let id = response.value(forKey: "id") as! Int
                     print(id);
                     Util .updateuser_id(user_id: id )
                    MBProgressHUD.hideAllHUDs(for: (self?.view)!, animated: true);
                    
                     self?.view.isUserInteractionEnabled = true
                    self? .performSegue(withIdentifier: "Homeview", sender: self)
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                    [weak self] in
                    self?.view.isUserInteractionEnabled = true
                    MBProgressHUD.hideAllHUDs(for: (self?.view)!, animated: true);
                    
                    let alert = UIAlertController(title: "Alert", message: json?["error"]as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                    
                }
            }
        }
        task.resume()
    
        }
    }
}

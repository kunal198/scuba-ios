 
//  AppDelegate.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 06/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var ishome = Bool()
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        IQKeyboardManager.sharedManager().enable = true

        ishome = false;
        
       /* let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        if (screenWidth == 414)
        {
            let tabBarBackground = UIImage(named: "Homeselect-2")
            UITabBar.appearance().backgroundImage = tabBarBackground
        }
        else if (screenWidth == 320)
        {
            let tabBarBackground = UIImage(named: "Homeselect-1")
            UITabBar.appearance().backgroundImage = tabBarBackground
        }
        else
        {
            let tabBarBackground = UIImage(named: "Homeselect")
            UITabBar.appearance().backgroundImage = tabBarBackground
        }
        */
        
//        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
//        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.black], for: .normal)
//        UITabBar.appearance().tintColor = UIColor.black
//        
//        
//        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
//        
//        if(FBSDKAccessToken.current() != nil)
//        {
//            OperationQueue.main.addOperation {
//                [weak self] in
//                
//                self?.window = UIWindow(frame: UIScreen.main.bounds)
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let initialViewController = storyboard.instantiateViewController(withIdentifier: "Homeview")
//                initialViewController.tabBarController?.tabBar.isHidden = false
//                self?.window?.rootViewController = initialViewController
//                self?.window?.makeKeyAndVisible()
//                
//                }
//            
//        }
//        else
//        {
//            self.window = UIWindow(frame: UIScreen.main.bounds)
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let initialViewController = storyboard.instantiateViewController(withIdentifier: "Login")
//            self.window?.rootViewController = initialViewController
//            self.window?.makeKeyAndVisible()
//
//        }
        
     //   let returnValue = UserDefaults.standard.object(forKey: "Islogin") as? String
        
//        if (returnValue == "yes")
//        {
//            
//                     ishome = true
//            //
//            self.window = UIWindow(frame: UIScreen.main.bounds)
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let initialViewController = storyboard.instantiateViewController(withIdentifier: "Homeview")
//            initialViewController.tabBarController?.tabBar.isHidden = false
//            self.window?.rootViewController = initialViewController
//            self.window?.makeKeyAndVisible()
//
//            
//            
//        }
//        else
//        {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "Login")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()

      // }

        
        // Override point for customization after application launch.
        return true
    }
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
//    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func applicationDidBecomeActive(application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url as URL!,
            sourceApplication: sourceApplication,
            annotation: annotation)
    }
    
    
    func changeback()
    {
        
        
        
        
    }
}


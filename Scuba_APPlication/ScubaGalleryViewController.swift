//
//  ScubaGalleryViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 16/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import SDWebImage

class ScubaGalleryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate{

    var oldrow = 0
    var newIndexPath: IndexPath?

    
    var viewHasMovedToRight = false
    var controller:SidemenuViewController? = nil    //hold the reference here.
    var viewHasMovedDown = false
    var array_imgUrl = NSMutableArray()
    var array_LocationImages = NSArray()
    var checkLogImages = false
    
    @IBOutlet var collectionview: UICollectionView!
    @IBOutlet var collectionViewGrid: UICollectionView!
    @IBOutlet var view_imgPreview: UIView!
    @IBOutlet var img_preview: UIImageView!
    @IBOutlet var view_imgPreview_back: UIView!
    
    @IBOutlet var selectedimage: UIImageView!
    var images :[String] = ["gallery-1", "gallery2", "gallery3" ,"gallery4","gallery5","gallery2","gallery3","gallery4","gallery5","gallery2","gallery3", "gallery-1", "gallery2",  "gallery3" ,"gallery4","gallery5","gallery2","gallery3","gallery4","gallery5","gallery2","gallery3"]
    func swipeview(notification:Notification) -> Void {
        
        self  .swipefunction()
        
    }
    
    var xr = 0;
    var xl = 0;
    var y = 0;

  
    func swipefunction()
    {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                        self.viewHasMovedToRight = false
                        
                        //self.controller?.didMove(toParentViewController: self)
        }, completion: { (finished) -> Void in
            
        })
    }
    
    
    @IBOutlet var btn_right: UIButton!
    @IBAction func RightAction(_ sender: Any)
    {
        
        
//        let visibleItems: NSArray = collectionview.indexPathsForVisibleItems as NSArray
//        let currentItem: IndexPath = visibleItems.object(at: 1) as! IndexPath
//        
//        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
//        print(nextItem)
//        collectionview.scrollToItem(at: nextItem, at: .left, animated: true)
        
    }
    
        
        
        
        
//        xr = images.count / 3
//        xl = 0
//        y = images.count % 3
//        if xr - 1 > 0 {
//            newIndexPath = IndexPath(row: oldrow + 3, section: 0)
//            oldrow = oldrow + 3
//            collectionview.scrollToItem(at: newIndexPath!, at: .left, animated: true)
//            xr -= 1
//            xl += 1
//        }
//        else if y > 0 {
//            
//            //            newIndexPath = IndexPath(row: images.count - 3, section: 0)
//            oldrow = images.count - 3
//            collectionview.scrollToItem(at: newIndexPath!, at: .left, animated: true)
//        }
  
    
    
    
    @IBAction func LeftAction(_ sender: Any)
    {
        
//        let visibleItems: NSArray = collectionview.indexPathsForVisibleItems as NSArray
//        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
//        
//        let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
//        print(nextItem)
//        collectionview.scrollToItem(at: nextItem, at: .right, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       
        if checkLogImages == true
        {
            self.locationImages()
        }
        else
        {
            self.getHistorylogs()
        }
        
    }
    
    func catchNotification(notification:Notification) -> Void
    {
        self.locationImages()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//gallery
        self.tabBarController?.tabBar.isHidden = true
        self.collectionViewGrid.frame.size.height = self.collectionViewGrid.frame.size.height + (self.tabBarController?.tabBar.frame.size.height)! + 120
         self.view_imgPreview.frame.size.height = (self.tabBarController?.tabBar.frame.size.height)! + self.view_imgPreview.frame.size.height + 20
        
        let locationLogImage = Notification.Name("NotificationIdentifier")
        
        NotificationCenter.default.addObserver(self, selector: #selector(ScubaGalleryViewController.catchNotification(notification:)), name: locationLogImage, object: nil)
        
        
        
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapBlurButton(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.img_preview.isUserInteractionEnabled = true
        self.img_preview.addGestureRecognizer(tapGesture)

//        let DoubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(DoubleTapBlurButton(sender:)))
//        DoubleTapGesture.numberOfTapsRequired = 2
//        //DoubleTapGesture.numberOfTouchesRequired = 2
//        self.img_preview.isUserInteractionEnabled = true
//        self.img_preview.addGestureRecognizer(DoubleTapGesture)


//          selectedimage.image = UIImage (named:self.images[0] )
        let nc = NotificationCenter.default // Note that default is now a property, not a method call
        nc.addObserver(forName:Notification.Name(rawValue:"gallery"),
                       object:nil, queue:nil,
                       using:swipeview)
        
        
        controller = storyboard?.instantiateViewController(withIdentifier: "slidemenu") as! SidemenuViewController?
        addChildViewController(controller!)
        controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
        
        view.addSubview((controller?.view)!)
        controller?.didMove(toParentViewController: self)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        controller?.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        controller?.view.addGestureRecognizer(swipeLeft)
        
 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func Menu_Action(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)
      /*  if viewHasMovedToRight == false
            
        {
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                            self.viewHasMovedToRight = true
                            
                            self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        else
        {
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                            self.viewHasMovedToRight = false
                            
                            self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }*/
  
        
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = true
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.down:
                
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = false
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.up:
                
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    //Mark : Collectionview Delegate and Datasource
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
    
    //2
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        if(self.array_imgUrl.count > 0){
            return (self.array_imgUrl.object(at: 0) as! NSArray).count
        }
        return 0
       // return self.images.count
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //1
        //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
        //                                                      for: indexPath) as! MediaCollectionViewCell
        
 // $$      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MediaCollectionViewCell
        
        
        //2
        //cell.backgroundColor = UIColor.black
        //3
 // $$       cell.selectmediaimg.image = UIImage (named:self.images[indexPath.row] )
       
      //  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MediaCollectionViewCell
        let cell = collectionViewGrid.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MediaCollectionViewCell
        
        cell.layer.cornerRadius = 5;
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.white.cgColor
        
        cell.img.sd_setShowActivityIndicatorView(true)
        cell.img.sd_setIndicatorStyle(.gray)
        cell.img.sd_setImage(with:  URL(string: (self.array_imgUrl.object(at: 0) as! NSArray).object(at: indexPath.row) as!String))
       // cell.img.image = UIImage (named:self.images[indexPath.row] )
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let flowLayout: UICollectionViewFlowLayout? = (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)
        let cellWidth: CGFloat = collectionView.frame.width / 3
        flowLayout?.minimumInteritemSpacing = 0
        flowLayout?.minimumLineSpacing = 0//cellWidth/3
        flowLayout?.itemSize = CGSize(width: cellWidth, height: cellWidth)
        return (flowLayout?.itemSize)!
    }

    
    //1
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//        //  Converted with Swiftify v1.0.6242 - https://objectivec2swift.com/
//        let flowLayout: UICollectionViewFlowLayout? = (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)
//        
//        let availableWidthForCells: CGFloat? = collectionView.frame.width - (flowLayout?.sectionInset.left)! - (flowLayout?.sectionInset.right)! - (flowLayout?.minimumInteritemSpacing)!
//        
//        let cellWidth: CGFloat = availableWidthForCells! / 2
//        flowLayout?.itemSize = CGSize(width: cellWidth, height: 140)
//        return (flowLayout?.itemSize)!
//        
//        
//    }
//    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
       // selectedimage.image = UIImage (named:self.images[indexPath.row] )
        self.tabBarController?.tabBar.isHidden = true
       
        self.view_imgPreview.isHidden = false
        self.img_preview.sd_setShowActivityIndicatorView(true)
        self.img_preview.sd_setIndicatorStyle(.gray)
        self.img_preview.sd_setImage(with:  URL(string: (self.array_imgUrl.object(at: 0) as! NSArray).object(at: indexPath.row) as!String))
        
    }
    @IBAction func btn_img_preview_back(_ sender: Any)
    {
        self.view_imgPreview.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    func tapBlurButton(sender:UITapGestureRecognizer)
    {
        
        if viewHasMovedDown == false
            
        {
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.view_imgPreview_back.frame = CGRect(x: CGFloat(0), y: CGFloat(-self.view_imgPreview_back.frame.size.height-20), width: CGFloat(self.view_imgPreview_back.frame.size.width), height: CGFloat(self.view_imgPreview_back.frame.size.height))
                            self.viewHasMovedDown = true
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        else
        {
           // self.view_imgPreview_back.isHidden = true
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.view_imgPreview_back.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view_imgPreview_back.frame.size.width), height: CGFloat(self.view_imgPreview_back.frame.size.height))
                            self.viewHasMovedDown = false
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        
    }
    
//    func DoubleTapBlurButton(sender:UITapGestureRecognizer)
//    {
//        
//    }
    //MARK: Getting Images
    func getHistorylogs()
    {
        
        
        DispatchQueue.main.async {
            self.view .isUserInteractionEnabled = false
            Util.showindicatior(with: (self.view)!)
        }
        
        
        Model .getlogs(withSuccess: { (Result) in
            print(Result);
            
            //  var success = Result.value(forKey: "c") as? String
            let success = Result.value(forKey: "status") as? String
            
            if (success == "success" )
            {
                DispatchQueue.main.async {
                    [weak self] in
                    
                    let Message = Result.value(forKey: "message") as! String
                    DispatchQueue.main.async {
                        
                    if (Message == "Log History")
                    {
                        
                        let array  = Result.value(forKey: "data") as! NSArray
                       // print(array.value(forKey: "image") as? NSArray as Any)
                        ///var url = NSArray()
                        let array_img = NSMutableArray()
                        for var i in 0..<array.count
                        {
                            let array1 = array.object(at: i) as! NSDictionary
                           // print(array1)
            
                            if array1.value(forKey: "image") as? String != ""
                            {
                                let str = array1.value(forKey: "image") as! String
                                print(str)
                                array_img.add(str)
                            }
                            if array1.value(forKey: "image1") as? String != ""
                            {
                                let str1 = array1.value(forKey: "image1") as! String
                                array_img.add(str1)
                            }
                            if array1.value(forKey: "image2") as? String != ""
                            {
                                let str2 = array1.value(forKey: "image2") as! String
                                 array_img.add(str2)
                            }
                            
                        }
                        self?.array_imgUrl.add(array_img.mutableCopy() as! NSMutableArray)
                        print(self?.array_imgUrl as Any)
                        
                        
                      /*  if array.value(forKey: "image") as? NSArray != nil
                        {
                            url = array.value(forKey: "image") as! NSArray
                            print(url)
                           // url.adding(array.value(forKey: "image"))
                        }
                        if array.value(forKey: "image1") as? NSArray != nil
                        {
                            url.addingObjects(from: [array.value(forKey: "image1")])
                        }
                        if array.value(forKey: "image2") as? NSArray != nil
                        {
                            url.addingObjects(from: [array.value(forKey: "image2")])
                        }
                        self?.array_imgUrl.add(url.mutableCopy() as! NSMutableArray)
                        print(url)
                        print(self?.array_imgUrl as Any)*/
                        
                        self?.collectionViewGrid.reloadData()
                        self?.view .isUserInteractionEnabled = true
                        Util.stopIndicator(with: (self?.view)!)
                    }
                    else
                    {
                        
                        OperationQueue.main.addOperation {
                            [weak self] in
                            //self?.activityIndicator.stopAnimating();
                            let alert = UIAlertController(title: "Alert", message: "There is no Images", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                            self?.present(alert, animated: true, completion: nil)
                            Util.showMessage("There is no Images", withTitle: "Alert!")
                            Util.stopIndicator(with: (self?.view)!)
                           // print("no data Found");
                         //   self?.Trips = NSArray()
                            self?.view .isUserInteractionEnabled = true
                         //  self?.table_view .reloadData()
                            
                        }
                    }
                    }
                    
                }
            }
            else {
                OperationQueue.main.addOperation {
                    [weak self] in
                    //self?.activityIndicator.stopAnimating();
                    self?.view .isUserInteractionEnabled = true
                    
                    let alert = UIAlertController(title: "Alert", message: "There is no Images", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                    Util.stopIndicator(with: (self?.view)!)
                    
                }
                
            }
            
        }, failure: { (error) in
            
            
            OperationQueue.main.addOperation {
                [weak self] in
                //self?.activityIndicator.stopAnimating();
                self?.view .isUserInteractionEnabled = true
                
                let alert = UIAlertController(title: "Alert", message: error, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self?.present(alert, animated: true, completion: nil)
                Util.stopIndicator(with: (self?.view)!)
                print(error);
            }
            
        })
    }

    //MARK: Location Logs images 
    func locationImages()
    {
        self.checkLogImages = false
        self.array_imgUrl.removeAllObjects()
        Model.getLocationlogs(withSuccess: { (Result) in
        print(Result)
            
            if(Result.object(forKey: "data") != nil)
            {
                let data :NSArray = Result.object(forKey: "data") as! NSArray
                
                var urlArray = [String]()
                DispatchQueue.main.async {
                
                for i in 0..<data.count
                {
                    let demo = data.object(at: i) as! NSDictionary
                    print(demo)
                    if UserDefaults.standard.object(forKey: "locationAddress") != nil && demo.value(forKey: "location") as! String == UserDefaults.standard.object(forKey: "locationAddress") as! String
                    {
                        let urlStr = "http://beta.brstdev.com/Scubap/frontend/web/uploads/"
                        let imgUrl = demo.value(forKey: "image")! as! String
                        let url = urlStr + imgUrl
                        urlArray.append(url)
                        let imgurl1 = demo.value(forKey: "image1")! as! String
                        let url1 = urlStr + imgurl1
                        urlArray.append(url1)
                        let imgurl2 = demo.value(forKey: "image2")! as! String
                        let url2 = urlStr + imgurl2
                        urlArray.append(url2)
                        print(urlArray)
                                              // self.array_imgUrl.add(url)
                    }
                   
                }
                    if urlArray.isEmpty == true
                    {
                        Util.showMessage("There is no Image", withTitle: "Alert!")
                    }
                    self.array_imgUrl[0] = urlArray
                    self.collectionViewGrid.reloadData()
                }
                

//                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//                    self.collectionViewGrid.reloadData()
//                }
                
            }
            
        }) { (error) in
            print (error)
            
        }
        
    }
    
}

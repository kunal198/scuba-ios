
//
//  AdddLOgsViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 09/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit
import CoreLocation
import AVFoundation

class AdddLOgsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,GMSMapViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate , UIPopoverControllerDelegate, UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
  
    var popover:UIPopoverController?=nil
    
    var controller:SidemenuViewController? = nil    //hold the reference here.
    var subcatagoriesArray = NSMutableArray()
    var subcatNameArray = NSMutableArray()
    var arrayForBool = NSMutableArray()
    var selectedSection = Int()
    var select = Int()
    var selectedlong :String = ""
    var selectedlat :String = ""
    var Airtemp : String = ""
    var water_temprature : String = ""
    var depth : String = ""
    var timeunder_water : String = ""
    var visibility : String = ""
    var weight : String = ""
    var water_conditions: String = ""
    var diveType: String = ""
    var airstart: String = ""
    var airfinish: String = ""
    var divePartnr: String = ""
    var unit: String = ""
    var water_condition = ""
    var window: UIWindow?
    var activity = String()
    var type = String()
    var picker:UIImagePickerController?=UIImagePickerController()
    var url = String()
    var selectedMedia = UIImage()
    var user_id = String()
    var dataArray = NSMutableArray()
    var cell_index = 0
    var check_section = Int()
    var demoArray = NSMutableArray()
    var checkSearchedItem = false
    var Dict_searchedItems = NSDictionary()
    var arrayDiveType = NSMutableArray()
    var imageData = [String: NSData]()
    var imageUrl = [Int : String]()
    var imagesCopy = [Int : String]()
    var imgCopy = NSMutableArray()
    var img_btnTag = Int()
    var checkImgCount = false
    var checkCount = 0
    var Scuba_Tropics_copy = NSMutableArray()
    var array_spots = NSMutableArray()
    var array_searchedSpots = NSMutableArray()
    var data11 = NSDictionary()
    var array_latLong = NSMutableArray()
    var Count = Int()
    
    var images : NSMutableArray = [UIImage.init(named: "camera2")!, UIImage.init(named: "camera2")!, UIImage.init(named: "camera2")! ,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!, UIImage.init(named: "camera2")!, UIImage.init(named: "camera2")!,  UIImage.init(named: "camera2")! ,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!]
    
    @IBOutlet var btn_createLog: UIButton!
    @IBOutlet var value_view: UIView!
    @IBOutlet var value_label: UILabel!
    @IBOutlet var value_textfeild: UITextField!
    var Location_Array = NSMutableArray()
    var LocationPlacesid_Array = NSMutableArray()
    @IBOutlet var location_textfeild: UITextField!
    @IBOutlet var location_Tableview: UITableView!
 
    var tap = UITapGestureRecognizer()
    var ishome = Bool()
    @IBOutlet var datepicker_view: UIView!
    @IBOutlet var menu_img: UIImageView!
    @IBOutlet var table: UITableView!
    var viewHasMovedToRight = false
    
    @IBOutlet var date_picker: UIDatePicker!
    @IBOutlet var Back_arrowimg: UIImageView!
    @IBOutlet var loginview: UIView!
    @IBOutlet var login_img: UIImageView!
    @IBOutlet var login_label: UILabel!
    @IBOutlet var Location_view: UIView!
    @IBOutlet var heightconstraint: NSLayoutConstraint!
    @IBOutlet var lbl_addLogs_title: UILabel!
    @IBOutlet var viewTitle: UIView!
    @IBOutlet var collectionView_cards: UICollectionView!
    
    var Page = Int()
    var checkEdit = Bool()
    var log_id = Int()
    
    @IBAction func Menu_Action(_ sender: Any)
    {
      _ = navigationController?.popViewController(animated: true)
//        if checkEdit == true
//        {
//            checkEdit = false
//        }
        //self.navigationController?.popToRootViewController(animated: true)
//        var myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
//        
//
////        if  myAppDelegate?.ishome == false
////        {
////        
////            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainHome") as! ViewController
////            self.navigationController?.pushViewController(secondViewController, animated: true)
////            
////        }
////        else
////        {
//
//        if viewHasMovedToRight == false
//            
//        {
//            UIView.animate(withDuration: 0.1,
//                           delay: 0.1,
//                           options: UIViewAnimationOptions.curveEaseIn,
//                           animations: { () -> Void in
//                            self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
//                            self.viewHasMovedToRight = true
//
//            }, completion: { (finished) -> Void in
//                
//            })
//            
//        }
//        else
//        {
//            UIView.animate(withDuration: 0.1,
//                           delay: 0.1,
//                           options: UIViewAnimationOptions.curveEaseIn,
//                           animations: { () -> Void in
//                            self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
//                            self.viewHasMovedToRight = false
//                            
//                            //self.controller?.didMove(toParentViewController: self)
//            }, completion: { (finished) -> Void in
//                
//            })
//            
//        }
        
 
        
    }
    override func viewDidAppear(_ animated: Bool)
    {
            self.dataArray.removeAllObjects()
    }
    
    func swipeview(notification:Notification) -> Void {
        
        self  .swipefunction()
    }
    
    func swipefunction()
    {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                        self.viewHasMovedToRight = false
                        
                        //self.controller?.didMove(toParentViewController: self)
        }, completion: { (finished) -> Void in
    
        })
    }

  
    
    
    @IBAction func crossDate_Picker(_ sender: Any)
    {
        
        select = -1;
        
        datepicker_view.isHidden = true
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let strDate = dateFormatter.string(from: date_picker.date)
        subcatNameArray[0] = strDate
        self.table.reloadSections([0], with: .automatic)
        datepicker_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        datepicker_view.isHidden = true
        UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
            self.datepicker_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                self.datepicker_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.datepicker_view.transform = CGAffineTransform.identity
                })
            })
        })
        
    }
    
    
    
    
    
    
    @IBAction func create_Log(_ sender: Any) {
    
    if self.checkEdit == true
    {
        self.submitLog()
    }
        
    else
    {
        self.btn_createLog.titleLabel?.text = "Create Logs"
        self.btn_createLog.setTitle("Create Log", for: UIControlState.normal)
     //   let meteo = subcatNameArray.object(at: 2) as! String
        let str = subcatNameArray.object(at: 0) as! String
        if (str == "Date:" as String)
               {
                    let alert = UIAlertController(title: "Alert", message: "You must select a Date", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                
               }
               else if (selectedlat == "" && selectedlong == "")
               {
                    let alert = UIAlertController(title: "Alert", message: "Must select a Location", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
//                else if (meteo == "meteo" && meteo == "")
//                {
//  
//                        let alert = UIAlertController(title: "Alert", message: "Must select a meteo", preferredStyle: UIAlertControllerStyle.alert)
//                        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//                }
        else
        {
           
            var myArray: [Any] = str.components(separatedBy: " ")
            print(myArray[0])
            print (myArray[1])
            print (selectedlong);
            print(selectedlong);
            print (subcatNameArray.object(at: 1))
            print (USerinfo.sharedInstance.ActivityType)
            print (USerinfo.sharedInstance.logType)
            print (Airtemp);
            print (timeunder_water)
            print (water_temprature)
            print (depth)
       
            OperationQueue.main.addOperation {
                [weak self] in
                self?.view.isUserInteractionEnabled = false

                Util.showindicatior(with: (self?.view)!)
                
            }
            

            
            
          //  let user_id  = USerinfo.sharedInstance.user_id
            
           // print (user_id);
           // print(self.subcatNameArray)
            //print(self.arrayDiveType)
            let finalstring = NSMutableString()
            for i in 0..<self.arrayDiveType.count
            {
            
                var stringatindex = self.arrayDiveType[i] as! String
                if(i == self.arrayDiveType.count-1)
                {
                
                } else {
                stringatindex.append(",")
                }
                finalstring.append(stringatindex)
            }
          //  print(finalstring)
            
           water_conditions = (self.subcatNameArray[3] as? String)!
            
            
            let parameters = ["user_id":user_id,"type":USerinfo.sharedInstance.logType,"activity":USerinfo.sharedInstance.ActivityType,"meteo":subcatNameArray.object(at: 2),"location":subcatNameArray.object(at: 1) as! String,"date_time":myArray[1] as! String,"date":myArray[0] as! String,"latitude": selectedlat,"longitude" :selectedlong,"air_temprature": subcatNameArray.object(at: 5),"water_temprature": subcatNameArray.object(at: 7),"visibility": subcatNameArray.object(at: 10) as! String,"weight":subcatNameArray.object(at: 11) as! String,"water_conditions": subcatNameArray.object(at: 3) as! String,"dive_type": finalstring/*subcatNameArray.object(at: 12) as! String*/,"air_start": subcatNameArray.object(at: 13),"air_finish": subcatNameArray.object(at: 14),"timer_underwater": subcatNameArray.object(at: 6),"depth": subcatNameArray.object(at: 8) as! String,"dive_partner": divePartnr,"unit": subcatNameArray.object(at: 9) as! String]
            
            print(parameters)
            
            self.imageUploadRequest(imageDict:imageData as [String : Data] , parameters: parameters as? [String : String] , imagename: "image", keyName: "image", completion: { (result) in
                
                print(result)
                
            }, Error: { (error) in
                print("error")
                self.view .isUserInteractionEnabled = true
                Util.stopIndicator(with: (self.view)!)
                
            })
            
           /* self.imageUploadRequest(imageDict:[String:Data]/*image: selectedMedia*/, parameters: parameters as? [String : String], imagename: "image", keyName: "image", completion: { (result) in
                
                print("error")
                    
            }, Error: { (error) in
                print("error")
            })
            */
            
            }
            
       // }
        }
        
    }
  
    @IBAction func Date_PickerAction(_ sender: Any)
    {
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let strDate = dateFormatter.string(from: date_picker.date)
        subcatNameArray[0] = (strDate)
        self.table.reloadSections([0], with: .automatic)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectedMedia = UIImage()
        Location_view.isHidden = true;
        let Date = ["Date"]
        let Location = ["Location"]
        let meteo = ["Raining","Storm","Sunny","Cloudy"]
        let Airtemprature = [""]
        let Timeunderwater = [""]
        let Water_temp = [""]
        let media = url
        let depth = [""]
        let WaterConditions = ["waves","Surf","surge","Current"]
        let selectunit = ["Imperial (Fahrenheit's, Feet's, Pounds, PSI)","Metric (Celsius, Meters, Kilograms, Bars)"]
        let visibility = [""]
        let weight = [""]
        let divetype = ["Fun Dive","Rebreather","Nitrox","Boat Diving","Deep Diving","Propulsion vehicle","Drift Diving","Multilevel Diving","Night Diving","search & Recovery","underwater & Naturalist","Navigation","Photograpgy"]
        
        let airstart = [""]
        let airfinish = [""]
        self.check_section = 0
        GMSPlacesClient.provideAPIKey("AIzaSyC1RnrtDlcytpiWm78ZXBJ24iSWFqqMHcU")
        GMSServices.provideAPIKey("AIzaSyC1RnrtDlcytpiWm78ZXBJ24iSWFqqMHcU");
        subcatagoriesArray.add(Date)
        subcatagoriesArray.add(Location)
        subcatagoriesArray.add(meteo)
        subcatagoriesArray.add(WaterConditions)
        subcatagoriesArray.add(media)
        subcatagoriesArray.add(Airtemprature)
        subcatagoriesArray.add(Timeunderwater)
        subcatagoriesArray.add(Water_temp)
        subcatagoriesArray.add(depth)
        subcatagoriesArray.add(selectunit)
        subcatagoriesArray.add(visibility)
        subcatagoriesArray.add(weight)
        subcatagoriesArray.add(divetype)
        subcatagoriesArray.add(airstart)
        subcatagoriesArray.add(airfinish)
        print(subcatagoriesArray)
        
        subcatNameArray = ["Date:","Location:","Meteo:","Water Conditions","Add Media","Air Temperature:","Time Under Water:","Water Temperature:","Depth:","Select Unit:","Visibility:","Weight:","Dive Type:","Air Start:","Air Finish:"]
        self.checkSearchedItem = UserDefaults.standard.object(forKey: "checkSearchedItem") as! Bool
        if self.checkSearchedItem == true
        {
            self.Dict_searchedItems = UserDefaults.standard.object(forKey: "dict_address") as! NSDictionary
            print(self.Dict_searchedItems)
            let strData = self.Dict_searchedItems["coordinates"] as! String
            print(strData)
            let arr = strData.components(separatedBy: ",") as! NSMutableArray
            print(arr)
            self.selectedlat = arr[0] as! String
            self.selectedlong = arr[1] as! String
            print(selectedlat, selectedlong)
            
            let str = self.Dict_searchedItems["address"] as! String
            print(str)
            subcatNameArray = ["Date:",str,"Meteo:","Water Conditions","Add Media","Air Temperature:","Time Under Water:","Water Temperature:","Depth:","Select Unit:","Visibility:","Weight:","Dive Type:","Air Start:","Air Finish:"]
        }

        self.demoArray = ["Date:","Location:","Meteo:","Water Conditions","Add Media","Air Temperature:","Time Under Water:","Water Temperature:","Depth:","Select Unit:","Visibility:","Weight:","Dive Type:","Air Start:","Air Finish:"]
        
        self.user_id  = USerinfo.sharedInstance.user_id
        
        if ( USerinfo.sharedInstance.ActivityType == "Surfing")
        {


        
        }
        
        controller = storyboard?.instantiateViewController(withIdentifier: "slidemenu") as! SidemenuViewController?
        addChildViewController(controller!)
        controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
        
        view.addSubview((controller?.view)!)
        controller?.didMove(toParentViewController: self)
        
        
        let nc = NotificationCenter.default // Note that default is now a property, not a method call
        nc.addObserver(forName:Notification.Name(rawValue:"hideAddlogs"),
                       object:nil, queue:nil,
                       using:swipeview)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        controller?.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        controller?.view.addGestureRecognizer(swipeLeft)
        
        var _: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        
        
//      if  myAppDelegate?.ishome == false
//      {
       // loginview.isHidden = false
       //
 
        
      
       // heightconstraint.constant = 0
        //loginview.isHidden = true
        Back_arrowimg.isHidden = false

        menu_img.isHidden = true

       // }
        
        
        
        select = -1;
        

        
        for _ in 0..<subcatNameArray.count
        {
            arrayForBool.add(false)
        }
        
        for i in 0..<subcatNameArray.count
        {
            var parent = [String: [String]]()
            let value = ["Tommy Turner", "Wolfgang Motart"] + ["Bobby Bushe"]
            parent[subcatNameArray[i] as! String] = value
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.tabBarController?.tabBar.isHidden = false
        if self.checkEdit == true
        {
            self.btn_createLog.titleLabel?.text = "Save Log"
            self.lbl_addLogs_title.text = "Modify Log"
            self.btn_createLog.setTitle("Save Log", for: UIControlState.normal)
            self.log_id = Int((UserDefaults.standard.object(forKey: "log_id") as! NSString) as String)!
            if self.checkImgCount == false
            {
                DispatchQueue.main.async {
                    self.editLogs()
                }
            }
        }
        
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = true
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.down:
                
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = false
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.up:
                
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        
//        if (textField.tag == 8)
//        {
//            print("textField.text! = \(textField.text!)")
//            let digits = textField.text
//            textField.text = "$\(digits) Kg"
//
//            if textField.text != ""
//            {
//                let str = textField.text! + " " + "Kg"
//                textField.text = str;
//                print("str = \(str)")
//            }
            
    //    }
//        else
//        {
        if (textField.tag == 20)
        {
           // self .generateData(withKey: textField.text!)
          //  self.location_Tableview.isHidden = false
            let textsoffield = (textField.text! as NSString).replacingCharacters(in: range, with: string)
print(textsoffield)
            self.searchSpot(withKey: textsoffield)
        }
//        }
        
        return true
        
    }
   
    
    func generateData(withKey key: String)
    {
        LocationPlacesid_Array = NSMutableArray()
        let _placesClient = GMSPlacesClient()
        _placesClient.autocompleteQuery(key, bounds: nil, filter: nil, callback: {(_ results: [GMSAutocompletePrediction]?, _ error: Error?) -> Void in
            if error == nil {
                for result in results!{
                    self.Location_Array = NSMutableArray()
                    if let result = result as? GMSAutocompletePrediction{
                      //  self.arrayplaceid.addObject(result.placeID!)
                       // self.location_Tableview.isHidden = false;
                            self.Location_Array.add(result.attributedFullText.string)
                        self.LocationPlacesid_Array.add(result.placeID!)
                        print(self.Location_Array.count)
                        
                    }
                print(results as Any)
                //self.Location_Array.add(results.attributedFullText.string as Any)
                    self.location_Tableview .reloadData()
                }
                
            }
            else {
                print("error placing searching \(String(describing: error))")
            }
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        print(dataArray);
        
        if (tableView == table)
        {
        if ( USerinfo.sharedInstance.ActivityType == "Surfing")
        {
           return 5
         
        }
        else if  (USerinfo.sharedInstance.ActivityType == "Snorking")
        {
            if (USerinfo.sharedInstance.logType == "EZ")
            {
                 return 7
            }
            else
            {
                return 5
            }
         
        }
        else
        {
            if (USerinfo.sharedInstance.logType == "EZ")
            {
                return 9
            }
            else
            {
                return subcatNameArray .count
            }
        }
        
        }
        else
        {
            return 1
        }
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == table )
        {
   if (section == 0 || section == 1  || section == 4 || section == 5 || section == 6 || section == 7 || section == 8  || section == 10 || section == 11  || section == 13 || section == 14)
 {
            return 0;
    
    }
            
         if  (arrayForBool[section] as! Bool == true as Bool)
        {
                let array = subcatagoriesArray .objects(at: [section])
                self.check_section = section
                return (array[0] as! NSArray) .count

           // }
        }
        else
        {
            return 0;
            
        }
        }
        else
        {
            if Location_Array.count == 0
            {
                self.location_Tableview.isHidden = true
            }
            else
            {
                self.location_Tableview.isHidden = false
            }
         return Location_Array.count
            
        }
        
    }

    
//Pragma Mark :- Tableview Datasource :-
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (tableView == self.table)
        {
            let cell:SlidemenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! SlidemenuTableViewCell
            
            let sectionView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.table.frame.size.width ), height: CGFloat(45)))
            sectionView.removeFromSuperview()
            //        sectionView.addSubview(viewLabel)
            
            
            
            if let nameLabel = cell.viewWithTag(100) as? UILabel{
                var arr = NSArray()
             
                    arr = subcatagoriesArray[indexPath.section] as! NSArray
                    let str = arr[indexPath.row] as! String
                    nameLabel.lineBreakMode = .byWordWrapping
                    nameLabel.numberOfLines = 0
                    nameLabel.text = ""
                    nameLabel.text = (str as NSString) as String
                
                if indexPath.section == 12
                {
                    if self.arrayDiveType.contains(str)
                    {
                        cell.backgroundColor = UIColor.lightGray
                    }
                    else
                    {
                        cell.backgroundColor = UIColor.white
                    }
                }
                
//                if self.arrayIndexCount.contains(indexPath.row)
//                {
//                        cell.backgroundColor = UIColor.lightGray
//                }
//                else
//                {
//                    cell.backgroundColor = UIColor.white
//                }
              
            }
            else{
                let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.table.frame.size.width-20 ), height: CGFloat(44)))
                
                viewLabel.backgroundColor = UIColor.clear
                viewLabel.textColor = UIColor.black
                viewLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
                viewLabel.text = ""
                viewLabel.tag = 100;
              
                var arr = NSArray()
                arr = subcatagoriesArray[indexPath.section] as! NSArray
                let str = arr[indexPath.row] as! String
                viewLabel.lineBreakMode = .byWordWrapping
                viewLabel.numberOfLines = 0
                viewLabel.text = ""
                viewLabel.text = (str as NSString) as String
                if indexPath.section == 12
                {
                    if self.arrayDiveType.contains(str)
                    {
                        cell.backgroundColor = UIColor.lightGray
                    }
                    else
                    {
                        cell.backgroundColor = UIColor.white
                    }
                }

                cell.addSubview(viewLabel)
            }
            
            //   sectionView.addSubview(viewLabel)
            /********** Add a custom Separator with Section view *******************/
            let separatorLineView = UIView(frame: CGRect(x: CGFloat(10), y: CGFloat(44), width: CGFloat(self.table.frame.size.width-20), height: CGFloat(1)))
            separatorLineView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
            sectionView.addSubview(separatorLineView)
            
            
            
            
            
            return cell
            
        }
        
            
//        let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.table.frame.size.width-20 ), height: CGFloat(44)))
//        viewLabel.backgroundColor = UIColor.clear
//        viewLabel.textColor = UIColor.black
//        viewLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
//        let str1 =  "  " as String
//      //  let str2 = subcatNameArray[indexPath.row] as! String
//        
//        var array = subcatagoriesArray .objects(at: [indexPath.section])
//        
//        viewLabel.text = "";
//        var arr = array[0] as! NSArray
//        var str2 = "";
//        str2 =  arr .object(at: indexPath.row) as! String
//        let str  =   str1 + str2
//        viewLabel.text = (str as! NSString) as String
//        sectionView.addSubview(viewLabel)
//        /********** Add a custom Separator with Section view *******************/
//        let separatorLineView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(self.table.frame.size.width), height: CGFloat(1)))
//        separatorLineView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
//        viewLabel.textColor = UIColor.black
//        sectionView.addSubview(separatorLineView)
//        cell.contentView .addSubview(sectionView)
        
        
        else
        {
            let cell:SlidemenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! SlidemenuTableViewCell
            
           cell.Location_Name.text = Location_Array.object(at: indexPath.row) as? String
            
            return cell
        }
    }
    
    
    
    @IBAction func LocationTable_Cross(_ sender: Any)
    {
        select = -1;
        
        self.Location_view.isHidden = true
        self.location_Tableview.isHidden = true
        Location_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        Location_view.isHidden = true
        UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
            self.Location_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                self.Location_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.Location_view.transform = CGAffineTransform.identity
                })
            })
        })

        
        
    }
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if (tableView == table)
        {
            if self.check_section == 12
            {
                var arr1 = NSArray()
                arr1 = subcatagoriesArray[indexPath.section] as! NSArray
                let arr = arr1.mutableCopy() as! NSMutableArray
                if self.arrayDiveType.contains(arr.object(at: indexPath.row) as! String)
                {
                    //self.arrayIndexCount.remove(indexPath.row)
                    self.arrayDiveType.remove(arr.object(at: indexPath.row) )
                }
                else
                {
                   // self.arrayIndexCount.add(indexPath.row)
                    self.arrayDiveType.add(arr.object(at: indexPath.row) )
                }
                self.table.reloadData()
            }
            else
            {
            var array = subcatagoriesArray .objects(at: [indexPath.section])
            print(subcatagoriesArray .objects(at: [indexPath.section]))
            let arr = array[0] as! NSArray
            if self.check_section == 9
            {
                if indexPath.row == 0
                {
                    if cell_index == 0
                    {
                       
                    }
                    else
                    {
                        subcatNameArray[5] = demoArray[5]
                        subcatNameArray[6] = demoArray[6]
                        subcatNameArray[7] = demoArray[7]
                        subcatNameArray[8] = demoArray[8]
                        subcatNameArray[10] = demoArray[10]
                        subcatNameArray[11] = demoArray[11]
                        subcatNameArray[13] = demoArray[13]
                        subcatNameArray[14] = demoArray[14]
                        self.cell_index = 0
                        
                        self.table.reloadSectionIndexTitles()
                    }
                    
                }
                else
                {
                    if self.cell_index == 1
                    {
                     
                    }
                    else
                    {
                        subcatNameArray[5] = demoArray[5]
                        subcatNameArray[6] = demoArray[6]
                        subcatNameArray[7] = demoArray[7]
                        subcatNameArray[8] = demoArray[8]
                        subcatNameArray[10] = demoArray[10]
                        subcatNameArray[11] = demoArray[11]
                        subcatNameArray[13] = demoArray[13]
                        subcatNameArray[14] = demoArray[14]
                        self.table.reloadSections([5], with: .automatic)
                        self.table.reloadSections([6], with: .automatic)
                        self.table.reloadSections([7], with: .automatic)
                        self.table.reloadSections([8], with: .automatic)
                        self.table.reloadSections([10], with: .automatic)
                        self.table.reloadSections([11], with: .automatic)
                        self.table.reloadSections([13], with: .automatic)
                        self.table.reloadSections([14], with: .automatic)
                        
                        self.cell_index = 1
                        
                        self.table.reloadSectionIndexTitles()

                    }
                    
                }
            }
            subcatNameArray[indexPath.section] = (arr .object(at: indexPath.row))
            arrayForBool[indexPath.section] = false
            self.table.reloadSectionIndexTitles()
            self.table.reloadSections([indexPath.section], with: .automatic)
            }
        }
        else
        {
            if self.array_latLong[indexPath.row] as! String == "alert"
            {
                let alert : UIAlertController = UIAlertController(title: "Alert", message: "No GPS coordinates for this location, Please select another",preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
//                if UIDevice.current.userInterfaceIdiom == .phone
//                {
                    self.present(alert, animated: true, completion: nil)
//                }
//                else
//                {
//                    alert.modalPresentationStyle = .popover
//                    alert.popoverPresentationController!.sourceView = self.viewTitle // CRASH!!!
//                    alert.popoverPresentationController!.sourceRect = CGRect.init(x: self.view.bounds.size.width / 2 - 0.5, y:70, width: 1.0, height:  1.0)
//                    self.present(alert, animated: true, completion: nil)
//                }

            }
            else
            {
            DispatchQueue.main.async {
                
            if self.Location_Array.count > 0 //&& self.LocationPlacesid_Array.count > 0
            {
                self.location_textfeild.text = self.Location_Array.object(at: indexPath.row)as? String
                self.subcatNameArray[1] = self.Location_Array.object(at: indexPath.row)as! String
                self.arrayForBool[1] = false
                
             //   let strrr=self.Location_Array[indexPath.row]
                
              //  let strr=GMSPlacesClient()
                var str = self.array_latLong[indexPath
                    .row] as! String
                str = str.replacingOccurrences(of: "(", with: "")
                str = str.replacingOccurrences(of: ")", with: "")
                str = str.replacingOccurrences(of: " ", with: "")
                print(str)
                let coordinates = str.components(separatedBy: ",") as! NSMutableArray
                self.selectedlat = coordinates[0] as! String        //let lat = Double(coordinates[0] as! String)
                self.selectedlong = coordinates[1] as! String        //let long = Double(coordinates[1] as! String)
                
                
            //    self.lbl_lat_Long.text = str
                
//                do {
//
//                    let url = String(format: "https://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", ((strrr as! String).addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!))
//                    let result = try Data(contentsOf: URL(string: url)!)
////                    let json = JSON(data: result)
//                    let json = try! JSONSerialization.jsonObject(with: result, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
//
//                    let geome = json["results"] as! [NSDictionary]
//                    let b = geome[0].value(forKey: "geometry") as! NSDictionary
//                    let c  = b["location"] as! NSDictionary
//                    let la = c["lat"] as! Double
//                    let long = c["lng"] as! Double
//                    
//                    self.selectedlat = String(la)
//                    self.selectedlong = String(long)
//                    print(self.selectedlat, self.selectedlong)
//                    
////                    lat = json["results"][0]["geometry"]["location"]["lat"].doubleValue
////                    lon = json["results"][0]["geometry"]["location"]["lng"].doubleValue
//                    
//                }
//                catch let error{
//                    print(error)
//                }
                
//                let geoCoder = CLGeocoder()
//                geoCoder.geocodeAddressString(strrr as! String) { (placemarks, error) in
//                    guard
//                        let placemarks = placemarks,
//                        let location = placemarks.first?.location
//                        else {
//                            // handle no location found
//                            return
//                    }
//                   
//                        print("success")
//                    
//                            let lat1 = location.coordinate.latitude
//                            let long1 = location.coordinate.longitude
//                            self.selectedlat = String(lat1)
//                            self.selectedlong = String(long1)
//                            print(long1)
//                            print(lat1)
////                            print(p.formattedAddress!)
//                    
//                    // Use your location
//                }

                
     //OLD Code//**************//
                
//                
//                strr.lookUpPlaceID(strrr as! String , callback: { (results, error) in
//                    
//                    if results == nil
//                    {
//                        print("nil")
//                    }
//                    else
//                    {
//                        print("success")
//                        if let p = results {
//                            let lat1 = p.coordinate.latitude
//                            let long1 = p.coordinate.longitude
//                            self.selectedlat = String(lat1)
//                            self.selectedlong = String(long1)
//                            print(long1)
//                            print(lat1)
//                            print(p.formattedAddress!)
//                            
//                        }
//                    }
//                })

            }
            
            self.location_textfeild.endEditing(true)
            self.location_Tableview.isHidden = true;
            self.Location_view.isHidden = true;
            self.table.reloadSections([1], with: .automatic)

        }
        }
        }
    
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        print(subcatNameArray)
        print(dataArray)
        print(subcatNameArray.count)
        print(dataArray.count)
        
        for i in (0..<dataArray.count)
        {
            
            if (dataArray[i] as? NSString) != ""
            {
                subcatNameArray.replaceObject(at: i, with: dataArray[i])
            }
          // print(subcatNameArray[i])
        }
        
        print(subcatNameArray)

        
        if (tableView == table)
        {
        let sectionView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.table.frame.size.width ), height: CGFloat(50)))
        sectionView.tag = section
        let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.table.frame.size.width-20 ), height: CGFloat(45)))
        let imageview = UIImageView (frame: CGRect(x: CGFloat(self.table.frame.size.width-50), y: CGFloat(15), width: CGFloat(20 ), height: CGFloat(20)))
        
        if (section == 0)
        {
            
            imageview.image = UIImage(named: "calendar-logs")

        }
        else
        {
            imageview.image = UIImage(named: "arw-dwn")

        }
        
        imageview.contentMode = UIViewContentMode.scaleAspectFit;

       // viewLabel.backgroundColor = UIColor.lightGray
        viewLabel.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
        //yourLabel.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundImage")!)
        viewLabel.textColor = UIColor.black
        viewLabel.font = UIFont.systemFont(ofSize: CGFloat(12))
            
            viewLabel.lineBreakMode = .byWordWrapping
            viewLabel.numberOfLines = 0
            
        let str1 =  "  " as String
        let str2 = subcatNameArray[section] as? String ?? self.demoArray[section] as! String
        let str  =   str1 + str2
        viewLabel.text = (str as NSString) as String
        //viewLabel.text = subcatNameArray[section] as? String
        sectionView.addSubview(viewLabel)
        /********** Add a custom Separator with Section view *******************/
        let separatorLineView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(45), width: CGFloat(self.table.frame.size.width), height: CGFloat(5)))
        separatorLineView.backgroundColor = UIColor.white
        sectionView.addSubview(separatorLineView)
        sectionView.addSubview(imageview)
        //let red = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
        viewLabel.layer.borderColor = UIColor.lightGray.cgColor
        viewLabel.layer.borderWidth = 1
        viewLabel.layer.masksToBounds = true
        
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        sectionView.tag = section+100;
        sectionView.addGestureRecognizer(tap)
        return sectionView
        }
        else
        {
            return nil;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if (tableView == table)
        {
        return 50
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 45
    }
    
    //MARK: TextField Handler :***:
    func textFieldHandler(textField: UITextField!)
    {
        textField.placeholder = "Air Temperature "
        textField.textAlignment = .center
    }
    func water_textFieldHandler(textField: UITextField!)
    {
        if self.cell_index == 0
        {
            textField.placeholder = "Farenheits"//"Water Temperature"
            textField.textAlignment = .center
        }
        if self.cell_index == 1
        {
            textField.placeholder = "Celsius"//"Water Temperature"
            textField.textAlignment = .center
        }
        
        // textField.placeholder = "Celsius/Farenheits"//"Water Temperature"
        // textField.textAlignment = .center
    }
    func Timewater_textFieldHandler(textField: UITextField!)
    {
        textField.placeholder = "Minutes"//"Time under Water"
        textField.textAlignment = .center
    }
    func depth_textFieldHandler(textField: UITextField!)
    {
        if self.cell_index == 0
        {
            textField.placeholder = "Feets"//"Depth"
            textField.textAlignment = .center
        }
        if self.cell_index == 1
        {
            textField.placeholder = "Meter"//"Depth"
            textField.textAlignment = .center
            
        }
        //        textField.placeholder = "Meter/Feets"//"Depth"
        //        textField.textAlignment = .center
    }
    func Visibility_textFieldHandler(textField: UITextField!)
    {
        if self.cell_index == 0
        {
            textField.placeholder = "Feets"//"Depth"
            textField.textAlignment = .center
        }
        if self.cell_index == 1
        {
            textField.placeholder = "Meter"//"Depth"
            textField.textAlignment = .center
            
        }
        
        //        textField.placeholder = "Meter/Feets"//"Visibility"
        //        textField.textAlignment = .center
    }
    func Weight_textFieldHandler(textField: UITextField!)
    {
        if self.cell_index == 0
        {
            textField.placeholder = "Pounds"//"Weight in Kg"
            textField.textAlignment = .center
        }
        if self.cell_index == 1
        {
            textField.placeholder = "Kilograms"//"Weight in Kg"
            textField.textAlignment = .center
        }
        
        //        textField.placeholder = "Kilograms/Pounds"//"Weight in Kg"
        //        textField.textAlignment = .center
    }
    
    func AirStart_textFieldHandler(textField: UITextField!)
    {
        if self.cell_index == 0
        {
            textField.placeholder = "(BAR at 170)"//"Air Start"
            textField.textAlignment = .center
            
        }
        if self.cell_index == 1
        {
            textField.placeholder = "(Preset PST at 2400)"//"Air Start"
            textField.textAlignment = .center
            
        }
        
        //        textField.placeholder = "(Preset PST at 2400 / BAR at 170)"//"Air Start"
        //        textField.textAlignment = .center
    }
    func Airfinish_textFieldHandler(textField: UITextField!)
    {
        if self.cell_index == 0
        {
            textField.placeholder = "(BAR at 35)"//"Air Finish"
            textField.textAlignment = .center
        }
        if self.cell_index == 1
        {
            textField.placeholder = "(Preset PST at 500)"//"Air Finish"
            textField.textAlignment = .center        }
        
        //        textField.placeholder = "(Preset PST at 500 / BAR at 35)"//"Air Finish"
        //        textField.textAlignment = .center
    }

    
    
    
    func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
        location_textfeild.resignFirstResponder()
       selectedSection = (gestureRecognizer.view?.tag)! as Int
        print (selectedSection)
        selectedSection = selectedSection - 100
        
       print(select)
        if (selectedSection == 0 )
        {
            if (select == selectedSection)
            
            {
                select = -1;

                datepicker_view.isHidden = true
                
                datepicker_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                datepicker_view.isHidden = true
                UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
                    self.datepicker_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                        self.datepicker_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
                    }, completion: {(_ finished: Bool) -> Void in
                        UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                            self.datepicker_view.transform = CGAffineTransform.identity
                        })
                    })
                })
            }
                
            else
            {
                
           select = selectedSection
                location_Tableview.isHidden = true;
            datepicker_view.isHidden = false
            datepicker_view.isHidden = false
            datepicker_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
            //Activity_Indicator
            UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
                self.datepicker_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.datepicker_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                        self.datepicker_view.transform = CGAffineTransform.identity
                    })
                })
            })
            self.view .addSubview(datepicker_view)
        }
        }
        
        if (selectedSection == 1)
        {
//            if self.checkSearchedItem == true
//            {
//                
//            }
//            else
//            {
            location_textfeild.text = ""
            location_textfeild.tag = 20
            if (select == selectedSection)
                
            {
                select = -1;
                
                Location_view.isHidden = true
                self.location_Tableview.isHidden = true

                Location_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                Location_view.isHidden = true
                UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
                    self.Location_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                        self.Location_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
                    }, completion: {(_ finished: Bool) -> Void in
                        UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                            self.Location_view.transform = CGAffineTransform.identity
                        })
                    })
                })
            }
            else
            {
                select = selectedSection
                Location_view.isHidden = false
                Location_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                //Activity_Indicator
                UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
                    self.Location_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                        self.Location_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
                    }, completion: {(_ finished: Bool) -> Void in
                        UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                            self.Location_view.transform = CGAffineTransform.identity
                        })
                    })
                })
                self.view .addSubview(Location_view)
            }
      //  }
        }
        
        if (selectedSection == 4)
        {
            if self.Count == 3
            {
                let alert1 : UIAlertController = UIAlertController(title: "Alert", message : "Maximum limit for Media is 3", preferredStyle: UIAlertControllerStyle.alert)
                alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert1, animated: true, completion: nil)
                
            }
            else
            {
            let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.checkImgCount = true
                self.openCamera()
            }
            let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.checkImgCount = true
                self.openGallary()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
            }
            
            // Add the actions
            picker?.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            // Present the controller
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                alert.modalPresentationStyle = .popover
                alert.popoverPresentationController!.sourceView = self.viewTitle // CRASH!!!
                alert.popoverPresentationController!.sourceRect = CGRect.init(x: self.view.bounds.size.width / 2 - 0.5, y:70, width: 1.0, height:  1.0)
                self.present(alert, animated: true, completion: nil)
            }
            }
        }
        
        
        
        if (selectedSection == 5)
        {
            
            let alert = UIAlertController(title: "Air Temperature", message: "Please Enter Air Temperature", preferredStyle:
                UIAlertControllerStyle.alert)
            
            alert.addTextField {[weak self] (textField: UITextField!) in

                textField.keyboardType = UIKeyboardType.numberPad;
                textField.delegate = self
                textField.tag = 5
            }
            
            
          //  alert.addTextField(configurationHandler: textFieldHandler)
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                
                
                let Airtempvalue = alert.textFields![0] as UITextField
                
                if Airtempvalue.text != "" {
                    
                    self.Airtemp = Airtempvalue.text!
                 
                    self.subcatNameArray[5] = self.Airtemp
                    self.arrayForBool[5] = false
                    self.table.reloadSections([5], with: .automatic)
                    
                } else
                {
                    
                    
                }
                
            }))
            
            self.present(alert, animated: true, completion:nil)
        }
        if (selectedSection == 6)
        {
            let alert1 = UIAlertController(title: "Time under Water", message: "Please Enter Time you spend under Water", preferredStyle:
                UIAlertControllerStyle.alert)

            alert1.addTextField {[weak self] (textField: UITextField!) in
                textField.delegate = self
                textField.keyboardType = UIKeyboardType.numberPad;

                textField.tag = 6
            }
            
         //   alert1.addTextField(configurationHandler: Timewater_textFieldHandler)
            
            alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                

                let teXtfeild = alert1.textFields![0] as UITextField
                
                if teXtfeild.text != ""
                {
                    
                    self.timeunder_water = teXtfeild.text!
                    self.subcatNameArray[6] = self.timeunder_water
                    self.arrayForBool[6] = false
                    self.table.reloadSections([6], with: .automatic)
                    
                } else
                {
                    
    
                }
                
            }))
            
            self.present(alert1, animated: true, completion:nil)
        }
        
        if (selectedSection == 7)
        {
            let alert1 = UIAlertController(title: "Water Temperature", message: "Please Enter water Temperature", preferredStyle:
                UIAlertControllerStyle.alert)
            
            alert1.addTextField {[weak self] (textField: UITextField!) in
                textField.delegate = self
                textField.keyboardType = UIKeyboardType.numberPad;

                textField.tag = 7
            }
            
          //  alert1.addTextField(configurationHandler: water_textFieldHandler)
            
            alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                
                
                let watertempvalue = alert1.textFields![0] as UITextField
                
                if watertempvalue.text != "" {
                    
                    self.water_temprature = watertempvalue.text!
                    self.subcatNameArray[7] = self.water_temprature
                    self.arrayForBool[7] = false
                    self.table.reloadSections([7], with: .automatic)
                    
                } else
                {
                    
                    
                }
                
            }))
            
            self.present(alert1, animated: true, completion:nil)
        }
        
        if (selectedSection == 8)
        {
            let alert1 = UIAlertController(title: "Depth", message: "Please Enter Depth of Water you undergo", preferredStyle:
                UIAlertControllerStyle.alert)
            
            alert1.addTextField {[weak self] (textField: UITextField!) in
                textField.delegate = self
                textField.keyboardType = UIKeyboardType.numberPad;

                textField.tag = 8
            }
            
          //  alert1.addTextField(configurationHandler: depth_textFieldHandler)
            
            alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                
                let watertempvalue = alert1.textFields![0] as UITextField
                
                if watertempvalue.text != "" {
                    
                    self.depth = watertempvalue.text!
                    self.subcatNameArray[8] = self.depth
                    self.arrayForBool[8] = false
                    self.table.reloadSections([8], with: .automatic)
                    
                }
                else
                {
                    
                    
                }
                
            }))
            
            self.present(alert1, animated: true, completion:nil)
        }
        
        
        if (selectedSection == 10)
        {
            let alert1 = UIAlertController(title: "Visibility", message: "Please Enter Visibility", preferredStyle:
                UIAlertControllerStyle.alert)
            
            
            alert1.addTextField {[weak self] (textField: UITextField!) in
                textField.delegate = self
                textField.keyboardType = UIKeyboardType.numberPad;

                textField.tag = 10
            }
            
           // alert1.addTextField(configurationHandler: Visibility_textFieldHandler)
            alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                
                
                let visibilityvalue = alert1.textFields![0] as UITextField
                
                if visibilityvalue.text != "" {
                    
                    self.visibility = visibilityvalue.text!
                    self.subcatNameArray[10] = self.visibility
                    self.arrayForBool[10] = false
                    self.table.reloadSections([10], with: .automatic)
                    
                }
                else
                {
                    
                    
                }
                
            }))
            
            self.present(alert1, animated: true, completion:nil)
        }
        
        if (selectedSection == 11)
        {
            let alert1 = UIAlertController(title: "Weight", message: "Please Enter Weight", preferredStyle:
                UIAlertControllerStyle.alert)
            
            alert1.addTextField {[weak self] (textField: UITextField!) in
                textField.delegate = self
                textField.keyboardType = UIKeyboardType.numberPad;

                textField.tag = 11
            }
            
           // alert1.addTextField(configurationHandler: Weight_textFieldHandler)
            
            alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                
                
                let weight_value = alert1.textFields![0] as UITextField
                
                if weight_value.text != "" {
                    
                    self.weight = weight_value.text!
                    self.subcatNameArray[11] = self.weight
                    self.arrayForBool[11] = false
                    self.table.reloadSections([11], with: .automatic)
                    
                }
                else
                {
                    
                    
                }
                
            }))
            
            self.present(alert1, animated: true, completion:nil)
        }
        if (selectedSection == 13)
        {
            let alert1 = UIAlertController(title: "Air Start", message: "Please Enter Air Start (PSI/BAR)", preferredStyle:
                UIAlertControllerStyle.alert)
            
            alert1.addTextField {[weak self] (textField: UITextField!) in
                textField.delegate = self
                textField.keyboardType = UIKeyboardType.numberPad;

                textField.tag = 13
            }
            
           // alert1.addTextField(configurationHandler: AirStart_textFieldHandler)
            
            alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                
                
                let Airstart_value = alert1.textFields![0] as UITextField
                
                if Airstart_value.text != "" {
                    
                    self.airstart = Airstart_value.text!
                    self.subcatNameArray[13] = self.airstart
                    self.arrayForBool[13] = false
                    self.table.reloadSections([13], with: .automatic)
                    
                }
                else
                {
                    
                }
                
            }))
            
            self.present(alert1, animated: true, completion:nil)
        }
        
        if (selectedSection == 14)
        {
            let alert1 = UIAlertController(title: "Air Finish", message: "Please Enter Air Finish (PSI/BAR)", preferredStyle:
                UIAlertControllerStyle.alert)
            
            alert1.addTextField {[weak self] (textField: UITextField!) in
                textField.delegate = self
                textField.keyboardType = UIKeyboardType.numberPad;

                textField.tag = 14
            }
            
           // alert1.addTextField(configurationHandler: Airfinish_textFieldHandler)
            
            alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                
                
                let airfinishvalue = alert1.textFields![0] as UITextField
                
                if airfinishvalue.text != "" {
                    
                    self.airfinish = airfinishvalue.text!
                    self.subcatNameArray[14] = self.airfinish
                    self.arrayForBool[14] = false
                    self.table.reloadSections([14], with: .automatic)
                }
                else
                {
                    
                    
                }
                
            }))
            
            self.present(alert1, animated: true, completion:nil)
        }

        
        
      self.table.reloadData()
        
        let indexPath = IndexPath(row: 0, section: selectedSection) as NSIndexPath
        if indexPath.row == 0 {
            let collapsed: Bool = arrayForBool[indexPath.section] as! Bool
            for i in 0..<subcatNameArray.count {
                if indexPath.section == i {
                    arrayForBool[i] = (!collapsed)
                }
            }
            
            if (selectedSection == 0 || selectedSection == 1 ||  selectedSection == 4 || selectedSection == 8  || selectedSection == 5 || selectedSection == 6 || selectedSection == 7 || selectedSection == 10 || selectedSection == 11 || selectedSection == 13 || selectedSection == 14)
            {
    

            }
            else
            {

               // DispatchQueue.main.async {
                    self.table.reloadSections([self.selectedSection], with: .automatic)
                    let collapse: Bool = self.arrayForBool[indexPath.section] as! Bool
                    
                    if (collapse == true)
                    {
                        let indexPath = IndexPath(row: 0, section: self.selectedSection)
                        
                        self.table.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                    }
              //  }
   
           //  DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if self.selectedSection == 12
                {
                        // your code here
                        self.table.reloadData()
                }
                }
            
           // }
            
      }

    }
    
    func editLogs()
    {
        var request = URLRequest(url: URL(string: "http://beta.brstdev.com/Scubap/api/web/index.php/v1/gethistory")!)
        request.httpMethod = "POST"
        
        let postString = "user_id=\(user_id)"
        request.httpBody = postString.data(using: .utf8)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            print(json!)
            
            let data1 = json?.value(forKey: "data") as! NSArray
            print(data1)
            for i in 0..<data1.count
            {
                let demo = data1[i] as! NSDictionary
                print(demo)
                print(self.log_id)
                print(demo.value(forKey: "id")!)
                var log_id = Int()
                log_id = Int((demo.value(forKey: "id") as! NSString) as String)!
                
                if log_id == self.log_id
                {
                    let activity = demo.value(forKey: "activity") as! NSString
                    USerinfo.sharedInstance.ActivityType = activity as String
                    print(USerinfo.sharedInstance.ActivityType)
                    print(demo)
//                    if activity == "Surfing"
//                    {
                    self.imageUrl.removeAll()
                    self.Count = 0
                    DispatchQueue.main.async {
                        
                        if demo.value(forKey: "image") as? NSString != ""
                        {
                            //count += 1
                            self.imageUrl[self.Count] = demo.value(forKey: "image") as? String
                        }
                        if demo.value(forKey: "image1") as? NSString != ""
                        {
                            self.Count += 1
                            self.imageUrl[self.Count] = demo.value(forKey: "image1") as? String
                        }
                        if demo.value(forKey: "image2") as? NSString !=  ""
                        {
                            self.Count += 1
                            self.imageUrl[self.Count] = demo.value(forKey: "image2") as? String
                        }
                        print(self.imageUrl)
                        let air_finish = (demo.value(forKey: "air_finish") as? NSString ?? "Air Finish" )
                        let air_start = demo.value(forKey: "air_start") as? NSString ?? "Air Start" 
                        let air_temp = demo.value(forKey: "air_temprature") as? NSString ?? "Air Temperature"
                        let date = demo.value(forKey: "date") as? NSString ?? "Date"
                        let time = demo.value(forKey: "date_time") as? NSString ?? "Date"
                        let depth = demo.value(forKey: "depth") as? NSString ?? "Depth"
                        let dive_partner = demo.value(forKey: "dive_partner") as? NSString ?? "Dive"
                        let dive_type = demo.value(forKey: "dive_type") as? NSString ?? "Dive Type"
                        let imageUrl1 = demo.value(forKey: "image") as? NSString ?? "Add Image"
                        let latitude = demo.value(forKey: "latitude") as? NSString ?? "location-lat"
                        let longitude = demo.value(forKey: "longitude") as? NSString ?? "locationlong"
                        let location = demo.value(forKey: "location") as? NSString ?? "Location"
                        let meteo = demo.value(forKey: "meteo") as? NSString ?? "Meteo"
                        let time_underwater = demo.value(forKey: "timer_underwater") as? NSString ?? "Time UnderWater"
                        let type = demo.value(forKey: "type") as? NSString ?? "Type"
                        let unit = demo.value(forKey: "unit") as? NSString ?? "Select Unit"
                        let visibility = demo.value(forKey: "visibility") as? NSString ?? "Visibility"
                        let water_conditions = demo.value(forKey: "water_conditions") as? NSString ?? "Water Conditions"
                        let water_temp = demo.value(forKey: "water_temprature") as? NSString ?? "Water Temperature"
                        let weight = demo.value(forKey: "weight") as? NSString ?? "Weight"
                        
                        print(air_finish, air_start, air_temp, date, time, depth, dive_partner, dive_type, imageUrl1,latitude, longitude, location, meteo, time_underwater, type, unit, visibility, water_conditions, water_temp, weight)
                        let str1 = String(date)
                        let str2 = " "
                        let str3 = String(time)
                        let str = str1 + str2 + str3 as NSString
                        self.dataArray.add(str)
                        self.dataArray.add(location)
                        self.dataArray.add(meteo)
                        self.dataArray.add(water_conditions)
                        var media = String()
                        self.Count += 1
                        media = ("Add Media \("(3,")\(self.Count))")
                        self.dataArray.add(media)
                        self.dataArray.add(air_temp)
                        self.dataArray.add(time_underwater)
                        self.dataArray.add(water_temp)
                        self.dataArray.add(depth)
                        self.dataArray.add(unit)
                        self.dataArray.add(visibility)
                        self.dataArray.add(weight)
                        self.dataArray.add("Dive Type:")  //dive_type
                        self.dataArray.add(air_start)
                        self.dataArray.add(air_finish)
                    
                    
                    let strDive = String(dive_type)
                    let arr1 = strDive.components(separatedBy: ",") as NSArray  //as! NSMutableArray
                    self.arrayDiveType = arr1.mutableCopy() as! NSMutableArray
                    print(self.arrayDiveType)
                    
                    self.table.reloadData()
                    self.collectionView_cards.reloadData()
                    }
                }
            }
         
        }
        task.resume()
    }
    
    //MARK: - ImagePicker Delegate And Functions
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
                picker!.sourceType = UIImagePickerControllerSourceType.camera
                self .present(picker!, animated: true, completion: nil)
            } else {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
                    if granted == true {
                        self.picker!.sourceType = UIImagePickerControllerSourceType.camera
                        self .present(self.picker!, animated: true, completion: nil)
                    } else {
                        let alert = UIAlertController(title: "This app does not have access to your Camera", message: "You can enable access in Privacy Settings", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                })
            }
            
            
        }

        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            picker?.delegate = self
            picker?.sourceType = .savedPhotosAlbum;
            picker?.allowsEditing = true
            
            self.present(picker!, animated: true, completion: nil)
        }
        else
        {
//                        popover=UIPopoverController(contentViewController: picker!)
//                        popover!.presentPopoverFromRect(btnClickMe.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        var originalImage:UIImage?, editedImage:UIImage?, imageToSave:UIImage?
        
        
        editedImage = info[UIImagePickerControllerEditedImage] as! UIImage?
        originalImage = info[UIImagePickerControllerOriginalImage] as! UIImage?
        
        if ( editedImage != nil ) {
            imageToSave = editedImage
        } else {
            imageToSave = originalImage
        }
        // imgView.image = imageToSave
        // imgView.reloadInputViews()
        

        
        
        picker .dismiss(animated: true, completion: nil)
        
        let image1 = imageToSave
        self.selectedMedia = image1!
        
        let keys = Array(imageUrl.keys)
        //print(keys)
        if keys.contains(self.img_btnTag)
        {
            self.imageUrl.removeValue(forKey: self.img_btnTag)
            print(self.imageUrl)
        }
        self.images.replaceObject(at: self.img_btnTag, with: imageToSave!)

        var media = String()
        Count += 1
        if Count > 3
        {
            Count = 3
        }
        media = ("Add Media \("(3,")\(self.Count))")
        subcatNameArray[4] = media
      
    
        if self.img_btnTag == 0
        {
            imageData["image"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            self.images.add(image1!)
        }
        if self.img_btnTag == 1
        {
            imageData["image1"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            self.images.add(image1!)
        }
        if self.img_btnTag == 2
        {
            imageData["image2"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            self.images.add(image1!)
        }
        self.img_btnTag += 1
        DispatchQueue.main.async {
            self.collectionView_cards.reloadData()
            self.table.reloadSectionIndexTitles()
            self.table.reloadSections([Int(4.4)], with: .automatic)
        }
        print(imageData)
      //  User_img.image=info[UIImagePickerControllerOriginalImage] as? UIImage
      //  User_img.layer.cornerRadius = User_img.frame.size.width / 2
      //  User_img.clipsToBounds = true
    }
    public  func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
        print("picker cancel.")
    }

    
  
    func imageUploadRequest (imageDict:[String:Data] /*image:UIImage!*/, parameters: [String: String]! ,imagename: String, keyName:String, completion:@escaping (_ result: NSDictionary?) -> (),  Error:@escaping (NSError) -> ()){
      
        var myUrl = NSURL(string:"http://beta.brstdev.com/Scubap/api/web/index.php/v1/createlog");

        if self.checkEdit == true {
            myUrl = NSURL(string:"http://beta.brstdev.com/Scubap/api/web/index.php/v1/updatelog");
            
        }
        
        let request = NSMutableURLRequest(url: myUrl! as URL)
        
        request.httpMethod = "POST";
        print(imageDict)
        print(parameters)
        let boundary = "myRandomBoundary12345"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = createRequestBodyWith(parameters: parameters, imageDict: imageDict)
        
        //var imageData = NSData()
//        if self.selectedMedia.size != CGSize.init(width: 0, height: 0)
//        {
//            imageData  = UIImageJPEGRepresentation(self.selectedMedia, 1)! as NSData
//            request.httpBody = createRequestBodyWith(parameters: parameters, imageDict: imageDict)
//
//           // request.httpBody = createBodyWithParameters(parameters: parameters, keyName: keyName, imageDataKey: imageData, boundary: boundary, filename: "\(imagename).jpg", mimetype: "image/jpg") as Data
//        }else
//        {
//            request.httpBody = createRequestBodyWith(parameters: parameters, imageDict: imageDict)
//
//          // request.httpBody = createBodyWithParameters(parameters: parameters, keyName: keyName, imageDataKey: nil, boundary: boundary, filename: "", mimetype: "image/jpg") as Data
//        }
        
     
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
                Error(error! as NSError)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(json!)
                
                completion(json)
                                    OperationQueue.main.addOperation {
                                        [weak self] in
                                        self?.view.isUserInteractionEnabled = true
                
                                        Util.stopIndicator(with: (self?.view)!)
                
                                    }
                
                                    let alertController = UIAlertController(title: "Alert!", message: "Logs Created successfully", preferredStyle: .alert)
                                    // Create the actions
                                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                                        UIAlertAction in
                
                                   _ = self.navigationController?.popViewController(animated: true)
                
                                    }
                                    // Add the actions
                                    alertController.addAction(okAction)
                                    // Present the controller
                                    self.present(alertController, animated: true, completion: nil)
                
            }
            
            catch {
                Error(error as NSError)
                OperationQueue.main.addOperation {
                                        [weak self] in
                                        self?.view.isUserInteractionEnabled = true
                                        MBProgressHUD.hideAllHUDs(for: (self?.view)!, animated: true);
                                        Util.showMessage(error as! String, withTitle: "Alert!")
                                        
                                    }

                
                
                
            }
            
        }
        
        task.resume()
        
    }
    func createRequestBodyWith(parameters:[String:String]?, imageDict:[String:Data]?) -> Data{
        
        let boundaryConstant = "myRandomBoundary12345";
        
        
        var uploadData = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: .utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                uploadData.append("\(value)\r\n".data(using: .utf8)!)
            }
        }
        
        if imageDict != nil {
            for (key, value) in imageDict! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: .utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(key).jpg\"\r\n".data(using: .utf8)!)
                uploadData.append("Content-Type: image/jpg\r\n\r\n".data(using: .utf8)!)
                uploadData.append(value)
                uploadData.append("\r\n".data(using: .utf8)!)
            }
        }
        uploadData.append("--\(boundaryConstant)--\r\n".data(using: .utf8)!)
        
        
        
        return uploadData
        
    }
  /*
    func createBodyWithParameters(parameters: [String: String]?, keyName:String, imageDataKey: NSData?, boundary: String,filename:String, mimetype:String) -> NSData {
        
      // sending image in multiparts
        
        var body = ""
        
        if let params = parameters {
            for (key, value) in params {
                body += "--\(boundary)\r\n"
                body += "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n"
                body += "\(value)\r\n"
            }
        }
        
     //   if( imageDataKey != nil){
            
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body += "--\(boundary)\r\n"
        body += "Content-Disposition: form-data; name=\"\(keyName)\"; filename=\"\(filename)\"\r\n"
        body += "Content-Type: \(mimetype)\r\n\r\n"
       
      //  }
        

        var data = body.data(using: .utf8)!
        // check the image
        if( imageDataKey != nil){
            data.append(imageDataKey! as Data)
        }
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        return data as NSData
        
    }
    */
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

    func submitLog()
    {
        var request = URLRequest(url: URL(string: "http://beta.brstdev.com/Scubap/api/web/index.php/v1/updatelog")!)
        request.httpMethod = "POST"
        
        
        let str = subcatNameArray.object(at: 0) as! String
        
        var myArray: [Any] = str.components(separatedBy: " ")
        print(myArray[0])
        print (myArray[1])
        
        print(subcatNameArray)
        
        let finalstring = NSMutableString()
        if self.arrayDiveType.count > 1
        {
            
            for i in 0..<self.arrayDiveType.count
            {
            
                var stringatindex = self.arrayDiveType[i] as! String
                if(i == self.arrayDiveType.count-1)
                {
                
                } else {
                    stringatindex.append(",")
                }
                finalstring.append(stringatindex)
            }
        }
        
//       let postString = String(format:"user_id=\(user_id)&log_id=\(self.log_id)&date_time=\(myArray[1] as! String)&date=\(myArray[0] as! String)&location=\(self.subcatNameArray[1] as! String)&meteo=\(self.subcatNameArray[2] as! String )&water_conditions=\(self.subcatNameArray[3] as! String)&image=\(self.subcatNameArray[4] as! String)&air_temprature=\(self.subcatNameArray[5] as! String)&timer_underwater=\(self.subcatNameArray[6] as! String)&water_temprature=\(self.subcatNameArray[7] as! String)&depth=\(self.subcatNameArray[8] as! String)&unit=\(self.subcatNameArray[9] as! String)&visibility=\(self.subcatNameArray[10] as! String)&weight=\(self.subcatNameArray[11] as! String)&dive_type=\(self.subcatNameArray[12] as! String)&air_start=\(self.subcatNameArray[13] as! String)&air_finish=\(self.subcatNameArray[14] as! String)" as String)
//        
//        
//     print(postString)
        
        
        let parameters = ["user_id":user_id,"log_id":"\(self.log_id)","meteo":subcatNameArray.object(at: 2),"location":subcatNameArray.object(at: 1) as! String,"date_time":myArray[1] as! String,"date":myArray[0] as! String,"latitude": selectedlat,"longitude" :selectedlong,"air_temprature": subcatNameArray.object(at: 5),"water_temprature": subcatNameArray.object(at: 7),"visibility": subcatNameArray.object(at: 10) as! String,"weight":subcatNameArray.object(at: 11) as! String,"water_conditions": subcatNameArray.object(at: 3) as! String,"dive_type": finalstring/*subcatNameArray.object(at: 12) as! String*/,"air_start": subcatNameArray.object(at: 13),"air_finish": subcatNameArray.object(at: 14),"timer_underwater": subcatNameArray.object(at: 6),"depth": subcatNameArray.object(at: 8) as! String,"dive_partner": divePartnr,"unit": subcatNameArray.object(at: 9) as! String]
        
        print(parameters)
        
        self.imageUploadRequest(imageDict:imageData as [String : Data] , parameters: parameters as? [String : String] , imagename: "image", keyName: "image", completion: { (result) in
            
            print("error")
            
        }, Error: { (error) in
            print("error")
            self.view .isUserInteractionEnabled = true
            Util.stopIndicator(with: (self.view)!)
            
        })
        
      /*  self.imageUploadRequest(image: selectedMedia, parameters: parameters as? [String : String] , imagename: "image", keyName: "image", completion: { (result) in
          //  self.checkEdit = false

            print("error")
            
        }, Error: { (error) in
            print("error")
        })*/

        
//        let postString:[String:Any]? = ["user_id":user_id,"log_id":self.log_id,"location":self.subcatNameArray[1] as! String,"meteo":self.subcatNameArray[2] as! String,"water_conditions":self.subcatNameArray[3] as! String,"image":self.subcatNameArray[4] as! String ,"air_temprature": self.subcatNameArray[5] as! String, "timer_underwater":self.subcatNameArray[6] as! String,"water_temprature":self.subcatNameArray[7] as! String, "depth":self.subcatNameArray[8] as! String, "unit": self.subcatNameArray[9] as! String, "visibility": self.subcatNameArray[10] as! String, "weight":self.subcatNameArray[11] as! String, "dive_type": self.subcatNameArray[12] as! String, "air_start": self.subcatNameArray[13] as! String, "air_finish": self.subcatNameArray[14] as! String  ]
        
        
   /*
       let post:String  = postString as String
        
        request.httpBody = post.data(using: .utf8)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-.")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            print(data!)
            guard let data = data, error == nil else {
                return
            }
            
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                print(json!)
                print(self.subcatNameArray)
            }
            catch {
               // Error(error as NSError)
//                OperationQueue.main.addOperation {
//                    [weak self] in
//                    self?.view.isUserInteractionEnabled = true
//                    MBProgressHUD.hideAllHUDs(for: (self?.view)!, animated: true);
//                    Util.showMessage(error as! String, withTitle: "Alert!")
//                    
//                }
         
                
            }
  

            
            
            DispatchQueue.main.async {
                
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "history") as! HistoricalViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
             
            }
            
        }
        
        task.resume()
 */

    }
    
    //MARK: Text field Delegates :-
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
        
    {
        textField.resignFirstResponder()
        
    }

    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
       
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 5
        {
            if self.cell_index == 0
            {
                textField.placeholder = "Farenheits"//"Water Temperature"
                textField.textAlignment = .center
            }
            if self.cell_index == 1
            {
                textField.placeholder = "Celsius"//"Water Temperature"
                textField.textAlignment = .center
            }
            
            // textField.placeholder = "Celsius/Farenheits"//"Water Temperature"
            // textField.textAlignment = .center
        }
        if textField.tag == 6
        {
            textField.placeholder = "Minutes"//"Time under Water"
            textField.textAlignment = .center
        }
        if textField.tag == 7
        {
            if self.cell_index == 0
            {
                textField.placeholder = "Farenheits"//"Water Temperature"
                textField.textAlignment = .center
            }
            if self.cell_index == 1
            {
                textField.placeholder = "Celsius"//"Water Temperature"
                textField.textAlignment = .center
            }
            
            // textField.placeholder = "Celsius/Farenheits"//"Water Temperature"
            // textField.textAlignment = .center
        }
        if textField.tag == 8
        {
            if self.cell_index == 0
            {
                textField.placeholder = "Feets"//"Depth"
                textField.textAlignment = .center
            }
            if self.cell_index == 1
            {
                textField.placeholder = "Meter"//"Depth"
                textField.textAlignment = .center
                
            }
            //        textField.placeholder = "Meter/Feets"//"Depth"
            //        textField.textAlignment = .center
        }
        if textField.tag == 10
        {
            if self.cell_index == 0
            {
                textField.placeholder = "Feets"//"Depth"
                textField.textAlignment = .center
            }
            if self.cell_index == 1
            {
                textField.placeholder = "Meter"//"Depth"
                textField.textAlignment = .center
                
            }
            
            //        textField.placeholder = "Meter/Feets"//"Visibility"
            //        textField.textAlignment = .center

        }
        if textField.tag == 11
        {
            if self.cell_index == 0
            {
                textField.placeholder = "Pounds"//"Weight in Kg"
                textField.textAlignment = .center
            }
            if self.cell_index == 1
            {
                textField.placeholder = "Kilograms"//"Weight in Kg"
                textField.textAlignment = .center
            }
            
            //        textField.placeholder = "Kilograms/Pounds"//"Weight in Kg"
            //        textField.textAlignment = .center

        }
        if textField.tag == 13
        {
            if self.cell_index == 1
            {
                textField.placeholder = "170"//"Air Start"
                textField.textAlignment = .center
                
            }
            if self.cell_index == 0
            {
                textField.placeholder = "2400"//"Air Start"
                textField.textAlignment = .center
                
            }
            
            //        textField.placeholder = "(Preset PST at 2400 / BAR at 170)"//"Air Start"
            //        textField.textAlignment = .center

        }
        if textField.tag == 14
        {
            if self.cell_index == 1
            {
                textField.placeholder = "35"//"Air Finish"
                textField.textAlignment = .center
            }
            if self.cell_index == 0
            {
                textField.placeholder = "500"//"Air Finish"
                textField.textAlignment = .center        }
            
            //        textField.placeholder = "(Preset PST at 500 / BAR at 35)"//"Air Finish"
            //        textField.textAlignment = .center

        }

        
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 5
        {
            if self.cell_index == 0 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) °F"
            }
            if self.cell_index == 1 && textField.text != ""
            {
                if textField.text != ""
                {
                    let digits = textField.text!
                    textField.text = "\(digits) °C"
                }
            }
        }
        if textField.tag == 6
        {
                let digits = textField.text!
                textField.text = "\(digits) MIN"
        }
        if textField.tag == 7
        {
            if self.cell_index == 0 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) °F"
            }
            if self.cell_index == 1 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) °C"
            }
        }
        if textField.tag == 8
        {
            if self.cell_index == 0 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) ft"
            }
            if self.cell_index == 1 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) m"
            }
        }
        if textField.tag == 10
        {
            if self.cell_index == 0 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) ft"
            }
            if self.cell_index == 1 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) m"
            }
        }
        if textField.tag == 11
        {
            if self.cell_index == 0 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) Lbs"
            }
            if self.cell_index == 1 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) Kg"
            }
        }
        if textField.tag == 13
        {
            if self.cell_index == 0 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) PSI"
            }
            if self.cell_index == 1 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) Bar"
            }
        }
        if textField.tag == 14
        {
            if self.cell_index == 0 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) PSI"
            }
            if self.cell_index == 1 && textField.text != ""
            {
                let digits = textField.text!
                textField.text = "\(digits) Bar"
            }
        }
        
        return true;
    }
   
    //MARK: Collectionview Delegate and Datasource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView_cards.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MediaCollectionViewCell
        
        cell.layer.cornerRadius = 5;
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.white.cgColor
        
        cell.img.image = self.images[indexPath.row] as? UIImage
        let keys = Array(imageUrl.keys)
        print(keys)
        if keys.contains(indexPath.row)
        {
            print(self.imageUrl[indexPath.row]!)
            if self.imageUrl[indexPath.row] != nil
            {
                cell.img.sd_setShowActivityIndicatorView(true)
                cell.img.sd_setIndicatorStyle(.gray)
                cell.img.sd_setImage(with:  URL(string: (self.imageUrl[indexPath.row])!))
            }
            else
            {
                cell.img.image = self.images[indexPath.row] as? UIImage
            }
            
         }
        else
        {
            cell.img.image = self.images[indexPath.row] as? UIImage
        }
        
        cell.img.contentMode = .scaleToFill
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.contentMode = .scaleToFill
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let flowLayout: UICollectionViewFlowLayout? = (collectionView_cards.collectionViewLayout as? UICollectionViewFlowLayout)
        let cellWidth: CGFloat = collectionView_cards.frame.width / 3
        let cellHeight: CGFloat = collectionView_cards.frame.size.height
        flowLayout?.minimumInteritemSpacing = 10
        flowLayout?.minimumLineSpacing = 10//cellWidth/3
        flowLayout?.itemSize = CGSize(width: cellWidth, height: cellHeight)
        return CGSize(width: cellWidth-10, height: cellHeight-10)
        //return (flowLayout?.itemSize)!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.img_btnTag = indexPath.row
      
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.checkImgCount = true
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.checkImgCount = true
            self.openGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            
        }
        
        picker?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            alert.modalPresentationStyle = .popover
            alert.popoverPresentationController!.sourceView = self.tabBarController?.tabBar//self.viewTitle // CRASH!!!
            alert.popoverPresentationController!.sourceRect = CGRect.init(x: self.view.bounds.size.width / 2 - 0.5, y:70, width: 1.0, height:  1.0)
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    func searchSpot(withKey key: String) {
        do {
            self.Scuba_Tropics_copy.removeAllObjects()
           
            if let file = Bundle.main.url(forResource: "oceans", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                let data1 = json as! NSDictionary
                let Scuba_Tropics = data1.value(forKey: "Scuba Tropics") as! NSArray
                self.Scuba_Tropics_copy = Scuba_Tropics.mutableCopy() as! NSMutableArray
            
         //       print(self.Scuba_Tropics_copy)
                self.Location_Array.removeAllObjects()
                self.array_latLong.removeAllObjects()
                DispatchQueue.main.async {
                    self.location_Tableview.isHidden = false
                   // self.Location_view.isHidden = false
                let predicate = NSPredicate(format: "SITE_NAME beginswith[c] %@", key )
                self.array_searchedSpots = self.Scuba_Tropics_copy.filter { predicate.evaluate(with: $0) } as! NSMutableArray
                print(self.array_searchedSpots.count)
                print(self.array_searchedSpots)

                
                if self.array_searchedSpots.count != 0
                {
                    
                    for i in 0..<self.array_searchedSpots.count
                    {
                        self.data11 = self.array_searchedSpots.object(at: i) as! NSDictionary
                        print(self.data11)
                        var country = String();     var region = String();   var subRegion = String();   var site = String()
                        country = self.data11.value(forKey: "COUNTRY") as! String
                        region = self.data11.value(forKey: "Region") as! String
                        subRegion = self.data11.value(forKey: "Sub Region") as! String
                        site = self.data11.value(forKey: "SITE_NAME") as! String

                        var address = String()
                        address =  "\(site)\(",")\(" ")\(subRegion)\(",")\(" ")\(region)\(" - ")\(country)"
                        self.Location_Array.add(address)
                        if self.data11.value(forKey: "GPS Coordinates") as? String != nil
                        {
                            self.array_latLong.add(self.data11.value(forKey: "GPS Coordinates") as! String)
                        }
                        else
                        {
                            self.array_latLong.add("alert")
                        }
                       
                    }
                    
                    self.location_Tableview .reloadData()
                    
                }
                else
                {
                        self.Location_Array.removeAllObjects()
                        self.array_latLong.removeAllObjects()
                        self.location_Tableview.reloadData()
                }

                }
                
                
              /*
               // print(Scuba_Tropics_copy)
                    for i in 0..<Scuba_Tropics_copy.count
                    {
                        data11 = Scuba_Tropics_copy.object(at: i) as! NSDictionary
                        print(data11)
                        
                        let keyArray = Array(searchDict.keys)
                        
                        var count = 0
                        for j in 0..<keyArray.count
                        {
                            if data11.value(forKey: keyArray[j]) as? String == searchDict[keyArray[j]]
                            {
                                count = count + 1
                            }
                        }
                        if keyArray.count == count && data11.value(forKey: "GPS Coordinates") as? String != nil
                        {
                            self.array_GPSCoordinates.add(data11.value(forKey: "GPS Coordinates") as! String)
                            var str = data11.value(forKey: "GPS Coordinates") as! String
                            str = str.replacingOccurrences(of: "(", with: "")
                            str = str.replacingOccurrences(of: ")", with: "")
                            str = str.replacingOccurrences(of: " ", with: "")
                            self.lbl_lat_Long.text = str
                            var country = String();     var region = String();   var subRegion = String();   var site = String()
                            country = data11.value(forKey: "COUNTRY") as! String
                            region = data11.value(forKey: "Region") as! String
                            subRegion = data11.value(forKey: "Sub Region") as! String
                            site = data11.value(forKey: "SITE_NAME") as! String
                            
                            
                            DispatchQueue.main.async {
                                var address = String()
                                address =  "\(site)\(",")\(" ")\(subRegion)\(",")\(" ")\(region)\(" - ")\(country)"
                                print(address)
                                self.lbl_mapAddress.text = address
                                self.dict_address = ["address": address, "coordinates": str]
                                
                                let coordinates = str.components(separatedBy: ",") as! NSMutableArray
                                let lat = Double(coordinates[0] as! String)
                                let long = Double(coordinates[1] as! String)
                                
                                self.annotation.coordinate = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
                                self.mapView.addAnnotation(self.annotation)
                                Util.stopIndicator(with: (self.view)!)
                            }
                        }
                        //  Util.stopIndicator(with: (self.view)!)
                    }
//                else
//                {
//                    DispatchQueue.main.async {
//                        Util.stopIndicator(with: (self.view)!)
//                    }
//                    let dive_site1 = Scuba_Tropics_copy.value(forKey: "GPS Coordinates") as! NSArray
//                    self.array_GPSCoordinates = dive_site1.mutableCopy() as! NSMutableArray
//                }
//                
//                //                DispatchQueue.main.asyncAfter(deadline: .now() + 3)
//                //                {
//                var str = self.array_GPSCoordinates[0] as! String
//                
//                str = str.replacingOccurrences(of: "(", with: "")
//                str = str.replacingOccurrences(of: ")", with: "")
//                str = str.replacingOccurrences(of: " ", with: "")
//                let arr = str.components(separatedBy: ",") as! NSMutableArray
//                //  print(str)
//                //  print(arr)
//                self.lat_str = arr[0] as! String
//                self.long_str = arr[1] as! String
//                self.lbl_lat_Long.text = str
//                self.locationManager.startUpdatingLocation()
//                DispatchQueue.main.async {
//                    self.getHistorylogs()
//                    
                }
                
               */
                
                
                
//                // }
//                
//            } else {
//                //  print("no file")
//                DispatchQueue.main.async {
//                    Util.stopIndicator(with: (self.view)!)
//                }
//                
            }
    }
            catch {
         //   print(error.localizedDescription)
            DispatchQueue.main.async {
                Util.stopIndicator(with: (self.view)!)
            }
        }
    }

    
    
    
    
    
    
    
    
    
    //    func textFieldDidEndEditing(_ textField: UITextField)
//    {
//        
//    }
  
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        return true;
//    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder();
//        return true;
//    }
    
    
    
//    func imageUploadRequest (image:UIImage, parameters: [String: String]? ,imagename: String, keyName:String, completion:@escaping (_ result: NSDictionary?) -> (),  Error:@escaping (NSError) -> ()){
//        
//        let myUrl = NSURL(string: "http://beta.brstdev.com/swipr/api/web/v1/users/addprofileimages");
//        let request = NSMutableURLRequest(url: myUrl! as URL)
//        
//        request.httpMethod = "POST";
//        
//        let boundary = generateBoundaryString()
//        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//        var imageData = NSData()
//        imageData   = UIImageJPEGRepresentation(image, 1)! as NSData
//        
//        request.httpBody = createBodyWithParameters(parameters: parameters, keyName: keyName, imageDataKey: imageData, boundary: boundary, filename: "\(imagename).jpg", mimetype: "image/jpg") as Data
//        
//        let task = URLSession.shared.dataTask(with: request as URLRequest) {
//            data, response, error in
//            
//            if error != nil {
//                print("error=\(String(describing: error))")
//                Error(error! as NSError)
//                return
//            }
//            
//            do {
//                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
//                print(json!)
//                
//                completion(json)
//                
//            } catch {
//                Error(error as NSError)
//            }
//            
//        }
//        
//        task.resume()
//        
//    }
//    
//    func createBodyWithParameters(parameters: [String: String]?, keyName:String, imageDataKey: NSData, boundary: String,filename:String, mimetype:String) -> NSData {
//        let body = Data();
//        
//        if parameters != nil {
//            for (key, value) in parameters! {
//                body.appendString(string: "--\(boundary)\r\n")
//                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//                body.appendString(string: "\(value)\r\n")
//            }
//        }
//        body.appendString(string: "--\(boundary)\r\n")
//        body.appendString(string: "Content-Disposition: form-data; name=\"\(keyName)\"; filename=\"\(filename)\"\r\n")
//        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
//        body.append(imageDataKey as Data)
//        body.appendString(string: "\r\n")
//        body.appendString(string: "--\(boundary)--\r\n")
//        
//        return body
//    }
//
//    
//    func generateBoundaryString() -> String {
//        return "Boundary-\(NSUUID().uuidString)"
//    }
}

//
//  BaseViewController.swift
//  Scuba_APPlication
//
//  Created by Brst on 10/9/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var demoStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ShowMessage(title: String, msg : String)
    {
        let alertController = UIAlertController(title: title, message:
            msg, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
   
}

//
//  SearchPOIViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 16/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class SearchPOIViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource,MKMapViewDelegate, CLLocationManagerDelegate{

    var controller:SidemenuViewController? = nil    //hold the reference here.
    let locationManager = CLLocationManager()
    let annotation = MKPointAnnotation()
    

    var viewHasMovedToRight = false
    var Scuba_Tropics_copy = NSMutableArray()
    var array_imgUrl = NSMutableArray()
    var viewHasMovedDown = false
    var arraySiteName = NSMutableArray()
    var data11 = NSDictionary()
    var array_gps = NSMutableArray()
    var lat_str = String()
    var long_str = String()
    var array_searchedSpots = NSMutableArray()
    var array_spots = NSMutableArray()
    var array_GPSCoordinates = NSMutableArray()
    var dict_address = NSDictionary()
    
    var images :[String] = ["pop-up-image1", "pop-up-image2", "pop-up-image3" ,"eye"]
    
    @IBOutlet var collectionview: UICollectionView!
    // MARK: - Properties
    
    
    @IBOutlet var Back_arrowimg: UIImageView!
    
    @IBOutlet var view_searchView: UIView!
    @IBOutlet var loginview: UIView!
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var heightconstraint: NSLayoutConstraint!
    @IBOutlet var menu_img: UIImageView!
    @IBOutlet var lbl_diveSiteName: UILabel!
    @IBOutlet var lbl_waterConditions: UILabel!
    @IBOutlet var lbl_lat_Long: UILabel!
    @IBOutlet var lbl_meteo: UILabel!
    
    @IBOutlet var view_imgPreview: UIView!
    @IBOutlet var img_preview: UIImageView!
    @IBOutlet var view_imgPreview_back: UIView!
    @IBOutlet var lbl_noMedia: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var lbl_spotType: UILabel!
    @IBOutlet var constraintsHeightOfMapView: NSLayoutConstraint!
    @IBOutlet var lbl_mapAddress: UILabel!
    
    @IBOutlet var constraintsHeightOfcollectionView: NSLayoutConstraint!
    func swipeview(notification:Notification) -> Void {
        
        self  .swipefunction()
        
    }
    
    
    @IBAction func Menu_Action(_ sender: Any) {
   
        
        let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        
        
        if  myAppDelegate?.ishome == false
        {
            
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainHome") as! ViewController
            self.navigationController?.pushViewController(secondViewController, animated: true)
            
            
            
        }
        else
        {
            
            if viewHasMovedToRight == false
                
            {
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = true
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
                
            }
            else
            {
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = false
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
                
            }
            
            
        }
        
        
        
    }
    

    
    
    
    
    
    
    func swipefunction()
    {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                        self.viewHasMovedToRight = false
                        
                        //self.controller?.didMove(toParentViewController: self)
        }, completion: { (finished) -> Void in
            
        })
    }

    
    
      override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            constraintsHeightOfMapView.constant = 350
            constraintsHeightOfcollectionView.constant = 460
        }
        else
        {
            constraintsHeightOfMapView.constant = 250
        }
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.mapView.showsUserLocation = true

        
        self.array_spots = ["Spot","COUNTRY", "Region", "Sub Region","SITE_NAME"]
        self.view_imgPreview.frame.size.height = (self.tabBarController?.tabBar.frame.size.height)! + self.view_imgPreview.frame.size.height + 20
        
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapBlurButton(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.img_preview.isUserInteractionEnabled = true
        self.img_preview.addGestureRecognizer(tapGesture)
        
        
        controller = storyboard?.instantiateViewController(withIdentifier: "slidemenu") as! SidemenuViewController?
        addChildViewController(controller!)
        controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
        
        view.addSubview((controller?.view)!)
        controller?.didMove(toParentViewController: self)
        
        
        let nc = NotificationCenter.default // Note that default is now a property, not a method call
        nc.addObserver(forName:Notification.Name(rawValue:"hidesearchpoi"),
                       object:nil, queue:nil,
                       using:swipeview)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        controller?.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        controller?.view.addGestureRecognizer(swipeLeft)
        
        let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        
        
        if  myAppDelegate?.ishome == false
        {
            loginview.isHidden = false
            Back_arrowimg.isHidden = false
            
        }
        else
        {
            heightconstraint.constant = 0
            loginview.isHidden = true
            Back_arrowimg.isHidden = true
            
            menu_img.isHidden = false
            
        }

    }

    override func viewWillAppear(_ animated: Bool) {
//        DispatchQueue.global(qos: .background).async
//        {
//            Util.showindicatior(with: (self.view)!)
//        }
        if self.view_searchView.isHidden == false
        {
            self.tabBarController?.tabBar.isHidden = true
        }
        else
        {
            self.tabBarController?.tabBar.isHidden = false
        }
        self.view_searchView.frame.size.height = (self.tabBarController?.tabBar.frame.size.height)! + self.view_searchView.frame.size.height
        DispatchQueue.main.async {
            Util.showindicatior(with: (self.view)!)
        }
        
        DispatchQueue.main.async {
            let demo = UserDefaults.standard.object(forKey: "searchSpot") as! NSArray   // Getting search data here
            self.arraySiteName = demo.mutableCopy() as! NSMutableArray
            self.tableView.reloadData()
            Util.stopIndicator(with: (self.view)!)
            let data = UserDefaults.standard.object(forKey: "GPSCoordinates") as! NSArray
            self.array_searchedSpots = data.mutableCopy() as! NSMutableArray
            let spottype = UserDefaults.standard.object(forKey: "SpotType") as! String
            if spottype == "Spot"
            {
                self.lbl_spotType.text = "SPOTS"
            }
            if spottype == "Beach"
            {
                self.lbl_spotType.text = "Beach"
            }
            if spottype == "Dive Spot"
            {
                self.lbl_spotType.text = "Dive Spot"
            }
        }
        
       // self.searchSpot()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
       
        if self.lat_str.isEmpty == false
        {
            let lat = Double(self.lat_str)
            let long = Double(self.long_str)
            let center = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
            
            self.mapView.setRegion(region, animated: true)
        }
        else
        {
            let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
            
            self.mapView.setRegion(region, animated: true)
        }
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Errors " + error.localizedDescription)
    }
    
    
//Mark : Collectionview Delegate and Datasource 
    
//     func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
    
    //2
     func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        if(self.array_imgUrl.count > 0){
            return (self.array_imgUrl.object(at: 0) as! NSArray).count
        }
        return 0

    }
    
    //3
     func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MediaCollectionViewCell
        
        cell.layer.cornerRadius = 5;
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.white.cgColor
        
        cell.mediaimage.sd_setShowActivityIndicatorView(true)
        cell.mediaimage.sd_setIndicatorStyle(.gray)
        cell.mediaimage.sd_setImage(with:  URL(string: (self.array_imgUrl.object(at: 0) as! NSArray).object(at: indexPath.row) as!String))
        
        return cell
    }


    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //  Converted with Swiftify v1.0.6242 - https://objectivec2swift.com/
        let flowLayout: UICollectionViewFlowLayout? = (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)
        
        let availableWidthForCells: CGFloat? = collectionView.frame.width - (flowLayout?.sectionInset.left)! - (flowLayout?.sectionInset.right)! - (flowLayout?.minimumInteritemSpacing)!

       // let cellWidth: CGFloat = availableWidthForCells! / 2
       // flowLayout?.itemSize = CGSize(width: cellWidth, height: 140)
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let cellWidth: CGFloat = availableWidthForCells! / 4
            flowLayout?.itemSize = CGSize(width: cellWidth, height: cellWidth)
        }
        else
        {
            let cellWidth: CGFloat = availableWidthForCells! / 2
            flowLayout?.itemSize = CGSize(width: cellWidth, height: cellWidth)
        }
        return (flowLayout?.itemSize)!

        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        // selectedimage.image = UIImage (named:self.images[indexPath.row] )
        self.tabBarController?.tabBar.isHidden = true
        
        self.view_imgPreview.isHidden = false
        self.img_preview.sd_setShowActivityIndicatorView(true)
        self.img_preview.sd_setIndicatorStyle(.gray)
        self.img_preview.sd_setImage(with:  URL(string: (self.array_imgUrl.object(at: 0) as! NSArray).object(at: indexPath.row) as!String))
        
    }
    @IBAction func btn_img_preview_back(_ sender: Any)
    {
        self.view_imgPreview.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    func tapBlurButton(sender:UITapGestureRecognizer)
    {
        
        if viewHasMovedDown == false
            
        {
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.view_imgPreview_back.frame = CGRect(x: CGFloat(0), y: CGFloat(-self.view_imgPreview_back.frame.size.height-20), width: CGFloat(self.view_imgPreview_back.frame.size.width), height: CGFloat(self.view_imgPreview_back.frame.size.height))
                            self.viewHasMovedDown = true
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        else
        {
            // self.view_imgPreview_back.isHidden = true
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.view_imgPreview_back.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view_imgPreview_back.frame.size.width), height: CGFloat(self.view_imgPreview_back.frame.size.height))
                            self.viewHasMovedDown = false
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        
    }
    //3
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        insetForSectionAt section: Int) -> UIEdgeInsets {
//        return sectionInsetss
//    }
//    
//    // 4
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return sectionInsetss.left
//    }

    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = true
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.down:
                
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = false
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.up:
                
                print("Swiped up")
            default:
                break
            }
        }
    }
    //MARK: Create ur own log here
    @IBAction func btn_createYourOwnLog(_ sender: Any)
    {
        DispatchQueue.main.async {
            
            UserDefaults.standard.set(true, forKey: "checkSearchedItem")
            UserDefaults.standard.set(self.dict_address, forKey: "dict_address")
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"home") as! HomeViewController
            viewController.checkAddLog = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    //MARK: TABLE view delegates 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arraySiteName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SlidemenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! SlidemenuTableViewCell
        
        
        cell.lbl_siteName.text = self.arraySiteName[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        self.array_searchedSpots.replaceObject(at: 4, with: self.arraySiteName[indexPath.row] as! String)
        //print(self.array_searchedSpots)
        DispatchQueue.main.async {
            Util.showindicatior(with: (self.view)!)
        }
       // Util.showindicatior(with: (self.view)!)

        DispatchQueue.global(qos: .background).async {
 //DispatchQueue.main.async {
            self.searchSpot()
        }
   // DispatchQueue.main.async {
    
        UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
            self.view_searchView.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                self.view_searchView.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.view_searchView.transform = CGAffineTransform.identity
                })
            })
        })
    //}
        DispatchQueue.main.async
        {
            self.tabBarController?.tabBar.isHidden = false
            self.view_searchView.isHidden = true
            self.lbl_diveSiteName.text = self.arraySiteName[indexPath.row] as? String
        }
    }
    @IBAction func btn_searchView_back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_search(_ sender: Any)
    {
        
    }
    
    @IBAction func btn_clear_search(_ sender: Any)
    {
        
    }
    
    func searchSpot() {
        do {
            self.array_GPSCoordinates.removeAllObjects()
            var searchedSpot = NSMutableArray()
            var searchDict = [String: String]()
            
            let GPS = UserDefaults.standard.object(forKey: "GPSCoordinates") as! NSArray  // Getting search data here
            searchedSpot = GPS.mutableCopy() as! NSMutableArray
            self.Scuba_Tropics_copy.removeAllObjects()
            
            if let file = Bundle.main.url(forResource: "oceans", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                let data1 = json as! NSDictionary
                let Scuba_Tropics = data1.value(forKey: "Scuba Tropics") as! NSArray
                self.Scuba_Tropics_copy = Scuba_Tropics.mutableCopy() as! NSMutableArray
                
                for i in 0..<array_spots.count
                {
                    if array_searchedSpots[i] as! String != array_spots[i] as! String
                    {
                        searchDict[array_spots[i] as! String] = (array_searchedSpots[i] as! String)
                    }
                }
                
                if searchDict.isEmpty == false
                {
                    for i in 0..<Scuba_Tropics_copy.count
                    {
                        data11 = Scuba_Tropics_copy.object(at: i) as! NSDictionary
                        let keyArray = Array(searchDict.keys)
                    
                        var count = 0
                        for j in 0..<keyArray.count
                        {
                            if data11.value(forKey: keyArray[j]) as? String == searchDict[keyArray[j]]
                            {
                                count = count + 1
                            }
                        }
                        if keyArray.count == count && data11.value(forKey: "GPS Coordinates") as? String != nil
                        {
                            self.array_GPSCoordinates.add(data11.value(forKey: "GPS Coordinates") as! String)
                            var str = data11.value(forKey: "GPS Coordinates") as! String
                            str = str.replacingOccurrences(of: "(", with: "")
                            str = str.replacingOccurrences(of: ")", with: "")
                            str = str.replacingOccurrences(of: " ", with: "")
                            self.lbl_lat_Long.text = str
                            var country = String();     var region = String();   var subRegion = String();   var site = String()
                            country = data11.value(forKey: "COUNTRY") as! String
                            region = data11.value(forKey: "Region") as! String
                            subRegion = data11.value(forKey: "Sub Region") as! String
                            site = data11.value(forKey: "SITE_NAME") as! String
                            
//                            var address = String()
//                            address =  "\(site)\(",")\(" ")\(subRegion)\(",")\(" ")\(region)\(" - ")\(country)"
//                            //print(address)
//                            self.lbl_mapAddress.text = address
//                            self.dict_address = ["address": address, "coordinates": str]
                           // print(dict_address)

                            DispatchQueue.main.async {
                                var address = String()
                                address =  "\(site)\(",")\(" ")\(subRegion)\(",")\(" ")\(region)\(" - ")\(country)"
                                //print(address)
                                self.lbl_mapAddress.text = address
                                self.dict_address = ["address": address, "coordinates": str]
                                
                                let coordinates = str.components(separatedBy: ",") as! NSMutableArray
                                let lat = Double(coordinates[0] as! String)
                                let long = Double(coordinates[1] as! String)
                                
                                self.annotation.coordinate = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
                                self.mapView.addAnnotation(self.annotation)
                                Util.stopIndicator(with: (self.view)!)
                            }
                        }
                      //  Util.stopIndicator(with: (self.view)!)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        Util.stopIndicator(with: (self.view)!)
                    }
                    let dive_site1 = Scuba_Tropics_copy.value(forKey: "GPS Coordinates") as! NSArray
                    self.array_GPSCoordinates = dive_site1.mutableCopy() as! NSMutableArray
                }
                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 3)
//                {
                    var str = self.array_GPSCoordinates[0] as! String
                    
                    str = str.replacingOccurrences(of: "(", with: "")
                    str = str.replacingOccurrences(of: ")", with: "")
                    str = str.replacingOccurrences(of: " ", with: "")
                    let arr = str.components(separatedBy: ",") as! NSMutableArray
                  //  print(str)
                  //  print(arr)
                    self.lat_str = arr[0] as! String
                    self.long_str = arr[1] as! String
                    self.lbl_lat_Long.text = str
                    self.locationManager.startUpdatingLocation()
                    DispatchQueue.main.async {
                        self.getHistorylogs()

                    }
               // }
                
            } else {
                //  print("no file")
                DispatchQueue.main.async {
                    Util.stopIndicator(with: (self.view)!)
                }
                
            }
        } catch {
            print(error.localizedDescription)
            DispatchQueue.main.async {
                Util.stopIndicator(with: (self.view)!)
            }
        }
    }

    //MARK: Location Logs images
    func getHistorylogs()
    {
        self.array_imgUrl.removeAllObjects()
        Model.getLocationlogs(withSuccess: { (Result) in
            
            if(Result.object(forKey: "data") != nil)
            {
                let data :NSArray = Result.object(forKey: "data") as! NSArray
                 print(data)
                
                var urlArray = [String]()
//                let meteoArray = NSMutableArray()
//                let waterConditionArray = NSMutableArray()
//                let waterTempArray = NSMutableArray()
                 DispatchQueue.main.async {
                    for i in 0..<data.count
                    {
                        let demo = data.object(at: i) as! NSDictionary
                        if demo.value(forKey: "latitude") as! String == self.lat_str && demo.value(forKey: "longitude") as! String == self.long_str
                        {
                            if (demo.value(forKey: "image")) != nil
                            {
                                self.lbl_noMedia.isHidden = true
//                                let urlStr = "http://beta.brstdev.com/Scubap/frontend/web/uploads/"
//                                let imgUrl = demo.value(forKey: "image")! as! String
//                                let url = urlStr + imgUrl
//                                urlArray.append(url)
                                let urlStr = "http://beta.brstdev.com/Scubap/frontend/web/uploads/"
                                let imgUrl = demo.value(forKey: "image")! as! String
                                let url = urlStr + imgUrl
                                urlArray.append(url)
                                let imgurl1 = demo.value(forKey: "image1")! as! String
                                let url1 = urlStr + imgurl1
                                urlArray.append(url1)
                                let imgurl2 = demo.value(forKey: "image2")! as! String
                                let url2 = urlStr + imgurl2
                                urlArray.append(url2)
                                print(urlArray)
//                                meteoArray.add(demo.value(forKey: "meteo") as! String)
//                                waterConditionArray.add(demo.value(forKey: "water_conditions") as! String)
//                                waterTempArray.add(demo.value(forKey: "water_temprature") as! String)
                                
                            }
                        }

                    }
               // }
               // DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    Util.stopIndicator(with: (self.view)!)

                    self.array_imgUrl[0] = urlArray
//                    var i = meteoArray.count-1
//                    while i > 0
//                    {
//                        if meteoArray.object(at: i) as! String != "Meteo:"
//                        {
//                            self.lbl_waterConditions.text = meteoArray.object(at: i) as! String
//                        }
//                        i -= 1
//                    }
//                    var j = waterConditionArray.count-1
//                    while j > 0
//                    {
//                        if waterConditionArray.object(at: j) as! String != "Water Conditions" && waterTempArray.object(at: j) as! String != "Water Temperature:"
//                        {
//                            self.lbl_waterConditions.text = "\(meteoArray.object(at: j) as! String)\(" ")\(waterTempArray.object(at: j) as! String)"
//                        }
//                        j -= 1
//                    }
                    self.collectionview.reloadData()
                    Util.stopIndicator(with: (self.view)!)

                }
             //   DispatchQueue.main.asyncAfter(deadline: .now() + 3)
               // {
                //    Util.stopIndicator(with: (self.view)!)
              //  }
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    Util.showMessage("There is no Images", withTitle: "Alert!")
                    Util.stopIndicator(with: (self.view)!)

                }
            }
            
        }) { (error) in
            print (error)
            DispatchQueue.main.async {
                Util.stopIndicator(with: (self.view)!)
            }
        }
        
    }
    @IBAction func btn_back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//
//  USerinfo.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 05/07/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class USerinfo: NSObject

{
    
    var user_name = String()
    var user_id = String()
    var user_email = String()
    var user_gender = String()
    var user_address = String()
    var created_at = String()
    var user_dob = String ()
    var is_admin = Bool()
    var username =  String()
    var updated_at =  String ()
    var Status = String()
    var ActivityType = String()
    var logType = String()

    
    class var sharedInstance: USerinfo {
            struct Static {
                static let instance: USerinfo = USerinfo()
            }
            return Static.instance
        }
    }
    

    
    

    
    

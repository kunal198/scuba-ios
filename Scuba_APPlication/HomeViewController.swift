//
//  HomeViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 08/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class HomeViewController: UIViewController,MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource , CLLocationManagerDelegate{
    var controller:SidemenuViewController? = nil    //hold the reference here.
    var array = [NSMutableArray]()
    var dict = [Any]()
    var imageName: String!
    var coordinates: [[Double]]!
    var names:[String]!
    var addresses:[String]!
    var phones:[String]!
    var weather:[String]!
    var water :[String]!
    var conditions :[String]!
    var visibility :[String]!
    var itle: [String]!
    var LocationArray = NSArray()
    var oceanArray = NSArray()
    var country = NSArray()
    var diveSiteArray = NSArray()
    var Region = NSArray()
    var SubRegion = NSArray()
    var DuplicatesubcatagoriesArray = NSMutableArray()
    var DuplicateArray = NSMutableArray()
    var section_index = Int()
    var item_selected = String()
    var Scuba_Tropics_copy = NSMutableArray()
    var section_array = NSMutableArray()
    var reSetData_array = NSMutableArray()
    var checkDiveSiteName = false
    var checkTable = false
    var checkAddLog = false
    let newPin = MKPointAnnotation()
    var locationManager:CLLocationManager!

    struct Location {
        let title: String
        let latitude: Double
        let longitude: Double
    }
    
    @IBOutlet var Mapview: MKMapView!
    @IBOutlet var addlog_btn: UIButton!
    @IBOutlet var Log_Typeview: UIView!
    @IBOutlet var quickLog_View: UIView!
    @IBOutlet var quickLog_btn: UIButton!
    @IBOutlet var EZLog_btn: UIButton!
    @IBOutlet var Activity_LOgsView: UIView!
    @IBOutlet var resetBtn: UIButton!
    
    @IBOutlet var Back_btn: UIButton!
    @IBOutlet var lbl_searchSpot_title: UILabel!
    
    
    var viewHasMovedToRight = false
    
    @IBAction func ADdLogMap(_ sender: Any)
    {
        
      if (addlog_btn.isSelected)
        {
           self.hidecrooslogtype()
        }
        else
        {
            EZLog_btn.setTitle("EZ Log",for: .normal)
            quickLog_btn.setTitle("Quick Log",for: .normal)
            self.Mapview.alpha = 0.5;
            
            Log_Typeview.isHidden = false
            Activity_LOgsView.isHidden = false
            quickLog_View.isHidden = true
            Log_Typeview.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
            //Activity_Indicator
            UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
                self.Log_Typeview.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.Log_Typeview.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                        self.Log_Typeview.transform = CGAffineTransform.identity
                    })
                })
            })
            
            addlog_btn.isSelected = true
        }
        
//        let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
//        myAppDelegate?.ishome = true
//        performSegue(withIdentifier: "Addlogs", sender: sender)
    }
    
    
    
    @IBAction func Back_Action(_ sender: Any)
    {
        
        
        Activity_LOgsView.isHidden = false;
        quickLog_View.isHidden = true
        
    }
    
    @IBAction func cross_Logtypeview(_ sender: Any)
    {
      self.hidecrooslogtype()
        
    }
    
    func hidecrooslogtype()
    {
        self.Mapview.alpha = 1;
        Log_Typeview.isHidden = true
        Log_Typeview.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        Log_Typeview.isHidden = true
        UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
            self.Log_Typeview.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                self.Log_Typeview.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.Log_Typeview.transform = CGAffineTransform.identity
                })
            })
        })
        
        
        
        
        addlog_btn.isSelected = false
    }
    
    func openquicklogtype()
    {
        Activity_LOgsView.isHidden = true;
        quickLog_View.isHidden = false
        
        
    }
    
    @IBAction func SurfingAction(_ sender: Any)
    {
        USerinfo.sharedInstance.logType = "Quick"
        let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        myAppDelegate?.ishome = true
        performSegue(withIdentifier: "Addlogs", sender: sender)

        
        
     //   EZLog_btn.setTitle("EZ Log",for: .normal)
      //  quickLog_btn.setTitlnote("Quick Log",for: .normal)
     //   self.openquicklogtype()
        USerinfo.sharedInstance.ActivityType = "Surfing"
     //   EZLog_btn.isHidden = true;
        
    }
    
    @IBAction func Snorking_Action(_ sender: Any)
    {
        USerinfo.sharedInstance.logType = "EZ"
        let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        myAppDelegate?.ishome = true
        performSegue(withIdentifier: "Addlogs", sender: sender)
        
      //  EZLog_btn.setTitle("EZ Log",for: .normal)
      //  quickLog_btn.setTitle("Quick Log",for: .normal)
       //  self.openquicklogtype()
        USerinfo.sharedInstance.ActivityType = "Snorking"
     //   EZLog_btn.isHidden = false;


      //  USerinfo.sharedInstance.logType = "Quick Log"
    }
    
    
    @IBAction func Diving_Action(_ sender: Any)
    {
        USerinfo.sharedInstance.logType = "Full"
        let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        myAppDelegate?.ishome = true
        performSegue(withIdentifier: "Addlogs", sender: sender)
        

//        self.openquicklogtype()
        USerinfo.sharedInstance.ActivityType = "Diving"
  //      EZLog_btn.setTitle("Full Log",for: .normal)
    //    quickLog_btn.setTitle("EZ Log",for: .normal)
      //  EZLog_btn.isHidden = false;

    }
    
    
    @IBAction func EZLog_Action(_ sender: Any)
    {
        
        if USerinfo.sharedInstance.ActivityType == "Diving"
        {
            
//            USerinfo.sharedInstance.logType = "Full"
//            let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
//            myAppDelegate?.ishome = true
//            performSegue(withIdentifier: "Addlogs", sender: sender)
            
        }
        
        else
        {
//            USerinfo.sharedInstance.logType = "EZ"
//            let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
//            myAppDelegate?.ishome = true
//            performSegue(withIdentifier: "Addlogs", sender: sender)
        }
   
    }
    
    
    
    @IBAction func QuickLOg_Action(_ sender: Any)
    {
        if USerinfo.sharedInstance.ActivityType == "Diving"
        {
            USerinfo.sharedInstance.logType = "EZ"
            let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
            myAppDelegate?.ishome = true
            performSegue(withIdentifier: "Addlogs", sender: sender)
        }
        else
        {
//        USerinfo.sharedInstance.logType = "Quick"
//        let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
//        myAppDelegate?.ishome = true
//        performSegue(withIdentifier: "Addlogs", sender: sender)
        }
        
    }
    
    

    @IBAction func Button_Action(_ sender: Any)
    {
        if viewHasMovedToRight == false

        {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                        self.viewHasMovedToRight = true
                        
                        self.controller?.didMove(toParentViewController: self)
        }, completion: { (finished) -> Void in
            
        })
        
        }
        else
        {
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                            self.viewHasMovedToRight = false
                            
                            self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        
    }
    

    
    func swipeview(notification:Notification) -> Void {
       
        self  .swipefunction()
        
    }
    
    func swipefunction()
    {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                        self.viewHasMovedToRight = false
                        
                        //self.controller?.didMove(toParentViewController: self)
        }, completion: { (finished) -> Void in
            
        })
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
     self.hidecrooslogtype()

        self.tabBarController?.tabBar.isHidden = false
       Util.setupview(singleview: searchbtn)
        Util.setupview(singleview: resetBtn)
        Util.setupview(singleview: Back_btn)
        selectedSection = -1
        self.searchMainCatagorysetup()
        
//        DispatchQueue.global(qos: .background).async {
//              self.set_Map()
//            
//        }
      
        let nc = NotificationCenter.default // Note that default is now a property, not a method call
        nc.addObserver(forName:Notification.Name(rawValue:"MyNotification"),
                       object:nil, queue:nil,
                       using:swipeview)
        
        controller = storyboard?.instantiateViewController(withIdentifier: "slidemenu") as! SidemenuViewController?
        addChildViewController(controller!)
        controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))

        view.addSubview((controller?.view)!)
        controller?.didMove(toParentViewController: self)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
         controller?.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
         controller?.view.addGestureRecognizer(swipeLeft)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = true
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.down:
                
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = false
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.up:
                
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    @IBAction func MapPlusAction(_ sender: Any)
    {
       
    }
    func determineCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3)
        {
            self.locationManager.stopUpdatingLocation()
        }

    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        //manager.stopUpdatingLocation()
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        
        Mapview.setRegion(region, animated: true)
        
        // Drop a pin at user's Current Location
//        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
//        myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
//        myAnnotation.title = "Current location"
//        Mapview.addAnnotation(myAnnotation)
    }
    func set_Map()
    {
        self.searchMainCatagorysetup()
        
        Model.getLocationlogs(/*with: 1, */withSuccess: { (Result) in
        print(Result)
            
            if(Result.object(forKey: "data") != nil)
            {
                let data :NSArray = Result.object(forKey: "data") as! NSArray
                self.LocationArray = NSMutableArray()
                self.LocationArray = Util .ParseLocation(with: data) as! NSMutableArray
            }

            
//            let data :NSArray = Result .object(forKey: "data") as! NSArray
//            var Array = NSArray()
//            Array = Util .ParseLocation(with: data) 
//            self.LocationArray = Array .mutableCopy() as! NSArray
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
               self.setLocalMap()
            }
            
            
        }) { (error) in
            print (error)
            
        }
        
    }
    
    func setLocalMap()
    {
        self.Mapview.delegate = self;
   
        for i in 0..<LocationArray.count
        {
            let Location : Location_List = LocationArray .object(at: i) as! Location_List
            let point = StarbucksAnnotation(coordinate: CLLocationCoordinate2D(latitude: Location.latitude , longitude: Location.longitude ))
            
            if Location.user_id == USerinfo.sharedInstance.user_id {
                point.title = "userImage"
            }else{
                point.title = Location.activity
            }
            
            point.address = Location.location
            point.conditions=Location.water_conditions
            point.weather = Location.meteo
            point.water = Location.water_temprature
            point.visibility = Location.visibility
            point.latitude = String (Location.latitude)
            point.longitude = String (Location.longitude)
            point.userId = String(Location.user_id)
            self.Mapview.addAnnotation(point)
            self.Mapview.showAnnotations( self.Mapview.annotations, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 3)
            {
               self.determineCurrentLocation()
            }

//            DispatchQueue.main.async {
//                self.determineCurrentLocation()
//            }
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = self.Mapview.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        
      //  print (annotation.title!!)
        
       
        if annotation.title!!  == "userImage"{
            
            annotationView?.image = UIImage(named: "octopus_mapIcon")
        }
        else if annotation.title!! == "Snorking"
        {
            annotationView?.image = UIImage(named: "octopus_mapIcon") //"search POIimg3"
            
        }
        else if annotation.title!! == "Surfing"
        {
            annotationView?.image = UIImage(named: "octopus_mapIcon")    //"search POIimg2"
            
        }
        else
        {
            annotationView?.image = UIImage(named: "octopus_mapIcon")    //"search POIimg1"
            
        }
        
        
        return annotationView
    }
    
    
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let starbucksAnnotation = view.annotation as! StarbucksAnnotation
        let views = Bundle.main.loadNibNamed("CustomCalloutView", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCalloutView
        calloutView.starbucksName.text = starbucksAnnotation.name
        calloutView.starbucksAddress.text = starbucksAnnotation.address
        UserDefaults.standard.set(calloutView.starbucksAddress.text!, forKey: "locationAddress")
        calloutView.starbuckscord.text = starbucksAnnotation.latitude  + "," + "  " + starbucksAnnotation.longitude
        // calloutView.starbucksPhone.text = starbucksAnnotation.phone
        calloutView.starbuckswater.text = starbucksAnnotation.water
        calloutView.starbucksweather.text = starbucksAnnotation.weather
        calloutView.starbucksconditions.text = starbucksAnnotation.conditions
        calloutView.starbucksvisibility.text = starbucksAnnotation.visibility
        calloutView.btn_more_mapView.addTarget(self, action: #selector(MoreButtonAction), for: .touchUpInside)
        // calloutView.starbucksImage.image = starbucksAnnotation.image
        // 3
        calloutView.center = CGPoint(x: view.bounds.size.width / 3+140, y: +calloutView.bounds.size.height*0.52+20)
        view.addSubview(calloutView)
        Mapview.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.Mapview.setRegion(region, animated: true)
    }
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        Mapview.removeAnnotation(newPin)//mapView.removeAnnotation(newPin)
//        
//        let location = locations.last! as CLLocation
//
//        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//        
//        //set region on the map
//        Mapview.setRegion(region, animated: true)
//        
//        newPin.coordinate = location.coordinate
//        Mapview.addAnnotation(newPin)
//        
//    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
    }

    //MARK: MAPView More.. Button action here
    func MoreButtonAction()
    {
        let locationLogImage = Notification.Name("NotificationIdentifier")
        
        NotificationCenter.default.post(name: locationLogImage, object: nil)
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "gallery") as! ScubaGalleryViewController
        secondViewController.checkLogImages = true
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
       self .swipefunction()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.searchSpot()
       self.tabBarController?.tabBar.isHidden = false
         self.hidecrooslogtype()
        self.searchMain_view.isHidden = true
        self.Log_Typeview.isHidden = true
        if self.checkAddLog == true
        {
            self.checkAddLog = false
            self.Log_Typeview.isHidden = false
        }
        else
        {
                UserDefaults.standard.set(false, forKey: "checkSearchedItem")
        }
    }
    
    //MARK: Delegate Fot Tableview and PopUpview Function :-
    
    
    @IBOutlet var table: UITableView!
    var subcatNameArray = NSMutableArray()
    var arrayForBool = NSMutableArray()
    var selectedSection = Int()
    var tap = UITapGestureRecognizer()
    
    @IBOutlet var searchMain_view: UIView!
    
    
    
    
    @IBOutlet var searchbtn: UIButton!
   
    
    @IBOutlet var searchbtnMain: UIButton!
    
    //MARK: <<<<<< Search Button >>>>>>>>
    @IBAction func searchbtnclicked(_ sender: Any) {
        self.lbl_searchSpot_title.text = "SEARCH SPOT"

        if (searchbtnMain.isSelected)
        {
            self.Mapview.alpha = 1;
            searchMain_view.isHidden = true
            
            searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
            searchMain_view.isHidden = true
            UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
                self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                        self.searchMain_view.transform = CGAffineTransform.identity
                    })
                })
            })
            
            
            
            
            searchbtnMain.isSelected = false
            
        }
        else
        {
            self.Mapview.alpha = 0.5;
            
            searchMain_view.isHidden = false
            searchMain_view.isHidden = false
            searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
            //Activity_Indicator
            UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
                self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                        self.searchMain_view.transform = CGAffineTransform.identity
                    })
                })
            })
            
            
            
            searchbtnMain.isSelected = true
        }
    }
    
    //MARK: Search Result Cross Action >>>> *X*
    @IBAction func searchcrossbtnAction(_ sender: Any) {
        self.reSetData()
        self.checkDiveSiteName = false
        self.checkTable = false
        self.table.reloadSectionIndexTitles()
        self.table.reloadData()
        self.Mapview.alpha = 1;
        searchMain_view.isHidden = true
        
        searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        searchMain_view.isHidden = true
        UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
            self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.searchMain_view.transform = CGAffineTransform.identity
                })
            })
        })
        
        searchbtnMain.isSelected = false
        
    }
    
    
    
    
    
    func searchMainCatagorysetup()
    {
        subcatNameArray = ["Spot Type","Country","Region","Sub Region","SITE_NAME"]  // "Ocean",,"Activity","DIVE SITE_NAME"
        
        // Subcatimage = ["","","","","","","","","",""]
        for  _ in 0..<subcatNameArray.count {
            arrayForBool.add(false)
        }
        
        
    }
    
   //MARK:<<< Table View Functions: >>>>>
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
//        if self.checkTable == true
//        {
//            return self.subcatNameArray.count
//        }
//        searchbtn.isUserInteractionEnabled = true

        return subcatNameArray.count-1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if self.checkTable == true
//        {
//            return (self.DuplicatesubcatagoriesArray[4] as AnyObject).count
//        }
        if  (arrayForBool[section] as! Bool == true as Bool)
        {
            return (self.DuplicatesubcatagoriesArray[section] as AnyObject).count
        }
        else
        {
            return 0;
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:SlidemenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! SlidemenuTableViewCell
        
        let sectionView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.table.frame.size.width ), height: CGFloat(45)))
        sectionView.removeFromSuperview()
        //        sectionView.addSubview(viewLabel)
        
        
        // cell.contentView = nil;
        
        
        if let nameLabel = cell.viewWithTag(100) as? UILabel{
            var arr = NSArray()
            var demo = NSArray()
            arr = self.DuplicatesubcatagoriesArray[indexPath.section] as! NSArray
//            if self.checkTable == true
//            {
//                arr = self.DuplicatesubcatagoriesArray[4] as! NSArray
//            }
            nameLabel.backgroundColor = UIColor.clear
            nameLabel.textColor = UIColor.white
            nameLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
            let str1 =  "  " as String
            demo = self.DuplicatesubcatagoriesArray[indexPath.section] as! NSArray
//            if self.checkTable == true
//            {
//                demo = self.DuplicatesubcatagoriesArray[4] as! NSArray
//            }
            let str2 = demo[indexPath.row] as! String
            var str  =   str1 + str2
            str = arr[indexPath.row] as! String
            nameLabel.text = ""
            nameLabel.text = (str as NSString) as String
            
        }
        else{
            let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.table.frame.size.width-20 ), height: CGFloat(44)))
            
            viewLabel.backgroundColor = UIColor.clear
            viewLabel.textColor = UIColor.white
            viewLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
            viewLabel.text = ""
            viewLabel.tag = 100;
            
            var arr = NSArray()
            
            arr = self.DuplicatesubcatagoriesArray[indexPath.section] as! NSArray
//            if self.checkTable == true
//            {
//                arr = self.DuplicatesubcatagoriesArray[4] as! NSArray
//            }
            let str = arr[indexPath.row] as! String
            viewLabel.text = ""
            viewLabel.text = (str as NSString) as String
            
            cell.addSubview(viewLabel)
        }
        
        //   sectionView.addSubview(viewLabel)
        /********** Add a custom Separator with Section view *******************/
        let separatorLineView = UIView(frame: CGRect(x: CGFloat(10), y: CGFloat(44), width: CGFloat(self.table.frame.size.width-20), height: CGFloat(1)))
        separatorLineView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
        sectionView.addSubview(separatorLineView)
        
        return cell
        
      }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        
        var arr = NSArray()
//        if self.checkTable == true
//        {
//            arr = self.DuplicatesubcatagoriesArray[4] as! NSArray
//            subcatNameArray[0] = (arr .object(at: indexPath.row))
//            searchbtn.isUserInteractionEnabled = true
//            self.section_index = (0)
//            self.item_selected = subcatNameArray[0] as! String
//            UserDefaults.standard.set(self.item_selected, forKey: "diveSiteName")
//            arrayForBool[0] = false;
//            
//            self.table.reloadSections([0], with: .automatic)
//        }
//        else
//        {
            arr = self.DuplicatesubcatagoriesArray[indexPath.section] as! NSArray
        
            subcatNameArray[indexPath.section] = (arr .object(at: indexPath.row))
        
            self.section_index = (indexPath.section)
            self.item_selected = subcatNameArray[indexPath.section] as! String
        
            arrayForBool[indexPath.section] = false;
        
            self.table.reloadSections([indexPath.section], with: .automatic)
        //}
        if self.section_index < 5
        {
            self.selected_output()
        }

    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let sectionView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.table.frame.size.width ), height: CGFloat(53)))
        sectionView.tag = section
        let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.table.frame.size.width-20 ), height: CGFloat(45)))
        let imageview = UIImageView (frame: CGRect(x: CGFloat(self.table.frame.size.width-50), y: CGFloat(15), width: CGFloat(20 ), height: CGFloat(20)))
        
        imageview.image = UIImage(named: "arw-dwn")
        imageview.contentMode = UIViewContentMode.scaleAspectFit;
        
        viewLabel.textColor = UIColor.white
        viewLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
        let str1 =  "  " as String
        let str2 = subcatNameArray[section] as! String
        let str  =   str1 + str2
        viewLabel.text = (str as NSString) as String
        sectionView.addSubview(viewLabel)
    /********** Add a custom Separator with Section view *******************/
        let separatorLineView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(45), width: CGFloat(self.table.frame.size.width), height: CGFloat(8)))
        separatorLineView.backgroundColor = UIColor.clear
        sectionView.addSubview(separatorLineView)
        sectionView.addSubview(imageview)
        viewLabel.backgroundColor = UIColor.lightGray
        viewLabel.layer.borderWidth = 1
        viewLabel.layer.masksToBounds = true
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        sectionView.tag = section+100;
        sectionView.addGestureRecognizer(tap)
        return sectionView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        
        return 53
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 45
    }
    
    func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
        selectedSection = (gestureRecognizer.view?.tag)! as Int
        //print (selectedSection)
        selectedSection = selectedSection - 100
        //  self.table.reloadData()
        //print(self.DuplicatesubcatagoriesArray)
        
        let indexPath = IndexPath(row: 0, section: selectedSection) as NSIndexPath
        if indexPath.row == 0 {
            let collapsed: Bool = arrayForBool[indexPath.section] as! Bool
            for i in 0..<subcatNameArray.count {
                if indexPath.section == i {
                    arrayForBool[i] = (!collapsed)
                }
            }
            self.table.reloadSections([selectedSection], with: .automatic)
            
            
            
            let collapse: Bool = arrayForBool[indexPath.section] as! Bool
            
            if (collapse == true)
            {
                let indexPath = IndexPath(row: 0, section: selectedSection)
                
                self.table.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
            }
            
            
        }
    }
    
    
    @IBAction func searchResultAction(_ sender: Any) {
//        if self.checkDiveSiteName == false
//        {
//            self.lbl_searchSpot_title.text = "SEARCH DIVE SITE"
//            self.checkDiveSiteName = true
//            self.checkTable = true
//            searchbtn.isUserInteractionEnabled = false
//            var diveSite = String()
//            diveSite = self.subcatNameArray[4] as! String
//            self.subcatNameArray.removeAllObjects()
//            self.subcatNameArray.add(diveSite)
//            print(self.subcatNameArray)
//            self.table.reloadSectionIndexTitles()
//            self.table.reloadData()
//        }
//        else
//        {
            //self.lbl_searchSpot_title.text = "SEARCH SPOT"
           // self.checkTable = false
           // print(self.subcatNameArray)
          //  print(section_array)
        
            UserDefaults.standard.set(self.subcatNameArray[0] as! String, forKey: "SpotType")
            UserDefaults.standard.set(self.section_array, forKey: "GPSCoordinates")
            UserDefaults.standard.set(self.DuplicatesubcatagoriesArray[4], forKey: "searchSpot")
            //self.Log_Typeview.isHidden = true
            self.reSetData()
            let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        
            myAppDelegate?.ishome = true
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "searchResult") as! SearchPOIViewController
            self.navigationController?.pushViewController(secondViewController, animated: true)
       // }
    }
    
    private func searchSpot() {
        do {
           // self.checkTable = false
            self.Scuba_Tropics_copy.removeAllObjects()
            self.reSetData_array.removeAllObjects()
            self.section_array.removeAllObjects()
            if let file = Bundle.main.url(forResource: "oceans", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                let data1 = json as! NSDictionary
                let Scuba_Tropics = data1.value(forKey: "Scuba Tropics") as! NSArray
               // print(Scuba_Tropics)
                self.Scuba_Tropics_copy = Scuba_Tropics.mutableCopy() as! NSMutableArray
              //  print(Scuba_Tropics_copy)

                let country1 = Scuba_Tropics.value(forKey: "COUNTRY") as! NSArray
                var country11 = NSMutableArray()
                let country = NSMutableArray()
                country11 = country1.mutableCopy() as! NSMutableArray
                if country11.count > 0
                {
                    for i in 0..<(country11.count-1)
                    {
                        if country.contains(country11[i])
                        {

                        }
                        else
                        {
                            country.add(country11[i])
                        }
                    }
                }
             //   print(country)
                let dive_site1 = Scuba_Tropics.value(forKey: "SITE_NAME") as! NSArray
                
                var dive_site11 = NSMutableArray()
                let dive_site = NSMutableArray()
                dive_site11 = dive_site1.mutableCopy() as! NSMutableArray
                if dive_site11.count > 0
                {
                    for i in 0..<(dive_site11.count-1)
                    {
                        if dive_site.contains(dive_site11[i])
                        {
                            
                        }
                        else
                        {
                            dive_site.add(dive_site11[i])
                        }
                    }
                }
//
//                let gps_data = Scuba_Tropics.value(forKey: "GPS Coordinates") as! NSArray
//                var GPS_coordinates = NSMutableArray()
//                GPS_coordinates = gps_data.mutableCopy() as! NSMutableArray
//                let ocean1 = Scuba_Tropics.value(forKey: "OCEAN") as! NSArray
//                var ocean11 = NSMutableArray()
//                let ocean = NSMutableArray()
//                ocean11 = ocean1.mutableCopy() as! NSMutableArray
//                if ocean11.count > 0
//                {
//                    for i in 0..<(ocean11.count-1)
//                    {
//                        if ocean.contains(ocean11[i])
//                        {
//                            
//                        }
//                        else
//                        {
//                            ocean.add(ocean11[i])
//                        }
//                    }
//                }

                
                let region1 = Scuba_Tropics.value(forKey: "Region") as! NSArray
                
                var region11 = NSMutableArray()
                let region = NSMutableArray()
                region11 = region1.mutableCopy() as! NSMutableArray
                if region11.count > 0
                {
                    for i in 0..<(region11.count-1)
                    {
                        if region.contains(region11[i])
                        {
                            
                        }
                        else
                        {
                            region.add(region11[i])
                        }
                    }
                }

                let sub_region1 = Scuba_Tropics.value(forKey: "Sub Region") as! NSArray
                
                var sub_region11 = NSMutableArray()
                let sub_region = NSMutableArray()
                sub_region11 = sub_region1.mutableCopy() as! NSMutableArray
                if sub_region11.count > 0
                {
                    for i in 0..<(sub_region11.count-1)
                    {
                        if sub_region.contains(sub_region11[i])
                        {
                            
                        }
                        else
                        {
                            sub_region.add(sub_region11[i])
                        }
                    }
                }

               // let activity = ["Surfing", "Snorkeling", "Scuba"] as NSArray
                let SpotType = ["Beach", "Dive Spot"] as NSArray
            //    DuplicatesubcatagoriesArray .add(ocean)
                DuplicatesubcatagoriesArray .add(SpotType)
                DuplicatesubcatagoriesArray .add(country)
                DuplicatesubcatagoriesArray .add(region)
                DuplicatesubcatagoriesArray .add(sub_region)
                DuplicatesubcatagoriesArray .add(dive_site)
              //  DuplicatesubcatagoriesArray .add(activity)
               // DuplicatesubcatagoriesArray . add(GPS_coordinates)
                self.section_array = ["Spot","COUNTRY","Region","Sub Region","SITE_NAME"] // "OCEAN",,"SITE_NAME"
              //  DispatchQueue.global(qos: .background).async {
                    self.set_Map()
               // }
                Util.setupview(singleview: searchbtn);
                self.reSetData_array = self.DuplicatesubcatagoriesArray.mutableCopy() as! NSMutableArray
              //  print(DuplicatesubcatagoriesArray)
                if json is [String: Any] {
                    // json is a dictionary
                //    print(object)
                } else if json is [Any] {
                    // json is an array
                   // print(object)
                } else {
                  //  print("JSON is invalid")
                }
            } else {
              //  print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    //MARK: Fillerting table view data
    func selected_output()
    {
        if self.Scuba_Tropics_copy.count > 0
        {
            let Scuba_Tropics = self.Scuba_Tropics_copy.mutableCopy() as! NSMutableArray
            DispatchQueue.main.async
                {
            
                    for j in 0..<self.section_array.count // >> rows count
                    {
                        let array = NSMutableArray()
                        if j != self.section_index  // >> current row
                        {
                            for i in 0..<(Scuba_Tropics.count) //   >> json result
                            {
                                let selected_section = self.section_array[self.section_index]   as! String  // >> selected option
                                let section_item = Scuba_Tropics[i] as!NSDictionary
                                if (section_item.value(forKey: selected_section) as! String) == self.item_selected  // >> fillerting data, acc to selected row
                                {
                                    let strData = section_item.value(forKey: self.section_array[j] as! String)
                                    if array.contains(strData!) == false
                                    {
                                        array.add(strData!)
                                    }
                                }
                            }
                            self.DuplicatesubcatagoriesArray[j] = array.mutableCopy()   // >> adding fillerted data
                        }
                    }
                    self.table.reloadData()
                }
        }
    }
    
    @IBAction func btn_resetData(_ sender: Any)
    {
        self.reSetData()
    }
    
    func reSetData()
    {
       // self.lbl_searchSpot_title.text = "SEARCH SPOT"
       // self.checkDiveSiteName = false
       // self.checkTable = false
        subcatNameArray.removeAllObjects()
        self.DuplicatesubcatagoriesArray = self.reSetData_array.mutableCopy() as! NSMutableArray
        
        // name used for filtering data
        subcatNameArray = ["Spot Type","Country","Region","Sub Region","SITE_NAME"]  //"Ocean",,"Activity","DIVE SITE_NAME"
     //   print(self.DuplicatesubcatagoriesArray);
        self.table.reloadSectionIndexTitles()
        
        self.table.reloadData()
    }
    
}

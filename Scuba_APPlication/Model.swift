//
//  Model.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 05/07/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class Model: NSObject

{
    //MARK: - Get logs
    
    class func getlogs(withSuccess success: @escaping (_ dic: NSDictionary) -> Void, failure: @escaping (_ err: String) -> Void)
    {
        let URL_string = NSString(format:"%@%@",BASE_URL,historyLog) as NSString
        let kBaseURL = NSURL(string: URL_string as String)! as URL
        print(URL_string)
        var request = URLRequest (url: kBaseURL)
        request.httpMethod = "POST"
        let user_id  = USerinfo.sharedInstance.user_id
        print (user_id);
      
        let post = NSString(format:"user_id=%@",user_id)
        print(post)
        let postString :String  = post as String
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                //self.activityIndicator.stopAnimating();
                failure("There is some Network Problem" )
                return;
                
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {           //check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }

            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            success(json!)
            print(json!)
            return;
            
        }
        
        task.resume()
        
    }

    
    
      //MARK: - Create Logs
    class func createlogs(with user_id:String, type:String , activity:String, meteo :String ,location:String ,date_time:String ,date :String, latitude :String, longitude :String ,air_temprature :String ,water_temprature:String,visibility:String,weight:String,water_conditions:String,dive_type:String,air_start:String,air_finish:String,timer_underwater:String,depth:String,dive_partner:String,unit:String, myimage: UIImage,withSuccess success: @escaping (_ dic: NSDictionary) -> Void, failure: @escaping (_ err: String) -> Void)
        
    {
        let URL_string = NSString(format:"%@%@",BASE_URL,createlog) as NSString
        let kBaseURL = NSURL(string: URL_string as String)! as URL
        
        var request = URLRequest (url: kBaseURL)
        request.httpMethod = "POST"
        
        let user_id  = USerinfo.sharedInstance.user_id

        print (user_id);
        
        let post = NSString(format:"user_id=%@&type=%@&activity=%@&meteo=%@&location=%@&date_time=%@&date=%@&latitude=%@&longitude=%@&air_temprature=%@&water_temprature=%@&visibility=%@&weight=%@&water_conditions=%@&dive_type=%@&air_start=%@&air_finish=%@&timer_underwater=%@&depth=%@&dive_partner=%@&unit=%@",user_id,type,activity,meteo,location,date_time,date,latitude,longitude,air_temprature,water_temprature,visibility,weight,water_conditions,dive_type,air_start,air_finish,timer_underwater,depth,dive_partner,unit,myimage)
        
        let postString :String  = post as String
        
        request.httpBody = postString.data(using: .utf8)
        
        

        
       ///////////////////
        
    /*
        
        
        let boundary = generateBoundaryString(<#Model#>)
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let imageData = UIImageJPEGRepresentation(myimage!, 1)
        
        if(imageData==nil)  { return; }
        
        request.HTTPBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)
        
        //myActivityIndicator.startAnimating();
        
        let task =  NSURLSession.sharedSession().dataTaskWithRequest(request,
                                                                     completionHandler: {
                                                                        (data, response, error) -> Void in
                                                                        if let data = data {
                                                                            
                                                                            // You can print out response object
                                                                            print("******* response = \(response)")
                                                                            
                                                                            print(data.length)
                                                                            // you can use data here
                                                                            
                                                                            // Print out reponse body
                                                                            let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
                                                                            print("****** response data = \(responseString!)")
                                                                            
                                                                            let json =  try!NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as? NSDictionary
                                                                            
                                                                            print("json value \(json)")
                                                                            
                                                                            //var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: &err)
                                                                            
                                                                            dispatch_async(dispatch_get_main_queue(),{
                                                                                //self.myActivityIndicator.stopAnimating()
                                                                                //self.imageView.image = nil;
                                                                            });
                                                                            
                                                                        } else if let error = error {
                                                                            print(error.description)
                                                                        }
        })
        task.resume()
        

        
        
        
        
        */
        
        
        
        ///////////////////////
       
        
        
        
        
        
        
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                failure(error as! String)
                return;
                
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            success(json!)
            return;
            
        }
        task.resume()
        
        
    }
    
   
    //MARK: - GetLocation Logs
    class func getLocationlogs(/*with num:Int,*/ withSuccess success: @escaping (_ dic: NSDictionary) -> Void, failure: @escaping (_ err: String) -> Void)
        
    {
        let URL_string = NSString(format:"%@%@",BASE_URL,locationlog) as NSString
        
        var request = URLRequest(url: URL(string: URL_string as String)!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 40)
//        let kBaseURL = NSURL(string: URL_string as String) as! URL
//        
//        var request = URLRequest (url: kBaseURL)
        request.httpMethod = "POST"
        
      
        let post = NSString(format:"")
        
        let postString :String  = post as String

        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                let err = String (describing: error)
                //self.activityIndicator.stopAnimating();
                failure(err )
                return;
                
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            success(json!)
            return;
            
        }
        task.resume()
        
    }
    
    
    //MARK: - FaceBook Login : -
    class func Loginfacebook(with user_id:String,fname:String,lName:String,url:String, withSuccess success: @escaping (_ dic: NSDictionary) -> Void, failure: @escaping (_ err: String) -> Void)
    {
        let URL_string = NSString(format:"%@%@",BASE_URL,sociallogin) as NSString
        let kBaseURL = NSURL(string: URL_string as String)! as URL
        
        var request = URLRequest (url: kBaseURL)
        request.httpMethod = "POST"
  
        let post = NSString(format:"social_id=%@&username=%@&gender=%@&firstname=%@&lastname=%@&image=%@",user_id,"","",fname,lName,url)
        
        let postString :String  = post as String
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                //self.activityIndicator.stopAnimating();
                failure(error as! String)
                return;
                
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            success(json!)
            return;
            
        }
        task.resume()
        
    }
  

    //MARK: Get Profile
    class func getProfileDetails(withSuccess success: @escaping (_ dic: NSDictionary) -> Void, failure: @escaping (_ err: String) -> Void)
    {
        let URL_string = NSString(format:"%@%@",BASE_URL,getProfile) as NSString
        let kBaseURL = NSURL(string: URL_string as String)! as URL
        print(URL_string)
        var request = URLRequest (url: kBaseURL)
        request.httpMethod = "POST"
        let user_id  = USerinfo.sharedInstance.user_id
        print (user_id);
        
        let post = NSString(format:"user_id=%@",user_id)
        print(post)
        let postString :String  = post as String
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                //self.activityIndicator.stopAnimating();
                failure("There is some Network Problem" )
                return;
                
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {           //check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            success(json!)
           // print(json!)
            return;
            
        }
        
        task.resume()
    }
    
    
    //MARK: - update user's details
//    class func updateProfileDetails(with dob:String , gender:String, address :String ,image:UIImage ,firstname:String ,lastname :String, age :String, phone :String ,mail_address_street :String ,mail_address_state:String,mail_address_city:String,mail_address_zip:String,mail_address_country:String,emergency_name:String,emergency_phone:String,emergency_email:String,cards:UIImage,withSuccess success: @escaping (_ dic: NSDictionary) -> Void, failure: @escaping (_ err: String) -> Void)
    
  
   
}

//
//  HistoryLogs.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 06/07/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class HistoryLogs: NSObject
{

    var id = String()
    var user_id = String()
    var date = String()
    var phone = String()
    var activity = String()
    var date_time = String()
    var location = String()
    var meteo = String ()
    var air_temprature = Bool()
    var water_temprature =  String()
    var availability =  String ()
    var visibility = String()
    var weight = String()
    var water_conditions = String()
    var dive_type = String()
    var air_start = String()
    var air_finish = String()
    var timer_underwater = String()
    var depth = String()
    var dive_partner = String()
    var image = String()
    var unit = String()
    var type = String()
    var latitude = Double()
    var longitude = Double()

    
    
}

//
//  MyAccountViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 08/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import AVFoundation

class MyAccountViewController: UIViewController,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate, UIPopoverControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate,UIPopoverPresentationControllerDelegate{

    
    var controller:SidemenuViewController? = nil    //hold the reference here.
    var viewHasMovedToRight = false
    var popover:UIPopoverController? = nil

    var picker:UIImagePickerController? = UIImagePickerController()
   // var checkImageUrl = false
    //var age = "26";
    var datePicker : UIDatePicker!
    var img_btnTag = Int()
    var selectedMedia = UIImage()
    var imageData = [String: NSData]()
    var checkCollectionView = false
    var imageUrl = [Int : String]()
    var viewHasMovedDown = false
    var imagesCopy = [Int : String]()
    var imgCopy = NSMutableArray()
    var checkdismiss = false
   // var checkDltImg = false
    
    var images : NSMutableArray = [UIImage.init(named: "camera2")!, UIImage.init(named: "camera2")!, UIImage.init(named: "camera2")! ,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!, UIImage.init(named: "camera2")!, UIImage.init(named: "camera2")!,  UIImage.init(named: "camera2")! ,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!,UIImage.init(named: "camera2")!]
    
    @IBOutlet var MyAccountTitle: UILabel!
    @IBOutlet var setimage_btn: UIButton!
    @IBOutlet var User_img: UIImageView!
    @IBOutlet var update_Profilebtn: UIButton!
    @IBOutlet var User_Name: UITextField!
    @IBOutlet var User_lastName: UITextField!
    @IBOutlet var User_Dob: UITextField!
    @IBOutlet var User_Email: UITextField!
    @IBOutlet var User_phone: UITextField!
    @IBOutlet var User_sex: UITextField!
    @IBOutlet var User_Age: UITextField!
    @IBOutlet var MD_street: UITextField!
    @IBOutlet var MD_state: UITextField!
    @IBOutlet var MD_city: UITextField!
    @IBOutlet var MD_zipCode: UITextField!
    @IBOutlet var MD_country: UITextField!
    @IBOutlet var EC_Name: UITextField!
    @IBOutlet var EC_phone: UITextField!
    @IBOutlet var Ec_email: UITextField!
    @IBOutlet var scroolview: UIScrollView!
    @IBOutlet var User_InFOview: UIView!
    
    @IBOutlet var Label_Name: UILabel!
    @IBOutlet var Label_Email: UILabel!
    @IBOutlet var Label_Sex: UILabel!
    @IBOutlet var Label_Age: UILabel!
    @IBOutlet var scr_view: UIView!
    @IBOutlet var user_img2: UIImageView!
    @IBOutlet var collectionView_cards: UICollectionView!
    @IBOutlet var view_imgPreView: UIView!
    @IBOutlet var view1: UIView!
    @IBOutlet var img_preView: UIImageView!
    @IBOutlet var viewTitle: UIView!
    @IBOutlet var view111: UIView!
    @IBOutlet var btn_dltImg: UIButton!
    
    @IBOutlet var img_profileBackground: UIImageView!
    
    @IBAction func Edit_Profile(_ sender: Any)
    {
     //   self.checkImgPreview = true
        self.view .isUserInteractionEnabled = true
        User_InFOview.isUserInteractionEnabled = true

        print(self.view)
        MyAccountTitle.text = " Edit Profile"
        setimage_btn.isEnabled = true;
        update_Profilebtn .isHidden = false;
 
    }
    
    @IBAction func update_profileAction(_ sender: Any)
    {
        
        self.updateDetails()
        MyAccountTitle.text = "My Account"
        setimage_btn.isEnabled = false;
        update_Profilebtn .isHidden = true;
        
      //  User_InFOview.isUserInteractionEnabled = false
        
    }
    
    
    @IBOutlet var topconstraint: NSLayoutConstraint!
    @IBOutlet var Listview: UIView!
    @IBOutlet var select_sex: UIButton!
    @IBAction func selectSex(_ sender: Any)
    {
        
        let alert:UIAlertController=UIAlertController(title: "Choose OPtion", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let MaleAction = UIAlertAction(title: "Male", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.maleselect()
            
        }
        let FemaleAction = UIAlertAction(title: "Female", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.femaleselet()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            
        }
        
        // Add the actions
        picker?.delegate = self
        alert.addAction(MaleAction)
        alert.addAction(FemaleAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            alert.modalPresentationStyle = .popover
            alert.popoverPresentationController!.sourceView = self.tabBarController?.tabBar//self.viewTitle // CRASH!!!
            alert.popoverPresentationController!.sourceRect = CGRect.init(x: self.view.bounds.size.width / 2 - 0.5, y:70, width: 1.0, height:  1.0)
            self.present(alert, animated: true, completion: nil)
        }

        
            
//            if select_sex.isSelected
//            {
//                select_sex .isSelected = false
//                topconstraint.constant = 40;
//                Listview.isHidden = true
//            }
//            else
//            {
//                
//                topconstraint.constant = topconstraint.constant+80;
//                Listview.isHidden = false
//                select_sex .isSelected = true
//                
//                
//            }
        
        
    }
    
    func maleselect()
    {
        User_sex.text = "Male"
  
    }
    
    func femaleselet()
    {
       User_sex.text = "Female"
    }
    
    @IBAction func Female_Select(_ sender: Any) {
        select_sex .isSelected = false
        topconstraint.constant = 40;
        Listview.isHidden = true
        User_sex.text = "Female"
        
        
    }
    
    
    @IBAction func Male_select(_ sender: Any)
    {
        
        select_sex .isSelected = false
        topconstraint.constant = 40;
        Listview.isHidden = true
        User_sex.text = "Male"
        
    }
    
    
    
    @IBAction func Image_PickAction(_ sender: Any)
    {
        self.img_btnTag = (sender as AnyObject).tag
        print(img_btnTag)
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.checkdismiss = true
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.checkdismiss = true
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            
        }
        
        // Add the actions
        picker?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
//            popover = UIPopoverController(contentViewController: alert)
//            popover!.present(from: setimage_btn.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
            
            alert.modalPresentationStyle = .popover
            alert.popoverPresentationController!.sourceView = self.tabBarController?.tabBar//self.viewTitle // CRASH!!!
            alert.popoverPresentationController!.sourceRect = CGRect.init(x: self.view.bounds.size.width / 2 - 0.5, y:70, width: 1.0, height:  1.0)
            self.present(alert, animated: true, completion: nil)

        }
        
    }
    //MARK: Menu Action $$$$$$$$
    @IBAction func Btn_Actions(_ sender: Any)
    {
        if viewHasMovedToRight == false
        {
            
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                            self.viewHasMovedToRight = true
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        else
        {
            
            
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                            self.viewHasMovedToRight = false
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        
        
    }
    func swipeview(notification:Notification) -> Void {
        
      self .swipefunction()
        
    }
    
    
    func swipefunction()
    {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                        self.viewHasMovedToRight = false
                        
                        self.controller?.didMove(toParentViewController: self)
        }, completion: { (finished) -> Void in
            
        })
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.gettingDetails()
        }
        self.imgCopy = self.images.mutableCopy() as! NSMutableArray
        self.view_imgPreView.frame.size.height = self.view_imgPreView.frame.size.height + (self.tabBarController?.tabBar.frame.size.height)!
        // Gesture
        let tapImg = UITapGestureRecognizer  (target: self, action: #selector(self.handleTap(sender:)))

     //   let tapImg = UITapGestureRecognizer(target: self, action: Selector(("handleTap:")))
        tapImg.delegate = self
        view_imgPreView.isUserInteractionEnabled = true
        view_imgPreView.addGestureRecognizer(tapImg)
        
        self.img_btnTag = 0
       
        self.scroolview.contentSize = CGSize(width: self.scr_view.frame.size.width, height: self.scr_view.frame.size.height + self.view.frame.size.height + 50)
        setimage_btn.isEnabled = false;

       // User_InFOview.isUserInteractionEnabled = false

        
        
        update_Profilebtn .isHidden = true;

        let nc = NotificationCenter.default // Note that default is now a property, not a method call
        nc.addObserver(forName:Notification.Name(rawValue:"MyAccountview"),
                       object:nil, queue:nil,
                       using:swipeview)
        
        
        controller = storyboard?.instantiateViewController(withIdentifier: "slidemenu") as! SidemenuViewController?
        addChildViewController(controller!)
        controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
        
        view.addSubview((controller?.view)!)
        controller?.didMove(toParentViewController: self)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        controller?.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        controller?.view.addGestureRecognizer(swipeLeft)
        
        let tap = UITapGestureRecognizer  (target: self, action: #selector(self.tapgesture))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
       // User_Name.text = USerinfo.sharedInstance.user_name
       // User_Email.text = USerinfo.sharedInstance.user_email
       // User_sex.text = USerinfo.sharedInstance.user_gender
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {

        if viewHasMovedDown == false
        {
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.view1.frame = CGRect(x: CGFloat(0), y: CGFloat(-self.view1.frame.size.height-20), width: CGFloat(self.view1.frame.size.width), height: CGFloat(self.view1.frame.size.height))
                            self.viewHasMovedDown = true
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }
        else
        {
            // self.view_imgPreview_back.isHidden = true
            UIView.animate(withDuration: 0.1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.view1.frame = CGRect(x: CGFloat(0), y: CGFloat(20), width: CGFloat(self.view1.frame.size.width), height: CGFloat(self.view1.frame.size.height))
                            self.viewHasMovedDown = false
                            
                            //self.controller?.didMove(toParentViewController: self)
            }, completion: { (finished) -> Void in
                
            })
            
        }

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        print("touched began")
        User_Name.resignFirstResponder()
        User_Email.resignFirstResponder()
        User_sex.resignFirstResponder()
        User_Age.resignFirstResponder()

    }

    
    func tapgesture(gesture: UIGestureRecognizer) {
        self.view.endEditing(true)
        print("touched began")
        User_Name.resignFirstResponder()
        User_Email.resignFirstResponder()
        User_sex.resignFirstResponder()
        User_Age.resignFirstResponder()
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-40), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = true
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.down:
                
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseIn,
                               animations: { () -> Void in
                                self.controller?.view.frame = CGRect(x: CGFloat(-self.view.frame.size.width-10), y: CGFloat(65), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
                                self.viewHasMovedToRight = false
                                
                                //self.controller?.didMove(toParentViewController: self)
                }, completion: { (finished) -> Void in
                    
                })
            case UISwipeGestureRecognizerDirection.up:
                
                print("Swiped up")
            default:
                break
            }
        }
    }
    
  //MARK: <<<<<<<<<<<- TextFeild Delegate >>>>>>>>>>>>>>>>
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
       
        if (textField == User_Age)
        {
            let text = textField.text
            
            if ((text?.characters.count)! <= 1)
            {
                return true

            }
            else
            {
                textField.deleteBackward()
                return true
            }
    }
    else
        {
            return true
        }
        }

    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    
    {
        self.pickUpDate(self.User_Dob)

        if (textField == User_Age)
        {
            User_Age.text = ""
           // User_Age.text = age
        }
    
        User_InFOview.isUserInteractionEnabled = true
        view.isUserInteractionEnabled = true
        
        
//        if (self.view.frame.size.width == 320)
//        {
//        
//        if (textField == User_Name)
//        {
//            scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(50)), animated: true)
//  
//        }
//        else if (textField == User_Email)
//        {
//             scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(80)), animated: true)
//        }
//        else
//        {
//            scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(130)), animated: true)
//        }
//    }
//        else if (self.view.frame.size.width == 414)
//        {
//            
//        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
   // }
        
       // textField.resignFirstResponder()
       // return true
    }
    public func textFieldDidEndEditing(_ textField: UITextField)
        
    {
        if (textField == User_Age)
        {
        //age = User_Age.text! as String
        //User_Age.text = User_Age.text!+" "+"yrs"
        }
   //     textField.resignFirstResponder()
        
      //  scroolview.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.main.async {
        if self.checkdismiss == false
        {
            self.update_Profilebtn.isHidden = true;
            self.imageUrl = self.imagesCopy
            self.images = self.imgCopy.mutableCopy() as! NSMutableArray
            
        }
        else
        {
            self.checkdismiss = false
        }
        self .swipefunction()
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {

    }
    override func viewWillAppear(_ animated: Bool) {
        
        let checkWidth = self.User_img.frame.size.width
        let checkHeight = self.User_img.frame.size.height
        var diff = CGFloat()
        if checkWidth > checkHeight
        {
            diff = checkWidth - checkHeight
            self.User_img.frame.size.width = checkHeight
            self.User_img.frame.origin.x = self.User_img.frame.origin.x + diff
            self.view111.frame.origin.x = self.view111.frame.origin.x - (diff/2)
        }
        else
        {
            diff = checkHeight - checkWidth
            self.User_img.frame.size.height = checkWidth
            self.User_img.frame.origin.x = self.User_img.frame.origin.x + diff
            self.view111.frame.origin.x = self.view111.frame.origin.x - (diff/2)
        }
        
        User_img.layer.cornerRadius = User_img.frame.size.width / 2
        User_img.layer.masksToBounds = true
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.user_img2.frame.size.width = 150
            self.user_img2.frame.size.height = 150
            self.user_img2.layer.masksToBounds = true
        }
        self.collectionView_cards.isUserInteractionEnabled = true
        DispatchQueue.main.async {
            self.collectionView_cards.reloadData()

        }
       // user_img2.layer.cornerRadius = user_img2.frame.size.width / 2
       // user_img2.layer.masksToBounds = true
    }
   // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called

    
    //MARK:- Function of datePicker
    func pickUpDate(_ textField : UITextField){
        
        //MARK: ^^^ DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        self.datePicker.layer.masksToBounds = true
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        // toolBar.backgroundColor = UIColor.red
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(MyAccountViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(MyAccountViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    // Button Done and Cancel
    func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .long
        dateFormatter1.timeStyle = .short
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM/dd/yyyy"// hh:mm a Z"
        User_Dob.text = dateFormatter1.string(from: datePicker.date)
        User_Dob.resignFirstResponder()
    }
    func cancelClick() {
        User_Dob.resignFirstResponder()
    }

    
    
    
    //MARK: - ImagePicker Delegate And Functions
    
    
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
                picker!.sourceType = UIImagePickerControllerSourceType.camera
                self .present(picker!, animated: true, completion: nil)
            } else {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
                    if granted == true {
                        self.picker!.sourceType = UIImagePickerControllerSourceType.camera
                        self .present(self.picker!, animated: true, completion: nil)
                    } else {
                        let alert = UIAlertController(title: "This app does not have access to your Camera", message: "You can enable access in Privacy Settings", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                       // Util.showMessage("Please allow the permission for Camera access in the settings", withTitle: "Alert")
                    }
                })
            }
            
           
        }
        else
        {
            openGallery()
        }
    }
    func openGallery()
    {
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(picker!, animated: true, completion: nil)
        }
        else
        {
                        popover=UIPopoverController(contentViewController: picker!)
                        popover!.present(from: (self.tabBarController?.tabBar.frame)!/*setimage_btn.frame*/, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)

        }
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
      //  let mediaType = info[UIImagePickerControllerMediaType] as! String
        var originalImage:UIImage?, editedImage:UIImage?, imageToSave:UIImage?
//        let compResult:CFComparisonResult = CFStringCompare(mediaType as NSString!, UIImagePickerController() as! CFString, CFStringCompareFlags.compareCaseInsensitive)
        
        
            editedImage = info[UIImagePickerControllerEditedImage] as! UIImage?
            originalImage = info[UIImagePickerControllerOriginalImage] as! UIImage?
            
            if ( editedImage != nil ) {
                imageToSave = editedImage
            } else {
                imageToSave = originalImage
            }
           // imgView.image = imageToSave
           // imgView.reloadInputViews()
    
        
        
        picker .dismiss(animated: true, completion: nil)
        var imageData1 = NSData()
        if self.checkCollectionView == false
        {
            User_img.image = imageToSave    //info[UIImagePickerControllerOriginalImage] as? UIImage
            img_profileBackground.image = imageToSave
            self.selectedMedia = imageToSave!   //(info[UIImagePickerControllerOriginalImage] as? UIImage)!
            imageData1  = UIImageJPEGRepresentation(self.selectedMedia, 1)! as NSData
            imageData["image"] = imageData1
        }
        else
        {
            self.images.replaceObject(at: self.img_btnTag, with: imageToSave!)
            let keys = Array(imageUrl.keys)
            print(keys)
            if keys.contains(self.img_btnTag)
            {
                self.imageUrl.removeValue(forKey: self.img_btnTag)
                print(self.imageUrl)
            }
            self.checkCollectionView = false
            DispatchQueue.main.async {
                self.collectionView_cards.reloadData()
            }
            
            if self.img_btnTag == 0
            {
                imageData["card1"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 1
            {
                imageData["card2"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 2
            {
                imageData["card3"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 3
            {
                imageData["card4"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 4
            {
                imageData["card5"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 5
            {
                imageData["card6"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 6
            {
                imageData["card7"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 7
            {
                imageData["card8"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 8
            {
                imageData["card9"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 9
            {
                imageData["card10"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 10
            {
                imageData["card11"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 11
            {
                imageData["card12"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 12
            {
                imageData["card13"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 13
            {
                imageData["card14"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
            if self.img_btnTag == 14
            {
                imageData["card15"] = UIImageJPEGRepresentation(imageToSave!, 1)! as NSData
            }
   
        }
        print(imageData)
    }
    
    public  func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imageUploadRequest (imageDict:[String:Data], parameters: [String: String]! ,imagename: String, keyName:String, completion:@escaping (_ result: NSDictionary?) -> (),  Error:@escaping (NSError) -> ()){
        
        let myUrl = NSURL(string:"http://beta.brstdev.com/Scubap/api/web/index.php/v1/updateprofile");
        
        
        let request = NSMutableURLRequest(url: myUrl! as URL)
        
        request.httpMethod = "POST";
        
        let boundary = "myRandomBoundary12345"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = createRequestBodyWith(parameters: parameters, imageDict: imageDict)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
                Error(error! as NSError)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(json!)
                
                completion(json)
                
                DispatchQueue.global(qos: .background).async
                {
                    let notificationName = Notification.Name("NotificationIdentifier")
                    
                    NotificationCenter.default.post(name: notificationName, object: nil)

                    self.gettingDetails()
                   
                }
                OperationQueue.main.addOperation {
                    [weak self] in
                    
                    Util.showMessage("Profile Updated successfully", withTitle: "Alert!")
                    Util.stopIndicator(with: (self?.view)!)
                    self?.view.isUserInteractionEnabled = true
                }
                
                
            }
            catch {
                Error(error as NSError)
                OperationQueue.main.addOperation {
                    [weak self] in
                    self?.view.isUserInteractionEnabled = true
                    Util.stopIndicator(with: (self?.view)!)
                    MBProgressHUD.hideAllHUDs(for: (self?.view)!, animated: true);
                    Util.showMessage(error as! String, withTitle: "Alert!")
                    
                }
                
            }
            
        }
        
        task.resume()
        
    }

    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

    
    func createRequestBodyWith(parameters:[String:String]?, imageDict:[String:Data]?) -> Data{
        
        let boundaryConstant = "myRandomBoundary12345";
        
        
        var uploadData = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: .utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                uploadData.append("\(value)\r\n".data(using: .utf8)!)
            }
        }
        
        if imageDict != nil {
            for (key, value) in imageDict! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: .utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(key).jpg\"\r\n".data(using: .utf8)!)
                uploadData.append("Content-Type: image/jpg\r\n\r\n".data(using: .utf8)!)
                uploadData.append(value)
                uploadData.append("\r\n".data(using: .utf8)!)
            }
        }
        uploadData.append("--\(boundaryConstant)--\r\n".data(using: .utf8)!)
        
        
        
        return uploadData
 

    }
    
    
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y -= keyboardSize.height
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y += keyboardSize.height
        }
    }

    //MARK: Getting Profile Details 
    func gettingDetails()
    {

        DispatchQueue.main.async {
            Util.showindicatior(with: (self.view)!)
        }

        Model.getProfileDetails(withSuccess: { (Result) in
            print(Result)
            self.imageUrl = [:]
            print(self.imageUrl)
            DispatchQueue.main.async {
                
            if(Result.object(forKey: "data") != nil)
            {
                let data = Result.object(forKey: "data") as! NSDictionary
                let user_data = data.object(forKey: "user_data") as! NSDictionary
                
                if user_data.value(forKey: "firstname") as? String != nil
                {
                    self.User_Name.text = (user_data.value(forKey: "firstname") as! String)
                }
                if user_data.value(forKey: "lastname") as? String != nil
                {
                    self.User_lastName.text = (user_data.value(forKey: "lastname") as! String)
                }
                if user_data.value(forKey: "dob") as? String != nil
                {
                    self.User_Dob.text = (user_data.value(forKey: "dob") as! String)
                }
                if user_data.value(forKey: "gender") as? String != nil
                {
                    self.User_sex.text = (user_data.value(forKey: "gender") as! String)
                }
                if user_data.value(forKey: "age") as? String != nil
                {
                    self.User_Age.text = (user_data.value(forKey: "age") as! String)
                }
                if user_data.value(forKey: "email") as? String != nil
                {
                    self.User_Email.text = (user_data.value(forKey: "email") as! String)
                }
                if user_data.value(forKey: "phone") as? String != nil
                {
                    self.User_phone.text = (user_data.value(forKey: "phone") as! String)
                }
                if user_data.value(forKey: "mail_address_street") as? String != nil
                {
                    self.MD_street.text = (user_data.value(forKey: "mail_address_street") as! String)
                }
                if user_data.value(forKey: "mail_address_state") as? String != nil
                {
                    self.MD_state.text = (user_data.value(forKey: "mail_address_state") as! String)
                }
                if user_data.value(forKey: "mail_address_city") as? String != nil
                {
                    self.MD_city.text = (user_data.value(forKey: "mail_address_city") as! String)
                }
                if user_data.value(forKey: "mail_address_zip") as? String != nil
                {
                    self.MD_zipCode.text = (user_data.value(forKey: "mail_address_zip") as! String)
                }
                if user_data.value(forKey: "mail_address_country") as? String != nil
                {
                    self.MD_country.text = (user_data.value(forKey: "mail_address_country") as! String)
                }
                // cards $$$$$$$$$$$$$$$
          //      if user_data.value(forKey: "cards") as? String != nil
            //    {
                    let urlStr = "http://beta.brstdev.com/Scubap/common/uploads/cards_image/"
                    if user_data.value(forKey: "card1") as? String != ""
                    {
                        if((user_data.value(forKey: "card1")) is NSNull)
                        {
                        
                        } else {
                        let url11 = (user_data.value(forKey: "card1") as! String)
                        let url1 = urlStr + url11
                        self.imageUrl[0] = url1
                        }
                    }
                if user_data.value(forKey: "card2") as? String != ""
                {
                    if((user_data.value(forKey: "card2")) is NSNull)
                    {
                        
                    } else {
                    let url12 = (user_data.value(forKey: "card2") as! String)
                    let url2 = urlStr + url12
                    self.imageUrl[1] = url2
                    }
                }
                if user_data.value(forKey: "card3") as? String != ""
                {
                    if((user_data.value(forKey: "card3")) is NSNull)
                    {
                        
                    } else {
                    let url13 = (user_data.value(forKey: "card3") as! String)
                    let url3 = urlStr + url13
                    self.imageUrl[2] = url3
                    }
                }
                if user_data.value(forKey: "card4") as? String != ""
                {
                    if((user_data.value(forKey: "card4")) is NSNull)
                    {
                        
                    } else {
                    let url14 = (user_data.value(forKey: "card4") as! String)
                    let url4 = urlStr + url14
                    self.imageUrl[3] = url4
                    }
                }
                if user_data.value(forKey: "card5") as? String != nil
                {
                    let url15 = (user_data.value(forKey: "card5") as! String)
                    let url5 = urlStr + url15
                    self.imageUrl[4] = url5
                }
                if user_data.value(forKey: "card6") as? String != ""
                {
                    if((user_data.value(forKey: "card6")) is NSNull)
                    {
                        
                    } else {
                    let url16 = (user_data.value(forKey: "card6") as! String)
                    let url6 = urlStr + url16
                    self.imageUrl[5] = url6
                    }
                }
                if user_data.value(forKey: "card7") as? String != ""
                {
                    if((user_data.value(forKey: "card7")) is NSNull)
                    {
                        
                    } else {
                    let url17 = (user_data.value(forKey: "card7") as! String)
                    let url7 = urlStr + url17
                    self.imageUrl[6] = url7
                    }
                }
                if user_data.value(forKey: "card8") as? String != ""
                {
                    if((user_data.value(forKey: "card8")) is NSNull)
                    {
                        
                    } else {
                    let url18 = (user_data.value(forKey: "card8") as! String)
                    let url8 = urlStr + url18
                    self.imageUrl[7] = url8
                    }
                }
                if user_data.value(forKey: "card9") as? String != ""
                {
                    if((user_data.value(forKey: "card9")) is NSNull)
                    {
                        
                    } else {
                    let url19 = (user_data.value(forKey: "card9") as! String)
                    let url9 = urlStr + url19
                    self.imageUrl[8] = url9
                    }
                }
                if user_data.value(forKey: "card10") as? String != ""
                {
                    if((user_data.value(forKey: "card10")) is NSNull)
                    {
                        
                    } else {
                    let url110 = (user_data.value(forKey: "card10") as! String)
                    let url10 = urlStr + url110
                    self.imageUrl[9] = url10
                    }
                }
                if user_data.value(forKey: "card11") as? String != ""
                {
                    if((user_data.value(forKey: "card11")) is NSNull)
                    {
                        
                    } else {
                    let url111 = (user_data.value(forKey: "card11") as! String)
                    let url21 = urlStr + url111
                    self.imageUrl[10] = url21
                    }
                }
                if user_data.value(forKey: "card12") as? String != ""
                {
                    if((user_data.value(forKey: "card12")) is NSNull)
                    {
                        
                    } else {
                    let url112 = (user_data.value(forKey: "card12") as! String)
                    let url22 = urlStr + url112
                    self.imageUrl[11] = url22
                    }
                }
                if user_data.value(forKey: "card13") as? String != ""
                {
                    if((user_data.value(forKey: "card13")) is NSNull)
                    {
                        
                    } else {
                    let url113 = (user_data.value(forKey: "card13") as! String)
                    let url23 = urlStr + url113
                    self.imageUrl[12] = url23
                    }
                }
                if user_data.value(forKey: "card14") as? String != ""
                {
                    if((user_data.value(forKey: "card14")) is NSNull)
                    {
                        
                    } else {
                    let url114 = (user_data.value(forKey: "card14") as! String)
                    let url24 = urlStr + url114
                    self.imageUrl[13] = url24
                    }
                }
                if user_data.value(forKey: "card15") as? String != ""
                {
                    if((user_data.value(forKey: "card15")) is NSNull)
                    {
                        
                    } else {
                    let url115 = (user_data.value(forKey: "card15") as! String)
                    let url25 = urlStr + url115
                    self.imageUrl[14] = url25
                    }
                }
                DispatchQueue.main.async {
                    self.imagesCopy = self.imageUrl
                    self.collectionView_cards.reloadData()
                }
                //     self.checkImageUrl = true
                //   print(self.imageUrl)
                   // self.user_img2.sd_setShowActivityIndicatorView(true)
                  //  self.user_img2.sd_setIndicatorStyle(.gray)
                 //   self.user_img2.sd_setImage(with:  URL(string: url ))
                    
              //  }
                
                // profile pic $$$$$$$$$$$$$$$
                if user_data.value(forKey: "image") as? String != nil
                {
                    let str = user_data.value(forKey: "image") as! String
                    if str.contains("https://scontent")
                    {
                        let fid = user_data.value(forKey: "social_id") as! String
                        let imgURLString = "http://graph.facebook.com/" + fid + "/picture?type=large"
                        
                        self.User_img.sd_setShowActivityIndicatorView(true)
                        self.User_img.sd_setIndicatorStyle(.gray)
                        self.User_img.sd_setImage(with:  URL(string: imgURLString ))
                        self.img_profileBackground.sd_setImage(with: URL(string:imgURLString ))
                    }
                    else
                    {
                        let urlStr = "http://beta.brstdev.com/Scubap/common/uploads/profile_images/"
                        let url11 = (user_data.value(forKey: "image") as! String)
                        let url = urlStr + url11
                        
                        self.User_img.sd_setShowActivityIndicatorView(true)
                        self.User_img.sd_setIndicatorStyle(.gray)
                        self.User_img.sd_setImage(with:  URL(string: url ))
                         self.img_profileBackground.sd_setImage(with: URL(string:url ))
                    }
                }

//                if user_data.value(forKey: "image") as? String != nil
//                {
//                    let urlStr = "http://beta.brstdev.com/Scubap/common/uploads/profile_images/"
//                    let url11 = (user_data.value(forKey: "image") as! String)
//                    let url = urlStr + url11
//                    
//                    print(url)
//                    
//                    self.User_img.sd_setShowActivityIndicatorView(true)
//                    self.User_img.sd_setIndicatorStyle(.gray)
//                    self.User_img.sd_setImage(with:  URL(string: url ))
//                    
//                }
         
                if user_data.value(forKey: "emergency_name") as? String != nil
                {
                    self.EC_Name.text = (user_data.value(forKey: "emergency_name") as! String)
                }
                if user_data.value(forKey: "emergency_phone") as? String != nil
                {
                    self.EC_phone.text = (user_data.value(forKey: "emergency_phone") as! String)
                }
                if user_data.value(forKey: "emergency_email") as? String != nil
                {
                    self.Ec_email.text = (user_data.value(forKey: "emergency_email") as! String)
                }

            }
            
            }
           
            DispatchQueue.main.asyncAfter(deadline: .now() + 3)
            {
                Util.stopIndicator(with: (self.view)!)
            }
            
            
        }) { (error) in
            print (error)
            Util.stopIndicator(with: (self.view)!)

        }
        
    }
    

    //MARK: Getting Profile Details
    func updateDetails()
    {
        DispatchQueue.main.async {
           // self.view .isUserInteractionEnabled = false
            self.update_Profilebtn.isHidden = true;

            Util.showindicatior(with: (self.view)!)
        }
        
        let user_id  = USerinfo.sharedInstance.user_id

        let parameters = ["user_id":user_id,"dob":self.User_Dob.text!, "gender": self.User_sex.text!, "address": "address", "firstname": self.User_Name.text!,"lastname": self.User_lastName.text!, "age": self.User_Age.text!,"phone": self.User_phone.text!, "mail_address_street": self.MD_street.text!, "mail_address_state": self.MD_state.text!, "mail_address_city": self.MD_city.text!, "mail_address_zip": self.MD_zipCode.text!, "mail_address_country": self.MD_country.text!, "emergency_name": self.EC_Name.text!, "emergency_phone": self.EC_phone.text!, "emergency_email": self.Ec_email.text!]
            print(parameters)

        self.imageUploadRequest(imageDict:imageData as [String : Data] , parameters: parameters , imagename: "image", keyName: "image", completion: { (result) in
            
            print("error")
            
        }, Error: { (error) in
            print("error")
            self.view .isUserInteractionEnabled = true
            Util.stopIndicator(with: (self.view)!)

        })
        
    
}
    
    //MARK: Collectionview Delegate and Datasource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
       
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        let cell = collectionView_cards.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MediaCollectionViewCell
        
        cell.layer.cornerRadius = 5;
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.white.cgColor

        let keys = Array(imageUrl.keys)
        print(keys)
        if keys.contains(indexPath.row)
        {
            print(self.imageUrl[indexPath.row]!)
            if self.imageUrl[indexPath.row] != ""
            {
                cell.img.sd_setShowActivityIndicatorView(true)
                cell.img.sd_setIndicatorStyle(.gray)
                cell.img.sd_setImage(with:  URL(string: (self.imageUrl[indexPath.row])!))
            }
            else
            {
                cell.img.image = self.images[indexPath.row] as? UIImage
            }
           
        }
        else
        {
            cell.img.image = self.images[indexPath.row] as? UIImage
        }

        cell.img.contentMode = .scaleAspectFill
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.contentMode = .scaleAspectFill
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let flowLayout: UICollectionViewFlowLayout? = (collectionView_cards.collectionViewLayout as? UICollectionViewFlowLayout)
        let cellWidth: CGFloat = collectionView_cards.frame.width / 2
        let cellHeight: CGFloat = collectionView_cards.frame.size.height/1.3
        flowLayout?.minimumInteritemSpacing = 10
        flowLayout?.minimumLineSpacing = 10//cellWidth/3
        flowLayout?.itemSize = CGSize(width: cellWidth, height: cellHeight)
        return CGSize(width: cellWidth-10, height: cellHeight-10)
        //return (flowLayout?.itemSize)!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

//        if self.checkImgPreview == false
//        {
//            var checkImg = false
//
//            let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
//            
//            let preView = UIAlertAction(title: "Preview", style: UIAlertActionStyle.default)
//            {
//                UIAlertAction in
//                DispatchQueue.main.async {
//                    if checkImg == true
//                    {
//                        checkImg = false
//                        self.img_preView.image = self.images[indexPath.row] as! UIImage
//                        self.tabBarController?.tabBar.isHidden = true
//                        self.view_imgPreView.isHidden = false
//                    }
//                    else
//                    {
//                        self.img_preView.sd_setShowActivityIndicatorView(true)
//                        self.img_preView.sd_setIndicatorStyle(.gray)
//                        self.img_preView.sd_setImage(with:  URL(string: (self.imageUrl[indexPath.row])!))
//                        self.tabBarController?.tabBar.isHidden = true
//                        self.view_imgPreView.isHidden = false
//                    }
//                }
//                
//            }
//            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
//            {
//                UIAlertAction in
//                
//            }
//            
//            // Add the actions
//            picker?.delegate = self
//            
//            // image preview option
//            let keys = Array(imageUrl.keys)
//            if keys.contains(indexPath.row)
//            {
//                print(self.imageUrl[indexPath.row]!)
//                if self.imageUrl[indexPath.row] != ""
//                {
//                    alert.addAction(preView)
//                }
//                else
//                {
//                    
//                }
//                
//            }
//            else if self.images.object(at: indexPath.row) as? UIImage != UIImage.init(named: "camera2")
//            {
//                checkImg = true
//                alert.addAction(preView)
//            }
//            
//            
//            alert.addAction(cancelAction)
//            if UIDevice.current.userInterfaceIdiom == .phone
//            {
//                self.present(alert, animated: true, completion: nil)
//            }
//            else
//            {
//                popover = UIPopoverController(contentViewController: alert)
//                popover!.present(from: setimage_btn.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
//            }
//
//
//        }
//        else
//        {
            self.img_btnTag = indexPath.row
            self.checkCollectionView = true
            var checkImg = false
            let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
            let preView = UIAlertAction(title: "Preview", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                DispatchQueue.main.async {
                if checkImg == true
                {
                    checkImg = false
                    self.img_preView.image = self.images[indexPath.row] as? UIImage
                    self.tabBarController?.tabBar.isHidden = true
                    self.view_imgPreView.isHidden = false
                }
                else
                {
                    self.img_preView.sd_setShowActivityIndicatorView(true)
                    self.img_preView.sd_setIndicatorStyle(.gray)
                    self.img_preView.sd_setImage(with:  URL(string: (self.imageUrl[indexPath.row])!))
                    self.tabBarController?.tabBar.isHidden = true
                    self.view_imgPreView.isHidden = false
                }
                }
           
            }

            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.checkdismiss = true
                self.openCamera()
            
            }
            let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.checkdismiss = true
                self.openGallery()
            }
       
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
            
            }
        
            // Add the actions
            picker?.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(galleryAction)
            // image preview option
            let keys = Array(imageUrl.keys)
            if keys.contains(indexPath.row)
            {
                print(self.imageUrl[indexPath.row]!)
                if self.imageUrl[indexPath.row] != ""
                {
                    alert.addAction(preView)
                }
                else
                {
                
                }
            
            }
            else if self.images.object(at: indexPath.row) as? UIImage != UIImage.init(named: "camera2")
            {
                checkImg = true
                alert.addAction(preView)
            }
        
        
            alert.addAction(cancelAction)
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                alert.modalPresentationStyle = .popover
                alert.popoverPresentationController!.sourceView = self.tabBarController?.tabBar//self.viewTitle // CRASH!!!
                alert.popoverPresentationController!.sourceRect = CGRect.init(x: self.view.bounds.size.width / 2 - 0.5, y:70, width: 1.0, height:  1.0)
                self.present(alert, animated: true, completion: nil)
    
        }
       // }
    }
    
    
    @IBAction func btn_imgPreview_back(_ sender: Any)
    {
        self.tabBarController?.tabBar.isHidden = false
        self.view_imgPreView.isHidden = true
    }
    
    @IBAction func btn_imgPreview_dlt(_ sender: Any)
    {
        let alert : UIAlertController = UIAlertController(title : "Are You Sure To Delete", message : nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let delete = UIAlertAction(title: "Delete", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            DispatchQueue.main.async {

            let keys = Array(self.imageUrl.keys)
            print(keys)
            if keys.contains(self.img_btnTag)
            {
                if self.imageUrl[self.img_btnTag] != ""
                {
                    self.imageUrl.removeValue(forKey: self.img_btnTag)
                    self.imagesCopy.removeValue(forKey: self.img_btnTag)
                    self.images[self.img_btnTag] = UIImage.init(named: "camera2")!
                    self.imgCopy[self.img_btnTag] = UIImage.init(named: "camera2")!
                    print(self.imageUrl.count)
                    print(self.imagesCopy.count)
                }
                else
                {
                    self.images[self.img_btnTag] = UIImage.init(named: "camera2")!
                    self.imgCopy[self.img_btnTag] = UIImage.init(named: "camera2")!
                    print(self.images)
                    print(self.imgCopy)
                }
            }
            else
            {
                self.images[self.img_btnTag] = UIImage.init(named: "camera2")!
                self.imgCopy[self.img_btnTag] = UIImage.init(named: "camera2")!
                print(self.images)
                print(self.imgCopy)
            }
            
           // DispatchQueue.global(qos: .background).async {
                //self.checkDltImg = true
                self.tabBarController?.tabBar.isHidden = false
                DispatchQueue.main.async {
                    self.collectionView_cards.reloadData()
                }
                self.view_imgPreView.isHidden = true
                self.deleteImg()
            }
        }
        let cancel  = UIAlertAction(title: "Cancel",  style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            
        }
        alert.addAction(delete)
        alert.addAction(cancel)
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            alert.modalPresentationStyle = .popover
            alert.popoverPresentationController!.sourceView = self.view1//self.tabBarController?.tabBar
            alert.popoverPresentationController!.sourceRect = CGRect.init(x: self.view.bounds.size.width/1.1 , y: 70, width: 1.0, height: 1.0)
            self.present(alert, animated: true, completion: nil)
        }
        
        

    }
    func deleteImg ()
    {
        let cardNum = self.img_btnTag + 1
        
        var request = URLRequest(url: URL(string: "http://beta.brstdev.com/Scubap/api/web/index.php/v1/deleteCard")!)
        request.httpMethod = "POST"
        let user_id  = USerinfo.sharedInstance.user_id
        let string1 = "user_id=\(user_id)&card_num=\(String(cardNum))"
        
        request.httpBody = string1.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
               
            }
            
            let responseString = String(data: data, encoding: .utf8)
//            DispatchQueue.main.async {
//                self.gettingDetails()
//                //self.view_imgPreView.isHidden = true
//            }
            
        }
        task.resume()
    }
    
}




 //
//  ViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 06/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//


import UIKit
import MapKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class ViewController: UIViewController,FBSDKGraphRequestConnectionDelegate,MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource{
    
    var array = [NSMutableArray]()
    var dict = [Any]()
    var imageName: String!
    var coordinates: [[Double]]!
    var names:[String]!
    var addresses:[String]!
    var phones:[String]!
    var weather:[String]!
    var water :[String]!
    var conditions :[String]!
    var visibility :[String]!
    var itle: [String]!
    var country = NSArray()
    var diveSiteArray = NSArray()
    var GPSarray = NSArray()
    var Region = NSArray()
    var SubRegion = NSArray()
    
    var login  = FBSDKLoginManager()
    var fbAccessToken = String()
    var LocationArray = NSMutableArray()
    var oceanArray = NSArray()
   // var finalStateArray = NSArray()
   // var finalCitiesArray = NSArray()
    var subcatagoriesArray = NSMutableArray()
    var DuplicatesubcatagoriesArray = NSMutableArray()
    var section_index = Int()
    var item_selected = String()
    var Scuba_Tropics_copy = NSMutableArray()
    var section_array = NSMutableArray()
    var reSetData_array = NSMutableArray()
    var checkDiveSiteName = false
    var checkTable = false
    var checkAddLog = false
    
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var resetBtn: UIButton!
    @IBOutlet var lbl_searchSpot_title: UILabel!

    
    
    @IBOutlet var tapbottoncopnstraint: NSLayoutConstraint!
    
    
    //some lat and long values
   
    struct Location
    {
        let title: String
        let latitude: Double
        let longitude: Double
    }
    
    @IBOutlet var table: UITableView!
    
    
    @IBAction func button_action(_ sender: Any)
    {
    
        self.performSegue(withIdentifier: "LoginPage", sender: sender)
        
    }
    
    @IBAction func Addlogbtn(_ sender: Any)
    {
          self.performSegue(withIdentifier: "LoginPage", sender: sender)
        
    }
    
    
      @IBOutlet var searchbtn: UIButton!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoginPage"
        {
            
            _ = segue.destination as! LoginViewController
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchSpot()
       
        self.tabBarController?.tabBar.isHidden = true
        
//        oceanArray = self.readJson(resources: "oceans") as NSArray
//        print(oceanArray)
//        country = self.readJson(resources: "COUNTRY")
//        diveSiteArray = self.readJson(resources: "DIVE SITE NAME")
//        Region = self.readJson(resources: "Region")
//        SubRegion = self.readJson(resources: "Sub Region")
//        DuplicatesubcatagoriesArray .add(oceanArray)
//        DuplicatesubcatagoriesArray .add(country)
//        DuplicatesubcatagoriesArray .add(Region)
//        DuplicatesubcatagoriesArray.add(SubRegion)
//        DuplicatesubcatagoriesArray.add(diveSiteArray)
//        self.set_Map()
//        Util .setupview(singleview: searchbtn);
//        
        
    }

    
//    private func readJson(resources: String) -> NSArray {
//        var resultArray = NSArray()
//        
//        do {
//            if let file = Bundle.main.url(forResource: resources, withExtension: "json") {
//                let data = try Data(contentsOf: file)
//                let json = try JSONSerialization.jsonObject(with: data, options: [])
//                if let object = json as? [String: Any] {
//                    // json is a dictionary
//                    resultArray = object[resources] as! NSArray
//                    
//                } else if json is [Any] {
//                    // json is an array
//                    
//                } else {
//                    print("JSON is invalid")
//                }
//                
//                return resultArray
//                
//            } else {
//                print("no file")
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
//        return resultArray
//        
//    }
    
      override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.searchSpot()
        
      //  self.hidecrooslogtype()
        self.searchMain_view.isHidden = true
       // self.Log_Typeview.isHidden = true
        if self.checkAddLog == true
        {
            self.checkAddLog = false
           // self.Log_Typeview.isHidden = false
        }
    }
    //MARK: MAPView More.. Button action here
    func MoreButtonAction()
    {
        let locationLogImage = Notification.Name("NotificationIdentifier")
        
        NotificationCenter.default.post(name: locationLogImage, object: nil)
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "gallery") as! ScubaGalleryViewController
        secondViewController.checkLogImages = true
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    //Mark :- Show locations in Map Functions :-
    func set_Map()
    {
        self.searchMainCatagorysetup()
    
      Model.getLocationlogs(/*with: 1,*/ withSuccess: { (Result) in
      //  print(Result)
        
        if(Result.object(forKey: "data") != nil)
        {
            let data :NSArray = Result.object(forKey: "data") as! NSArray
            self.LocationArray = NSMutableArray()
            self.LocationArray = Util.ParseLocation(with: data) as! NSMutableArray
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
           self .setLocalMap()
        }
    
        
      }) { (error) in
        print (error)
        
        }
        
    }
    
    func setLocalMap()
    {
        self.mapView.delegate = self;
        
        for i in 0..<LocationArray.count
        {
            let Location : Location_List = LocationArray .object(at: i) as! Location_List

            let point = StarbucksAnnotation(coordinate: CLLocationCoordinate2D(latitude: Location.latitude , longitude: Location.longitude ))
                        //point.image = UIImage(named: "search POIimg\(i+1).jpg")
            point.title = Location.activity
            point.address = Location.location
            //point.phone = phones[i]
            point.conditions=Location.water_conditions
            point.weather = Location.meteo
            point.water = Location.water_temprature
            point.visibility = Location.visibility
            point.latitude = String (Location.latitude)
            point.longitude = String (Location.longitude)
            self.mapView.addAnnotation(point)
            self.mapView.showAnnotations( self.mapView.annotations, animated: true)
        }
        
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation
        {
            return nil
        }
        
        var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "octopus_mapIcon")
     //   print (annotation.title!!)
        
//        if annotation.title!! == "Snorking"
//        {
//            annotationView?.image = UIImage(named: "octopus_mapIcon")
//            
//        }
//        else if annotation.title!! == "Surfing"
//        {
//            annotationView?.image = UIImage(named: "octopus_mapIcon")
//            
//        }
//        else
//        {
//            annotationView?.image = UIImage(named: "octopus_mapIcon")
//            
//        }
        
        
        
        return annotationView
    }
    
    
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let starbucksAnnotation = view.annotation as! StarbucksAnnotation
        let views = Bundle.main.loadNibNamed("CustomCalloutView", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCalloutView
        calloutView.starbucksName.text = starbucksAnnotation.name
        calloutView.starbucksAddress.text = starbucksAnnotation.address
        calloutView.starbuckscord.text = starbucksAnnotation.latitude  + "," + "  " + starbucksAnnotation.longitude
       // calloutView.starbucksPhone.text = starbucksAnnotation.phone
        calloutView.starbuckswater.text = starbucksAnnotation.water
        calloutView.starbucksweather.text = starbucksAnnotation.weather
        calloutView.starbucksconditions.text = starbucksAnnotation.conditions
        calloutView.starbucksvisibility.text = starbucksAnnotation.visibility
        
       // calloutView.starbucksImage.image = starbucksAnnotation.image
        // 3
        calloutView.center = CGPoint(x: view.bounds.size.width / 3+140, y: +calloutView.bounds.size.height*0.52+20)
        view.addSubview(calloutView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
    
    
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
    }
    
    //MARK: Delegate Fot Tableview and PopUpview Function :-
    var subcatNameArray = NSMutableArray()
    var arrayForBool = NSMutableArray()
    var selectedSection = Int()
    var tap = UITapGestureRecognizer()
    @IBOutlet var searchMain_view: UIView!
    @IBOutlet var searchbtnMain: UIButton!
    //MARK: <<<< Search Button >>>>>>
    @IBAction func searchbtnclicked(_ sender: Any)
    {
        self.lbl_searchSpot_title.text = "SEARCH SPOT"

      if (searchbtnMain.isSelected)
      {
        self.mapView.alpha = 1;
        searchMain_view.isHidden = true
        
        searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        searchMain_view.isHidden = true
        UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
            self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.searchMain_view.transform = CGAffineTransform.identity
                })
            })
        })

        
        
        
        searchbtnMain.isSelected = false
        
        }
        else
      {
        self.mapView.alpha = 0.5;

        searchMain_view.isHidden = false
        searchMain_view.isHidden = false
        searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        //Activity_Indicator
        UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
            self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.searchMain_view.transform = CGAffineTransform.identity
                })
            })
        })
        

        searchbtnMain.isSelected = true
        }
        
    }
    
    
    @IBAction func searchcrossbtnAction(_ sender: Any)
    {
        self.reSetData()
        self.checkDiveSiteName = false
        self.checkTable = false
        self.table.reloadSectionIndexTitles()
        self.table.reloadData()

        self.mapView.alpha = 1;
        searchMain_view.isHidden = true
         searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        searchMain_view.isHidden = true
        UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
            self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                self.searchMain_view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.searchMain_view.transform = CGAffineTransform.identity
                })
            })
        })

        searchbtnMain.isSelected = false
        
    }
    
    
    
    
    
    // MARK :-   Places searching Methods:-
    
    
    func searchMainCatagorysetup()
    {
        subcatNameArray = ["Spot Type","Country","Region","Sub Region"] //,"Activity" "Ocean",,"City"
        
        // Subcatimage = ["","","","","","","","","",""]
        for i in (0..<subcatNameArray.count)
        {
           // print(subcatNameArray)
           // print(subcatNameArray.count)
            arrayForBool.add(false)
        }
    }
    
//    // MARK: -<<<<<<< Tableview Deleate and Datasources >>>>>>>>>
//    func numberOfSections(in tableView: UITableView) -> Int{
//        
//        return subcatNameArray.count
//    }
//    
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        
//    
//        if  (arrayForBool[section] as! Bool == true as Bool)
//        {
//            if (section == selectedSection)
//            {
//                var array = NSArray()
//                array = subcatagoriesArray .objects(at: [section]) as NSArray
//                
//                return (array[0] as! NSArray) .count
//            }
//            else
//            {
//                return 0;
//            }
//          
//        }
//        else
//        {
//            return 0;
//            
//        }
//        
//        
//    }
//    
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        let cell:SlidemenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! SlidemenuTableViewCell
//        
//        let sectionView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.table.frame.size.width ), height: CGFloat(45)))
//        sectionView.removeFromSuperview()
////        sectionView.addSubview(viewLabel)
//        
//
//       // cell.contentView = nil;
//        
//        
//        
//        if let nameLabel = cell.viewWithTag(100) as? UILabel{
//             var arr = NSArray()
//            if (indexPath.section == 0)
//            {
//                
//                arr = subcatagoriesArray[indexPath.section] as! NSArray
//                let str = arr[indexPath.row] as! String
//                nameLabel.text = ""
//                nameLabel.text = (str as NSString) as String
//            }
//            else
//            {
//                
//                
//                arr = subcatagoriesArray[indexPath.section] as! NSArray
//                let locationDict = arr[indexPath.row] as! NSDictionary
//                let  strname = (locationDict["name"] as! NSString) as String
//                nameLabel.text = ""
//                nameLabel.text = (strname as NSString) as String
//            }
//        }
//        else{
//            let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.table.frame.size.width-20 ), height: CGFloat(44)))
//
//            viewLabel.backgroundColor = UIColor.clear
//            viewLabel.textColor = UIColor.white
//            viewLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
//            viewLabel.text = ""
//            viewLabel.tag = 100;
//            
//            
//            if (indexPath.section == 0)
//            {
//                 var arr = NSArray()
//                arr = subcatagoriesArray[indexPath.section] as! NSArray
//                let str = arr[indexPath.row] as! String
//                viewLabel.text = ""
//                viewLabel.text = (str as NSString) as String
//            }
//            else
//            {
//                 var arr = NSArray()
//                arr = subcatagoriesArray[indexPath.section] as! NSArray
//                let locationDict = arr[indexPath.row] as! NSDictionary
//                let  strname = (locationDict["name"] as! NSString) as String
//                viewLabel.text = ""
//                viewLabel.text = (strname as NSString) as String
//            }
//            
//            
//            
//                        
//           
//            
//            
//            cell.addSubview(viewLabel)
//        }
//        
//          
////        sectionView.addSubview(viewLabel)
//        /********** Add a custom Separator with Section view *******************/
//        let separatorLineView = UIView(frame: CGRect(x: CGFloat(10), y: CGFloat(44), width: CGFloat(self.table.frame.size.width-20), height: CGFloat(1)))
//        separatorLineView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
//        sectionView.addSubview(separatorLineView)
//        
//       
//        
//        
//        
//        return cell
//        
//    }
//    
//    
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        let arr = subcatagoriesArray[indexPath.section] as! NSArray
//   
//        //let arr = array[0] as! NSArray
//        print(arr)
//        
//       
//        if (indexPath.section == 0)
//        {
//            
//           
//
//            let str = arr.object(at: indexPath.row) as! String
//            let resultPredicate = NSPredicate(format: "ocean contains[c]%@", str)
//            var countryarray = DuplicatesubcatagoriesArray .objects(at: [1])
//            let countryarr = countryarray[0] as! NSArray
//            let arrayy = countryarr.filtered(using: resultPredicate)
//            print(array)
//            oceanArray = NSArray ()
//            oceanArray = arrayy as NSArray
//            print(subcatagoriesArray.object(at: indexPath.row) as! NSArray)
//            subcatNameArray[indexPath.section] = arr .object(at: indexPath.row)
//           
//        }
//        else if (indexPath.section == 1)
//        {
//            
//            let locationDict = arr[indexPath.row] as! NSDictionary
//            let locationName = locationDict["name"] as! NSString
//             let locationid = locationDict["id"] as! NSString
//            subcatNameArray[indexPath.section] = locationName
//            let resultPredicate = NSPredicate(format: "country_id contains[c]%@", locationid)
//            var countryarray = DuplicatesubcatagoriesArray .objects(at: [2])
//            let countryarr = countryarray[0] as! NSArray
//            let arrayy = countryarr.filtered(using: resultPredicate)
//            country = NSArray()
//               country = arrayy as NSArray
//            print(array)
//            print(subcatagoriesArray.object(at: indexPath.row) as! NSArray)
//     
//
//        }
//        else if (indexPath.section == 2)
//        {
//            let locationDict = arr[indexPath.row] as! NSDictionary
//            let locationName = locationDict["name"] as! NSString
//            let locationid = locationDict["id"] as! NSString
//            subcatNameArray[indexPath.section] = locationName
//            let resultPredicate = NSPredicate(format: "state_id contains[c]%@", locationid)
//            var countryarray = DuplicatesubcatagoriesArray .objects(at: [3])
//            let countryarr = countryarray[0] as! NSArray
//            let arrayy = countryarr.filtered(using: resultPredicate)
//            diveSiteArray = NSArray ()
//            diveSiteArray = arrayy as NSArray
//            print(array)
//            print(subcatagoriesArray.object(at: indexPath.row) as! NSArray)
//      
//           
//            
//        }
//        else if (indexPath.section == 3)
//        {
//            let locationDict = arr[indexPath.row] as! NSDictionary
//            let locationName = locationDict["name"] as! NSString
//            let locationid = locationDict["id"] as! NSString
//            subcatNameArray[indexPath.section] = locationName
//           
//            
//            
//        }
//     
//
//        
//        
//        arrayForBool[indexPath.section] = false
//        self.table.reloadSections([indexPath.section], with: .automatic)
//        
//    }
//    
//    
//    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
//    {
//        
//        
//        let sectionView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.table.frame.size.width ), height: CGFloat(53)))
//        sectionView.tag = section
//        let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.table.frame.size.width-20 ), height: CGFloat(45)))
//        let imageview = UIImageView (frame: CGRect(x: CGFloat(self.table.frame.size.width-50), y: CGFloat(15), width: CGFloat(20 ), height: CGFloat(20)))
//         imageview.image = UIImage(named: "arw-dwn")
//         imageview.contentMode = UIViewContentMode.scaleAspectFit;
//        viewLabel.backgroundColor = UIColor(red: 51/255, green: 53/255, blue: 53/255, alpha: 1.0)
//        
//        viewLabel.textColor = UIColor.white
//        viewLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
//        let str1 =  "  " as String
//        var str2 = ""
//        str2 = subcatNameArray[section] as! String
//        let str  =   str1 + str2
//        viewLabel.text = (str as NSString) as String
//        sectionView.addSubview(viewLabel)
//        /********** Add a custom Separator with Section view *******************/
//        let separatorLineView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(45), width: CGFloat(self.table.frame.size.width), height: CGFloat(8)))
//        separatorLineView.backgroundColor = UIColor.clear
//        sectionView.addSubview(separatorLineView)
//        sectionView.addSubview(imageview)
//        viewLabel.layer.borderColor = UIColor.lightGray.cgColor
//        viewLabel.layer.borderWidth = 1
//        viewLabel.layer.masksToBounds = true
//        
//        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
//        tap.numberOfTapsRequired = 1
//        tap.numberOfTouchesRequired = 1
//        sectionView.tag = section+100;
//        sectionView.addGestureRecognizer(tap)
//        return sectionView
//        
//    }
//    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
//    {
//        return 53
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 45
//    }
//    
//    func handleTap(gestureRecognizer: UIGestureRecognizer)
//    {
//        selectedSection = (gestureRecognizer.view?.tag)! as Int
//        print (selectedSection)
//        selectedSection = selectedSection - 100
//      //  self.table.reloadData()
//        
//        
//        if selectedSection == 0
//        {
//           // Region = ["South-Central Asia","Europe","North Africa","Polynesia, Oceania","Middle Africa","The Caribbean","Antarctica","South America","Asia","Central America","Northern America","Africa"]
//            
//            subcatagoriesArray .add(oceanArray)
//        }
//        else if selectedSection == 1
//        {
//         subcatagoriesArray .add(country)
//        }
//        else if selectedSection == 2
//        {
//            subcatagoriesArray.add(Region)
//        }
//        else if selectedSection == 3
//        {
//           subcatagoriesArray.add(SubRegion)
//        }
//        else if selectedSection == 4
//        {
//            subcatagoriesArray.add(diveSiteArray)
//        }
//     
//        
//        
//        
//        
//        
//        let indexPath = IndexPath(row: 0, section: selectedSection) as NSIndexPath
//        if indexPath.row == 0 {
//            let collapsed: Bool = arrayForBool[indexPath.section] as! Bool
//            for i in 0..<subcatNameArray.count {
//                if indexPath.section == i {
//                    arrayForBool[i] = (!collapsed)
//                }
//            }
//            self.table.reloadSections([selectedSection], with: .automatic)
//           
//            
//            let collapse: Bool = arrayForBool[indexPath.section] as! Bool
//            
//            if (collapse == true)
//            {
//                let indexPath = IndexPath(row: 0, section: selectedSection)
//                
//                self.table.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
//            }
//            
//            
//        }
//    }
//    
//    
    
    
    
    
    
    
    
    
    //Mark :- Facebook Login and Get Post Part Don' Touch it ....
    
    
    //Please Don't Touch This Code ......../////////

    // let timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(someSelector), userInfo: nil, repeats: false)
    
//    func check_fbpermission() {
//
//           login = FBSDKLoginManager()
//            
//            login.logIn(withPublishPermissions: ["publish_actions"], from: self, handler: { (result, error) in
//                
//                //TODO: process error or result.
//                print("error are \(error)")
//                print("REsult are \(result)")
//                self.fbAccessToken = FBSDKAccessToken.current().tokenString
//                if error == nil {
//                    self.login .logIn(withReadPermissions:["public_profile","user_friends","user_photos","user_location","user_education_history","user_birthday","user_posts"], from: self) { (result, error) in
//                        if (error == nil)
//                        {
//                            print( (result)!)
//                          // self.user_id = (result?.value(forKey: "id") as! NSString)  as NSString
//                           // self.user_id = FBSDKAccessToken.current().userID as NSString
//                            
//                           self.fbAccessToken = FBSDKAccessToken.current().tokenString
//                            print(self.fbAccessToken);
//                            //self .uploadvideo()
//
//                            self.getuserpost()
//                        }
//                        else
//                        {
//                            print(error)
//                        }
//                        
//                    }
//                }
//            })
//        
//    }
    
    
  
    
    
    
    
//    func getuserpost()
//    {
//        
//        let params: [AnyHashable: Any] = ["fields": "id, name, story , message, from, picture, object_id, created_time, description, link, source, properties, type"]
//        
//        let request : FBSDKGraphRequest  = FBSDKGraphRequest(graphPath: "/me/feed", parameters: params, httpMethod: "GET")
//        
//        request.start(completionHandler: { (connection, result,  error) in
//            // Handle the result
//            
//            print("Error: \(error)")
//            print("result: \(result)")
//        })
//        
//        
//        
//    }
    
    
       
    
//
//    
//    func uploadvideo() {
//        let bundle = Bundle.main
//        let url = bundle.url(forResource: "fmovie", withExtension: ".mov")
//        let videoData = NSData.init(contentsOf: url!)
//
//        var params = [AnyHashable: Any](minimumCapacity: 3)
//        params["video_filename.MOV"] = videoData
//        params["title"] = "Title for this post11."
//        params["description"] = "Scuba Post."
//        
//        let graphRequest = FBSDKGraphRequest.init(graphPath: "me/videos", parameters: params, httpMethod: "POST")
//        
//        let connection = FBSDKGraphRequestConnection()
//        
//        connection.delegate = self
//        
//        connection.add(graphRequest) { (connection, result, error) in
//            
//            
//            if (error == nil)
//            {
//              print( (result)!)
//             self.post_id =  ((result as AnyObject).value(forKey: "id") as! NSString)  as NSString
//                self.getpostURL()
//                
//            }
//            else
//            {
//                 print( (error)!)
//            }
//        }
//        
//        connection.start()
//
//    }
    
//    func getpostURL()
//    {
//          var Requset : FBSDKGraphRequest
////        
//        let params: [AnyHashable: Any] = [fbAccessToken: "1798452450405925|c68162970c2f3cd639857b151fbb665d"]
//
//        
//        //  Converted with Swiftify v1.0.6242 - https://objectivec2swift.com/
//         Requset = FBSDKGraphRequest(graphPath: "397866140565511", parameters: params, httpMethod: "GET")
//        
//        Requset.start(completionHandler: { (connection, result, error) -> Void in
//            
//            
//            if ((error) != nil)
//                        {
//                            print ("Error: \(error)")
//                        }
//                        else
//                        {
//                            print("fetched user: \(result)")
//            
//                         //   var dataDict: AnyObject = (result! ).object("data")!
//                           
//                        }
//            
//            
//            
//        })
//        
        
        
        
        
        
//        var Requset : FBSDKGraphRequest
//        
//        
//        var acessToken = String(format:"%@", FBSDKAccessToken.current().tokenString) as String
//        
//        
//        var parameters1 = ["access_token":FBSDKAccessToken.current().tokenString]
//        
//        
//        Requset  = FBSDKGraphRequest(graphPath:"me/posts", parameters:parameters1, httpMethod:"GET")
//        
//        Requset.start(completionHandler: { (connection, result, error) -> Void in
//            
//            
//            if ((error) != nil)
//            {
//                print ("Error: \(error)")
//            }
//            else
//            {
//                print("fetched user: \(result)")
//                
//             //   var dataDict: AnyObject = (result! ).object("data")!
//               
//            }
//        })
//    
//        let jsonUrlPath = "https://graph.facebook.com/\(user_id)/posts?fields=description,picture&access_token=\(fbAccessToken)"
//        let url:NSURL = NSURL(string: jsonUrlPath)!
//        
//        let session  = URLSession .shared
//        _ = session.dataTask(with: url as URL, completionHandler: {data,response,error -> Void in
//            
//            print(data!)
//            if ((error) != nil)
//            {
//                
//                 print(error!)
//                print(response!)
//                print(data!);
//                
//            }
//            else
//            {
//                print(error!)
//                print(response!)
//                print(data!);
//
//            }
//            
//            
//        })
//        
      
   //  https://www.facebook.com/brst.dev32/videos/397868080565317/
        
   // }
        
    
    
    @IBAction func searchResultAction(_ sender: Any)
    {
//        if self.checkDiveSiteName == false
//        {
//            self.lbl_searchSpot_title.text = "SEARCH DIVE SITE"
//            self.checkDiveSiteName = true
//            self.checkTable = true
//            searchbtn.isUserInteractionEnabled = false
//            var diveSite = String()
//            diveSite = self.subcatNameArray[4] as! String
//            self.subcatNameArray.removeAllObjects()
//            self.subcatNameArray.add(diveSite)
//            print(self.subcatNameArray)
//            self.table.reloadSectionIndexTitles()
//            self.table.reloadData()
//        }
//        else
//        {
//            self.lbl_searchSpot_title.text = "SEARCH SPOT"
//            self.checkTable = false
//           // self.Log_Typeview.isHidden = true
//            self.reSetData()
            self.performSegue(withIdentifier: "LoginPage", sender: sender)

//            let myAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
//            
//            myAppDelegate?.ishome = true
//            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "searchResult") as! SearchPOIViewController
//            secondViewController.checkLogin = true
//            self.navigationController?.pushViewController(secondViewController, animated: true)
    //    }

    }
    
   // +++++++++++++++
    
    private func searchSpot() {
        do {
            // self.checkTable = false
            self.Scuba_Tropics_copy.removeAllObjects()
            self.reSetData_array.removeAllObjects()
            self.section_array.removeAllObjects()
            if let file = Bundle.main.url(forResource: "oceans", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                let data1 = json as! NSDictionary
                let Scuba_Tropics = data1.value(forKey: "Scuba Tropics") as! NSArray
               // print(Scuba_Tropics)
                self.Scuba_Tropics_copy = Scuba_Tropics.mutableCopy() as! NSMutableArray
                //  print(Scuba_Tropics_copy)
                
                // >>>
                let country1 = Scuba_Tropics.value(forKey: "COUNTRY") as! NSArray
                var country11 = NSMutableArray()
                let country = NSMutableArray()
                country11 = country1.mutableCopy() as! NSMutableArray
                if country11.count > 0
                {
                    for i in 0..<(country11.count-1)
                    {
                        if country.contains(country11[i])
                        {
                            
                        }
                        else
                        {
                            country.add(country11[i])
                        }
                    }
                }
                //   print(country)
                //                let dive_site1 = Scuba_Tropics.value(forKey: "SITE NAME") as! NSArray
                //
                //                var dive_site11 = NSMutableArray()
                //                let dive_site = NSMutableArray()
                //                dive_site11 = dive_site1.mutableCopy() as! NSMutableArray
                //                if dive_site11.count > 0
                //                {
                //                    for i in 0..<(dive_site11.count-1)
                //                    {
                //                        if dive_site.contains(dive_site11[i])
                //                        {
                //
                //                        }
                //                        else
                //                        {
                //                            dive_site.add(dive_site11[i])
                //                        }
                //                    }
                //                }
                //
                
                //                let GPS_coordinates = Scuba_Tropics.value(forKey: "GPS Coordinates") as! NSArray
                //                let ocean1 = Scuba_Tropics.value(forKey: "OCEAN") as! NSArray
                //                var ocean11 = NSMutableArray()
                //                let ocean = NSMutableArray()
                //                ocean11 = ocean1.mutableCopy() as! NSMutableArray
                //                if ocean11.count > 0
                //                {
                //                    for i in 0..<(ocean11.count-1)
                //                    {
                //                        if ocean.contains(ocean11[i])
                //                        {
                //
                //                        }
                //                        else
                //                        {
                //                            ocean.add(ocean11[i])
                //                        }
                //                    }
                //                }
                
                
                //>>
                let region1 = Scuba_Tropics.value(forKey: "Region") as! NSArray
                
                var region11 = NSMutableArray()
                let region = NSMutableArray()
                region11 = region1.mutableCopy() as! NSMutableArray
                if region11.count > 0
                {
                    for i in 0..<(region11.count-1)
                    {
                        if region.contains(region11[i])
                        {
                            
                        }
                        else
                        {
                            region.add(region11[i])
                        }
                    }
                }
                
                // >>
                let sub_region1 = Scuba_Tropics.value(forKey: "Sub Region") as! NSArray
                
                var sub_region11 = NSMutableArray()
                let sub_region = NSMutableArray()
                sub_region11 = sub_region1.mutableCopy() as! NSMutableArray
                if sub_region11.count > 0
                {
                    for i in 0..<(sub_region11.count-1)
                    {
                        if sub_region.contains(sub_region11[i])
                        {
                            
                        }
                        else
                        {
                            sub_region.add(sub_region11[i])
                        }
                    }
                }
                
                //>>
                // let activity = ["Surfing", "Snorkeling", "Scuba"] as NSArray
                let SpotType = ["Beach", "Dive Spot"] as NSArray
                
                //    DuplicatesubcatagoriesArray .add(ocean)
                DuplicatesubcatagoriesArray .add(SpotType)
                DuplicatesubcatagoriesArray .add(country)
                DuplicatesubcatagoriesArray . add(region)
                DuplicatesubcatagoriesArray . add(sub_region)
                //   DuplicatesubcatagoriesArray . add(dive_site)
                //  DuplicatesubcatagoriesArray .add(activity)
                // DuplicatesubcatagoriesArray . add(GPS_coordinates)
                self.section_array = ["Spot","COUNTRY","Region","Sub Region"] // "OCEAN",,"SITE NAME"
                self.set_Map()
                Util.setupview(singleview: searchbtn);
                self.reSetData_array = self.DuplicatesubcatagoriesArray.mutableCopy() as! NSMutableArray
             //   print(DuplicatesubcatagoriesArray)
                if json is [String: Any] {
                    // json is a dictionary
                    //    print(object)
                } else if json is [Any] {
                    // json is an array
                    // print(object)
                } else {
                    //  print("JSON is invalid")
                }
            } else {
                //  print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    //MARK:<<< Table View Functions: >>>>>
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
//        if self.checkTable == true
//        {
//            return self.subcatNameArray.count
//        }
      //  searchbtn.isUserInteractionEnabled = true
        
        return subcatNameArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if self.checkTable == true
//        {
//            return (self.DuplicatesubcatagoriesArray[4] as AnyObject).count
//        }
        if  (arrayForBool[section] as! Bool == true as Bool)
        {
            return (self.DuplicatesubcatagoriesArray[section] as AnyObject).count
        }
        else
        {
            return 0;
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:SlidemenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! SlidemenuTableViewCell
        
        let sectionView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.table.frame.size.width ), height: CGFloat(45)))
        sectionView.removeFromSuperview()
        //        sectionView.addSubview(viewLabel)
        
        
        // cell.contentView = nil;
        
        
        if let nameLabel = cell.viewWithTag(100) as? UILabel{
            var arr = NSArray()
            var demo = NSArray()
            arr = self.DuplicatesubcatagoriesArray[indexPath.section] as! NSArray
//            if self.checkTable == true
//            {
//                arr = self.DuplicatesubcatagoriesArray[4] as! NSArray
//            }
            nameLabel.backgroundColor = UIColor.clear
            nameLabel.textColor = UIColor.white
            nameLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
            let str1 =  "  " as String
            demo = self.DuplicatesubcatagoriesArray[indexPath.section] as! NSArray
//            if self.checkTable == true
//            {
//                demo = self.DuplicatesubcatagoriesArray[4] as! NSArray
//            }
            let str2 = demo[indexPath.row] as! String
            var str  =   str1 + str2
            str = arr[indexPath.row] as! String
            nameLabel.text = ""
            nameLabel.text = (str as NSString) as String
            
        }
        else{
            let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.table.frame.size.width-20 ), height: CGFloat(44)))
            
            viewLabel.backgroundColor = UIColor.clear
            viewLabel.textColor = UIColor.white
            viewLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
            viewLabel.text = ""
            viewLabel.tag = 100;
            
            var arr = NSArray()
            
            arr = self.DuplicatesubcatagoriesArray[indexPath.section] as! NSArray
//            if self.checkTable == true
//            {
//                arr = self.DuplicatesubcatagoriesArray[4] as! NSArray
//            }
            let str = arr[indexPath.row] as! String
            viewLabel.text = ""
            viewLabel.text = (str as NSString) as String
            
            cell.addSubview(viewLabel)
        }
        
        //   sectionView.addSubview(viewLabel)
        /********** Add a custom Separator with Section view *******************/
        let separatorLineView = UIView(frame: CGRect(x: CGFloat(10), y: CGFloat(44), width: CGFloat(self.table.frame.size.width-20), height: CGFloat(1)))
        separatorLineView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
        sectionView.addSubview(separatorLineView)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var arr = NSArray()
//        if self.checkTable == true
//        {
//            arr = self.DuplicatesubcatagoriesArray[4] as! NSArray
//            subcatNameArray[0] = (arr .object(at: indexPath.row))
//            searchbtn.isUserInteractionEnabled = true
//            self.section_index = (0)
//            self.item_selected = subcatNameArray[0] as! String
//            UserDefaults.standard.set(self.item_selected, forKey: "diveSiteName")
//            arrayForBool[0] = false;
//            
//            self.table.reloadSections([0], with: .automatic)
//        }
//        else
//        {
            arr = self.DuplicatesubcatagoriesArray[indexPath.section] as! NSArray
            
            subcatNameArray[indexPath.section] = (arr .object(at: indexPath.row))
            
            self.section_index = (indexPath.section)
            self.item_selected = subcatNameArray[indexPath.section] as! String
            
            arrayForBool[indexPath.section] = false;
            
            self.table.reloadSections([indexPath.section], with: .automatic)
      //  }
        if self.section_index < 5
        {
            self.selected_output()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let sectionView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.table.frame.size.width ), height: CGFloat(53)))
        sectionView.tag = section
        let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.table.frame.size.width-20 ), height: CGFloat(45)))
        let imageview = UIImageView (frame: CGRect(x: CGFloat(self.table.frame.size.width-50), y: CGFloat(15), width: CGFloat(20 ), height: CGFloat(20)))
        
        imageview.image = UIImage(named: "arw-dwn")
        imageview.contentMode = UIViewContentMode.scaleAspectFit;
        
        viewLabel.textColor = UIColor.white
        viewLabel.font = UIFont.systemFont(ofSize: CGFloat(15))
        let str1 =  "  " as String
        let str2 = subcatNameArray[section] as! String
        let str  =   str1 + str2
        viewLabel.text = (str as NSString) as String
        sectionView.addSubview(viewLabel)
        /********** Add a custom Separator with Section view *******************/
        let separatorLineView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(45), width: CGFloat(self.table.frame.size.width), height: CGFloat(8)))
        separatorLineView.backgroundColor = UIColor.clear
        sectionView.addSubview(separatorLineView)
        sectionView.addSubview(imageview)
        viewLabel.backgroundColor = UIColor.lightGray
        viewLabel.layer.borderWidth = 1
        viewLabel.layer.masksToBounds = true
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        sectionView.tag = section+100;
        sectionView.addGestureRecognizer(tap)
        return sectionView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        
        return 53
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 45
    }
    
    func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
        selectedSection = (gestureRecognizer.view?.tag)! as Int
       // print (selectedSection)
        selectedSection = selectedSection - 100
        //  self.table.reloadData()
        
        
        let indexPath = IndexPath(row: 0, section: selectedSection) as NSIndexPath
        if indexPath.row == 0 {
            let collapsed: Bool = arrayForBool[indexPath.section] as! Bool
            for i in 0..<subcatNameArray.count {
                if indexPath.section == i {
                    arrayForBool[i] = (!collapsed)
                }
            }
            self.table.reloadSections([selectedSection], with: .automatic)
            
            
            
            let collapse: Bool = arrayForBool[indexPath.section] as! Bool
            
            if (collapse == true)
            {
                let indexPath = IndexPath(row: 0, section: selectedSection)
                
                self.table.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
            }
            
            
        }
    }
    func selected_output()
    {
        if self.Scuba_Tropics_copy.count > 0
        {
            
            let Scuba_Tropics = self.Scuba_Tropics_copy.mutableCopy() as! NSMutableArray
            DispatchQueue.main.async
                {
                    
                    
                    for j in 0..<self.section_array.count
                    {
                        let array = NSMutableArray()
                        if j != self.section_index
                        {
                            for i in 0..<(Scuba_Tropics.count)
                            {
                                let selected_section = self.section_array[self.section_index]   as! String  // >> selected option
                                let section_item = Scuba_Tropics[i] as!NSDictionary
                                //print("index : = \(section_item)")
                                if (section_item.value(forKey: selected_section) as! String) == self.item_selected
                                {
                                    let strData = section_item.value(forKey: self.section_array[j] as! String)
                                    if array.contains(strData!) == false
                                    {
                                        array.add(strData!)
                                        
                                    }
                                    
                                }
                            }
                            
                            self.DuplicatesubcatagoriesArray[j] = array.mutableCopy()
                        }
                    }
                    
                    
                    
                    self.table.reloadData()
            }
        }
    }
    @IBAction func btn_resetData(_ sender: Any)
    {
        self.reSetData()
    }
    
    func reSetData()
    {
        // self.lbl_searchSpot_title.text = "SEARCH SPOT"
        // self.checkDiveSiteName = false
        // self.checkTable = false
        subcatNameArray.removeAllObjects()
        self.DuplicatesubcatagoriesArray = self.reSetData_array.mutableCopy() as! NSMutableArray
        subcatNameArray = ["Spot","Country","Region","Sub Region"]  //"Ocean",,"Activity","DIVE SITE NAME"
      //  print(self.DuplicatesubcatagoriesArray);
        self.table.reloadSectionIndexTitles()
        
        self.table.reloadData()
    }

        
}







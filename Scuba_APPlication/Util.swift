//
//  Util.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 05/07/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class Util: NSObject {
    
    
    let ButtonWidth  : CGFloat = 230.0
    let ButtonHeight : CGFloat = 40.0
    let MarginY      : CGFloat = 10.0
    
    let ThemeColor   = UIColor(red: 255/255.0, green: 163/255.0, blue: 0/255.0, alpha: 1.0)
  
    
    
   class func setupview(singleview: UIButton)
    {
    
    let red = UIColor(red: 255/255, green: 190/255, blue: 33/255, alpha: 1.0)
    singleview.layer.borderColor = red.cgColor
    singleview.layer.borderWidth = 1
    singleview.layer.masksToBounds = true
  
    }
    
  
    
    class func login(with  Name: String, Password :String , withSuccess success: @escaping (_ dic: NSDictionary) -> Void, failure: @escaping (_: String) -> Void)
    {
        
        
        
    }
    
    class func updateuser_id(user_id : Int)
    {
        let str = String(user_id)
        print(str)
        let string = "\(user_id)"
        print(string );
        UserDefaults.standard.set(user_id, forKey: "User_id")
    }
    
//    class func getuser_id() -> String
//    {
////        let prefs = UserDefaults.standard
////        let myString: String? = prefs.string(forKey: "User_id")
//        //return String;
//    }
    
    
    
    class func Parsedict(with data:NSArray) -> NSArray
    {
        
        //var Tripsarray = NSMutableArray()
        let Tripsarray :NSMutableArray = []
        
        print (data);
        if (data.count > 0)
        {
            for var i in 0..<data.count {

            var dict = NSDictionary()
                 dict  =  data .object(at:i)  as! NSDictionary
                
                let histoy: HistoryLogs = HistoryLogs()
                histoy.id =  validator .getsafestring(obj: dict ["id"]as Any )
                histoy.user_id = validator .getsafestring(obj: dict["user_id"]as Any )
                histoy.date = validator .getsafestring(obj: dict["date"] as Any)
                print(histoy.date)
                histoy.meteo = validator .getsafestring(obj: dict["meteo"]as Any)
                histoy.water_conditions = validator .getsafestring(obj: dict["water_conditions"]as Any)
                histoy.visibility = validator .getsafestring(obj: dict["visibility"]as Any)
                histoy.location = validator .getsafestring(obj: dict["location"]as Any)
                histoy.activity = validator .getsafestring(obj: dict["activity"]as Any)
                histoy.water_temprature =  validator .getsafestring(obj: dict["water_temprature"]as Any)
                let latitude = validator.getsafestring(obj: dict["latitude"]as Any)
                if (latitude == "")
                {
                    
                }
                else
                {
                    histoy.latitude = Double(latitude)!
                    print(histoy.latitude )
                    let longitude = validator.getsafestring(obj: dict["longitude"]as Any)
                    if (longitude == "")
                    {
                        
                    }
                    else
                    {
                        histoy.longitude = Double (longitude)!
                        print(histoy.latitude )
                        Tripsarray.add(histoy)
                        print(Tripsarray.count)
                    }
                    
                }
                
            }
            
        }
        
        print(Tripsarray);
        print(Tripsarray.count)
        return Tripsarray;
    }
    
    
    class func ParseLocation(with data:NSArray) -> NSArray
    {
        
        //var Tripsarray = NSMutableArray()
        let LocationArray :NSMutableArray = []
        
        print(data.count);
        if (data.count > 0)
        {
            for var i in 0..<data.count {
                
                var dict = NSDictionary()
                dict  =  data .object(at:i)  as! NSDictionary
              //  print("user Id =\( USerinfo.sharedInstance.user_id) \(dict.value(forKey: "user_id") as! String)")
              //  if dict.value(forKey: "user_id") as! String ==  USerinfo.sharedInstance.user_id
              //  {
                    let Location: Location_List = Location_List()
                    Location.id =  validator .getsafestring(obj: dict ["id"] as Any )
                    Location.user_id = validator .getsafestring(obj: dict["user_id"]as Any)
                    Location.date = validator .getsafestring(obj: dict["date"] as Any)
                    print(Location.date)
                    Location.meteo = validator .getsafestring(obj: dict["meteo"]as Any)
                    Location.water_conditions = validator .getsafestring(obj: dict["water_conditions"]as Any)
                    Location.visibility = validator .getsafestring(obj: dict["visibility"]as Any)
                    Location.location = validator .getsafestring(obj: dict["location"]as Any)
                    Location.activity = validator .getsafestring(obj: dict["activity"]as Any)
                    Location.water_temprature =  validator .getsafestring(obj: dict["water_temprature"]as Any)
                    let latitude = validator.getsafestring(obj: dict["latitude"]as Any)
                    if (latitude == "")
                    {
                        
                    }
                    else
                    {
                        Location.latitude = Double(latitude)!
                        print(Location.latitude)
                        let longitude = validator.getsafestring(obj: dict["longitude"]as Any)
                        if (latitude == "")
                        {
                            
                        }
                        else
                        {
                            Location.longitude = Double (longitude)!
                            print(Location.latitude )
                            LocationArray.add(Location)
                            print(LocationArray.count)
                        }
                        
                    }
                    
              //  }
             
                
            }
            
        }
        
      //  print(LocationArray);
       // print(LocationArray.count)
        return LocationArray;
    }
    
    

    
    class func showindicatior(with view:UIView)
    {
        let spinnerActivity = MBProgressHUD.showAdded(to: view, animated: true);
        //spinnerActivity.backgroundColor = UIColor(red: 127.0, green: 127.0, blue: 127.0, alpha: 1.0)
        spinnerActivity.label.text = "Loading";
        //spinnerActivity.detailsLabel.text = "Please Wait!!";
        spinnerActivity.isUserInteractionEnabled = false;
    }
    
    class func stopIndicator(with  view:UIView)
    
    {
        
        
        MBProgressHUD.hideAllHUDs(for: view, animated: true);

    }
    
    class func showMessage(_ message: String, withTitle title: String) {
        
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOK = UIAlertAction(title:"ok", style: .default, handler: nil)
        alertController.addAction(actionOK)
        UIApplication.shared.keyWindow?.rootViewController?.present((alertController as? UIViewController)! , animated: true) { _ in }

    }

        }






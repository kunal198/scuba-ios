//
//  SidemenuViewController.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 07/02/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit


var controller:HomeViewController?
class SidemenuViewController: UIViewController  , UITableViewDataSource ,UITableViewDelegate
{
    
    var window: UIWindow?
    var items: [String] = ["Home", "My Historic Logs", "My Gallery" ,"My Account" ,"Terms & Conditions","About Us","Contact Us","Logout"]

    var images :[String] = ["slide-home", "slide-history", "slide-gallery" ,"slide-account" ,"slide-gallery","slide-gallery","slide-gallery","slide-logout"]
    
    @IBOutlet var slidetableview: UITableView!
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var txt_name: UILabel!
    @IBOutlet var imgLogo: UIImageView!
    
    
    @IBAction func close_Sidemenu(_ sender: Any)
    {
        
        let controlr = self.navigationController?.visibleViewController
        
        if (controlr is HomeViewController)
        {
            
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"MyNotification"),
                    object: nil,
                    userInfo: ["message":"Hello there!", "date":Date()])
            
        }
        else   if (controlr is HistoricalViewController)
        {
            
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"Historicalview"),
                    object: nil,
                    userInfo: ["message":"Hello there!", "date":Date()])
            
        }
       else if (controlr is MyAccountViewController)
        {
            
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"MyAccountview"),
                    object: nil,
                    userInfo: ["message":"Hello there!", "date":Date()])
            
        }
        else if (controlr is AdddLOgsViewController)
        {
            
            
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"hideAddlogs"),
                    object: nil,
                    userInfo: ["message":"Hello there!", "date":Date()])
        }

      
        
            
       else  if (controlr is AboutusViewController)
        {
            
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"Aboutus"),
                    object: nil,
                    userInfo: ["message":"Hello there!", "date":Date()])
            
        }
        
        
        
       else if (controlr is TermsViewController)
        {
            
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"TermsPage"),
                    object: nil,
                    userInfo: ["message":"Hello there!", "date":Date()])
            
        }
        else if (controlr is ContactUsViewController)
        {
            
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"Contactus"),
                    object: nil,
                    userInfo: ["message":"Hello there!", "date":Date()])
            
        }
        
        else if (controlr is SearchPOIViewController)
        {
            
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"hidesearchpoi"),
                    object: nil,
                    userInfo: ["message":"Hello there!", "date":Date()])
            
        }
        else if (controlr is ScubaGalleryViewController)
        {
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"gallery"),
                    object: nil,
                    userInfo: ["message":"Hello there!", "date":Date()])
            
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gettingDetails()

        self.img_profile.layer.cornerRadius = self.img_profile.frame.size.width/2
        self.img_profile.layer.masksToBounds = true
        
    let notificationName = Notification.Name("NotificationIdentifier")

       NotificationCenter.default.addObserver(self, selector: #selector(SidemenuViewController.catchNotification(notification:)), name: notificationName, object: nil)

        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.imgLogo.isHidden = false
        }
        else
        {
            self.imgLogo.isHidden = true
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
       
    }
    
    func catchNotification(notification:Notification) -> Void
    {
        self.gettingDetails()
    }
    //Mark : Tableview Datasource Method :-
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    
    
    {
        return items .count;
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SlidemenuTableViewCell
        
                cell.slidemenuLabel.text = self.items[indexPath.row]
                cell.slidemenuimg.image = UIImage (named:self.images[indexPath.row] )
        
              
        
       return cell
    }
    //Mark : Tableview Delegate Method :-

     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
     {
        switch indexPath.row {
            case  0:
                let controlr = self.navigationController?.visibleViewController
                
                if (controlr is HomeViewController)
                {

                    let nc = NotificationCenter.default
                    nc.post(name:Notification.Name(rawValue:"MyNotification"),
                            object: nil,
                            userInfo: ["message":"Hello there!", "date":Date()])
                    
                }
          else if (controlr is HistoricalViewController)||(controlr is MyAccountViewController)
                {
                    self.tabBarController?.selectedIndex = 0
                    
                    

                }
                else
                {
                    let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeViewController
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
  
            break
            case  1:
                let controlr = self.navigationController?.visibleViewController
                
                if (controlr is HistoricalViewController)
                {
                    
                    let nc = NotificationCenter.default
                    nc.post(name:Notification.Name(rawValue:"Historicalview"),
                            object: nil,
                            userInfo: ["message":"Hello there!", "date":Date()])
                    
                }
                    
                else if (controlr is HomeViewController)||(controlr is MyAccountViewController)
                {
                    self.tabBarController?.selectedIndex = 1
                    
                    
                }
                else
                {
                    let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "history") as! HistoricalViewController
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
                

                    
                
            break
            
            
        case 2:
            
                let controlr = self.navigationController?.visibleViewController
                
                if (controlr is ScubaGalleryViewController)
                {
                    
                    let nc = NotificationCenter.default
                    nc.post(name:Notification.Name(rawValue:"gallery"),
                            object: nil,
                            userInfo: ["message":"Hello there!", "date":Date()])
                    
                }
                else
                {
                    
                    let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "gallery") as! ScubaGalleryViewController
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
            break
            
        
                        
        case 3:
            
            
            let controlr = self.navigationController?.visibleViewController
            
            if (controlr is MyAccountViewController)
            {
                
                let nc = NotificationCenter.default
                nc.post(name:Notification.Name(rawValue:"MyAccountview"),
                        object: nil,
                        userInfo: ["message":"Hello there!", "date":Date()])
                
            }
            else if (controlr is HistoricalViewController)||(controlr is HomeViewController)
            {
                self.tabBarController?.selectedIndex = 2
                
                
            }
            else
            {
                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyAccount") as! MyAccountViewController
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
         
            break

        case 4:
            let controlr = self.navigationController?.visibleViewController
            
            if (controlr is TermsViewController)
            {
                let nc = NotificationCenter.default
                nc.post(name:Notification.Name(rawValue:"TermsPage"),
                        object: nil,
                        userInfo: ["message":"Hello there!", "date":Date()])
                
            }
            else
            {
                self .performSegue(withIdentifier: "terms", sender: self)
            }
            break
                        case 5:
            
            
                            
                            let controlr = self.navigationController?.visibleViewController
                            
                            if (controlr is AboutusViewController)
                            {
                                
                                let nc = NotificationCenter.default
                                nc.post(name:Notification.Name(rawValue:"Aboutus"),
                                        object: nil,
                                        userInfo: ["message":"Hello there!", "date":Date()])
                                
                            }
                            else
                            {
                                self .performSegue(withIdentifier: "Aboutus", sender: self)
                            }
            break

        case 6:
            
            
            
            let controlr = self.navigationController?.visibleViewController
            
            if (controlr is ContactUsViewController)
            {
                
                let nc = NotificationCenter.default
                nc.post(name:Notification.Name(rawValue:"Contactus"),
                        object: nil,
                        userInfo: ["message":"Hello there!", "date":Date()])
                
            }
            else
            {
                
                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "Contactus") as! ContactUsViewController
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
            break
            
        case 7 :
            
            
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            FBSDKAccessToken.setCurrent(nil)
            
                UserDefaults.standard.set("NO", forKey: "Islogin")
                UserDefaults.standard.synchronize()
                let kUserDefault = UserDefaults.standard
                kUserDefault.set(nil, forKey: "id")
                
                window = UIWindow(frame: UIScreen.main.bounds)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainHome")
                window?.rootViewController = initialViewController
                window?.makeKeyAndVisible()
                
                
                
                
                
                
                
                
            
            break
            
            default:break
        }
        
        
        
    }

    //MARK: Getting Profile Details
    func gettingDetails()
    {
        
//        DispatchQueue.main.async {
//            //self.view .isUserInteractionEnabled = false
//            Util.showindicatior(with: (self.view)!)
//        }
        
        Model.getProfileDetails(withSuccess: { (Result) in
            print(Result)
            
            if(Result.object(forKey: "data") != nil)
            {
                let data = Result.object(forKey: "data") as! NSDictionary
                let user_data = data.object(forKey: "user_data") as! NSDictionary
                                
                var str1 = String()
                var str2 = String()
                 if user_data.value(forKey: "firstname") as? String != nil
                 {
                    let firstName = (user_data.value(forKey: "firstname") as! String)
                    str1 = firstName
                 }
                 if user_data.value(forKey: "lastname") as? String != nil
                 {
                    let lastName = (user_data.value(forKey: "lastname") as! String)
                    str2 = lastName
                 }
                if str1 == ""
                {
                    self.txt_name.text = str2
                }
                if str2 == ""
                {
                    self.txt_name.text = str1
                }
                if str1 != "" && str2 != ""
                {
                    let space = " "
                    let str = str1 + space + str2
                    self.txt_name.text = str
                }
                if str1 == "" && str2 == ""
                {
                     self.txt_name.text = "SCUBAP"
                }

                // profile pic 
                if user_data.value(forKey: "image") as? String != nil
                {
                    let str = user_data.value(forKey: "image") as! String
                    if str.contains("https://scontent")
                    {
                        let fid = user_data.value(forKey: "social_id") as! String
                        let imgURLString = "http://graph.facebook.com/" + fid + "/picture?type=large" //type=normal
                        
                        self.img_profile.sd_setShowActivityIndicatorView(true)
                        self.img_profile.sd_setIndicatorStyle(.gray)
                        self.img_profile.sd_setImage(with:  URL(string: imgURLString ))
                    }
                    else
                    {
                        let urlStr = "http://beta.brstdev.com/Scubap/common/uploads/profile_images/"
                        let url11 = (user_data.value(forKey: "image") as! String)
                        let url = urlStr + url11
                    
                        self.img_profile.sd_setShowActivityIndicatorView(true)
                        self.img_profile.sd_setIndicatorStyle(.gray)
                        self.img_profile.sd_setImage(with:  URL(string: url ))
                    }
                }
                
            }
            
        }) { (error) in
            print (error)
            Util.stopIndicator(with: (self.view)!)
            
        }
        
    }


}

//
//  StarbucksAnnotation.swift
//  CustomCalloutView
//
//  Created by Malek T. on 3/16/16.
//  Copyright © 2016 Medigarage Studios LTD. All rights reserved.
//

import MapKit

class StarbucksAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var phone: String!
    var subtitle: String!
    var title: String!
    var name: String!
    var address: String!
    var water: String!
    var weather: String!
    var image: UIImage!
    var conditions: String!
    var visibility: String!
    var latitude :String!
    var longitude : String!
    var userId : String!
    
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
    
    
    
    
//        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//    
//            if annotation is MKUserLocation
//            {
//                return nil
//            }
//            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
//            if annotationView == nil{
//                annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
//                annotationView?.canShowCallout = false
//            }else{
//                annotationView?.annotation = annotation
//            }
//            annotationView?.image = image;
//    
//            
//            return annotationView
//        }
}


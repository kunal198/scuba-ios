//
//  constant.swift
//  Scuba_APPlication
//
//  Created by Brst981 on 05/07/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import Foundation

let  BASE_URL            =       "http://beta.brstdev.com/Scubap/api/web/index.php/v1/"
let historyLog           =       "gethistory"
let login                =        "login"
let createlog            =         "createlog"
let  locationlog         =        "getlocations"
let  sociallogin         =        "sociallogin"
let getProfile           =        "getprofile"
let updateProfile        =        "updateprofile"
